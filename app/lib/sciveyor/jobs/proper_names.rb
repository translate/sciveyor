# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Extract proper nouns from documents
    class ProperNames < Base
      include Sciveyor::Visualization::Csv

      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["PROPER_NAMES_JOB_DISABLED"] || "false").to_boolean
      end

      # Export the proper name data
      #
      # This function saves out the proper names as a JSON array, to be visualized
      # by the job views.
      #
      # @param [Task] task the task we're working from
      # @return [void]
      def perform(task, options = {})
        super

        refs = Sciveyor::Analysis::ProperNames.call(dataset: dataset)
        refs ||= {}

        csv_string =
          csv_with_header(
            header: t(".header", name: dataset.name_for(user))
          ) do |csv|
            write_csv_data(
              csv: csv,
              data: refs,
              data_spec: {
                t(".name_column") => :first,
                t(".count_coumnt") => :second
              }
            )
          end

        output = { names: refs }

        # Write it out
        task
          .uploads
          .create(
            filename: unique_filename("proper_names.json"),
            content_type: "application/json",
            description: "Raw JSON Data",
            short_description: "JSON"
          ) { |f| f.from_string(output.to_json) }

        task
          .uploads
          .create(
            filename: unique_filename("results.csv"),
            content_type: "text/csv",
            description: "Spreadsheet",
            short_description: "CSV",
            downloadable: true
          ) { |f| f.from_string(csv_string) }

        task.mark_completed
      end
    end
  end
end
