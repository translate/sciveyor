# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Plot occurrences of a term in a dataset by date
    class TermDates < Base
      include Sciveyor::Visualization::Csv

      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["TERM_DATES_JOB_DISABLED"] || "false").to_boolean
      end

      # Export the date format data
      #
      # Like all view/multiexport jobs, this job saves its data out as a JSON
      # file and then sends it to the user in various formats depending on
      # user selectons.
      #
      # @param [Task] task the task we're working from
      # @param [Hash] options parameters for this job
      # @option options [String] :term the focal word to analyze
      # @return [void]
      def perform(task, options)
        super

        # Get the counts
        dates =
          Sciveyor::Analysis::CountTermsByField.call(
            term: @options[:term],
            field: :date,
            dataset: dataset
          )

        # Convert the dates to integers and sort
        dates = dates.to_a
        dates.each { |d| d[0] = d[0].to_i }
        dates.sort!

        # Save out the data
        date_header = Document.human_attribute_name(:date)
        value_header = t(".number_column")

        csv_string =
          csv_with_header(
            header: t(".header", name: dataset.name_for(user)),
            subheader: t(".subheader", term: @options[:term])
          ) do |csv|
            write_csv_data(
              csv: csv,
              data: dates,
              data_spec: {
                date_header => :first,
                value_header => :second
              }
            )
          end

        output = {
          data: dates,
          term: @options[:term],
          date_header: date_header,
          value_header: value_header
        }

        # Serialize out to JSON
        task
          .uploads
          .create(
            filename: unique_filename("term_dates.json"),
            content_type: "application/json",
            description: "Raw JSON Data",
            short_description: "JSON"
          ) { |f| f.from_string(output.to_json) }

        task
          .uploads
          .create(
            filename: unique_filename("results.csv"),
            content_type: "text/csv",
            description: "Spreadsheet",
            short_description: "CSV",
            downloadable: true
          ) { |f| f.from_string(csv_string) }

        task.mark_completed
      end
    end
  end
end
