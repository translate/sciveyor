# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Examine the network of words associated with a focal term
    class Network < Base
      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["NETWORK_JOB_DISABLED"] || "false").to_boolean
      end

      # Examine the network of words associated with a focal term.
      #
      # @param [Task] task the task we're working from
      # @param [Hash] options parameters for this job
      # @option options [String] :focal_word the focal word to analyze
      # @return [void]
      def perform(task, options)
        super

        graph =
          Sciveyor::Analysis::Network::Graph.new(
            dataset: dataset,
            focal_word: @options[:focal_word]
          )

        # Convert to D3-able format
        d3_nodes = graph.nodes.map { |n| { name: n.id, forms: n.words } }

        max_weight = graph.max_edge_weight.to_f
        d3_links =
          graph.edges.map do |e|
            {
              source: d3_nodes.find_index { |n| e.one == n[:name] },
              target: d3_nodes.find_index { |n| e.two == n[:name] },
              strength: e.weight.to_f / max_weight
            }
          end

        # Save out all the data
        data = {
          name: dataset.name_for(user),
          focal_word: @options[:focal_word],
          d3_nodes: d3_nodes,
          d3_links: d3_links,
          word_stem: t(".word_stem"),
          word_forms: t(".word_forms")
        }

        # Write it out
        task
          .uploads
          .create(
            filename: unique_filename("network.json"),
            content_type: "application/json",
            description: "Raw JSON Data",
            short_description: "JSON"
          ) { |f| f.from_string(data.to_json) }
        task.mark_completed
      end
    end
  end
end
