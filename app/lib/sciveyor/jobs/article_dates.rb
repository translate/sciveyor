# frozen_string_literal: true

module Sciveyor
  module Jobs
    # Plot a dataset's members by date
    class ArticleDates < Base
      include Sciveyor::Visualization::Csv

      # Returns true if this job can be started now
      #
      # @return [Boolean] true if this job is not disabled
      def self.available?
        !(ENV["ARTICLE_DATES_JOB_DISABLED"] || "false").to_boolean
      end

      # Export the date format data
      #
      # Like all view/multiexport jobs, this job saves its data out as a JSON
      # file and then sends it to the user in various formats depending on
      # user selectons.
      #
      # @param [Task] task the task we're working from
      # @param [Hash] options remaining job options
      # @option options [Boolean] :normalize if true, divide the counts for the
      #   dataset by the counts for the same field in normalization_dataset before
      #   returning
      # @option options [Dataset] :normalization_dataset dataset to normalize by
      #   (or nil for the whole corpus)
      # @return [void]
      def perform(task, options = {})
        super

        # Get the counts
        result =
          Sciveyor::Analysis::CountArticlesByField.call(
            @options.merge(field: :date, dataset: dataset)
          )

        # Convert the dates to integers and sort
        dates = result.counts.to_a
        dates.each { |d| d[0] = d[0].to_i }
        dates.sort!

        # Save out the data, including getting the name of the normalization
        # set for pretty display
        norm_set_name = ""
        if result.normalize
          norm_set_name =
            if result.normalization_dataset
              result.normalization_dataset.name_for(user)
            else
              t(".entire_corpus")
            end
          value_header = t(".fraction_column")
        else
          value_header = t(".number_column")
        end
        date_header = Document.human_attribute_name(:date)

        output = {
          data: dates,
          percent: result.normalize,
          normalization_set: norm_set_name,
          date_header: date_header,
          value_header: value_header
        }

        # Serialize out to JSON and CSV
        task
          .uploads
          .create(
            filename: unique_filename("article_dates.json"),
            content_type: "application/json",
            description: "Raw JSON Data",
            short_description: "JSON"
          ) { |f| f.from_string(output.to_json) }

        csv_string =
          csv_with_header(
            header: t(".header", name: dataset.name_for(user))
          ) do |csv|
            write_csv_data(
              csv: csv,
              data: dates,
              data_spec: {
                date_header => :first,
                value_header => :second
              }
            )
          end
        task
          .uploads
          .create(
            filename: unique_filename("results.csv"),
            content_type: "text/csv",
            description: "Spreadsheet",
            short_description: "CSV",
            downloadable: true
          ) { |f| f.from_string(csv_string) }

        task.mark_completed
      end
    end
  end
end
