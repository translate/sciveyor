# frozen_string_literal: true

module Sciveyor
  module Documents
    module Serializers
      # Convert a document to an EndNote record
      class EndNote < Base
        define_array(
          "EndNote",
          "https://en.wikipedia.org/wiki/EndNote"
        ) do |doc|
          ret = +"%0 Journal Article\n"
          doc.authors.each do |a|
            ret << "%A #{a.last}, #{a.first}"
            ret << " #{a.prefix}" if a.prefix
            ret << ", #{a.suffix}" if a.suffix
            ret << "\n"
          end
          ret << "%T #{doc.title}\n" if doc.title
          ret << "%D #{doc.date.strftime("%Y")}\n" if doc.date
          ret << "%J #{doc.journal}\n" if doc.journal
          ret << "%V #{doc.volume}\n" if doc.volume
          ret << "%N #{doc.number}\n" if doc.number
          ret << "%P #{doc.pages}\n" if doc.pages
          ret << "%R #{doc.doi}\n" if doc.doi

          if doc.keywords
            doc.keywords.take(5).each { |k| ret << "%K #{k}\n" }
          elsif doc.tags
            doc.tags.take(5).each { |k| ret << "%K #{k}\n" }
          end

          ret << "\n"
          ret
        end
      end
    end
  end
end
