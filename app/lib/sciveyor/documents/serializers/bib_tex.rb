# frozen_string_literal: true

module Sciveyor
  module Documents
    module Serializers
      # Convert a document to a BibTeX record
      class BibTex < Base
        define_array(
          "BibTeX",
          "http://mirrors.ctan.org/biblio/bibtex/contrib/doc/btxdoc.pdf"
        ) do |doc|
          # We don't have a concept of cite keys, so we're forced to just use
          # AuthorYear and hope it doesn't collide
          first_author =
            if doc.authors.empty?
              "Anon"
            else
              doc.authors[0].last.delete(" ").gsub(/[^A-za-z0-9_]/, "")
            end
          year = doc.date ? doc.date.strftime("%Y") : "nd"

          cite_key = "#{first_author}#{year}"

          ret = +"@article{#{cite_key},\n"
          unless doc.authors.empty?
            ret << "    author = {#{doc.authors.map(&:name).join(" and ")}},\n"
          end
          ret << "    title = {#{doc.title}},\n" if doc.title
          ret << "    journal = {#{doc.journal}},\n" if doc.journal
          ret << "    volume = {#{doc.volume}},\n" if doc.volume
          ret << "    number = {#{doc.number}},\n" if doc.number
          ret << "    pages = {#{doc.pages}},\n" if doc.pages
          ret << "    doi = {#{doc.doi}},\n" if doc.doi
          ret << "    year = {#{year}}\n" if doc.date
          ret << "}\n"

          ret
        end
      end
    end
  end
end
