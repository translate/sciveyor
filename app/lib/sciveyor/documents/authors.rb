# frozen_string_literal: true

module Sciveyor
  module Documents
    # The authors of a document
    #
    # This class patches some methods into +Array+ for creating the array of
    # authors from a list and converting it back into its original
    # comma-separated format.
    class Authors < Array
      # Create the array from a set of author documents
      #
      # @param [String] docs the nested author documents
      # @return [Authors] a list of authors for this document
      def self.from_docs(docs)
        # Solr will return a single result as one hash, rather than as a
        # one-element array containing a hash
        docs = [docs] if docs.is_a?(Hash)

        return Authors.new([]) if docs.nil? || docs.empty?

        array = docs.map { |d| Sciveyor::Documents::Author.new(d) }
        Authors.new(array)
      end

      # @return [String] a comma-separated list of author names
      def to_s
        map(&:name).join(", ")
      end
    end
  end
end
