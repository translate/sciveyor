# frozen_string_literal: true

# The module containing all domain-specific logic for Sciveyor
module Sciveyor
  # Code that connects Sciveyor to a Solr server and parses its responses
  module Solr
    # Methods for managing the singleton connection to the Solr server
    module Connection
      # Exception thrown on failure to connect to Solr
      class Error < RuntimeError
      end

      # The default Solr search fields
      DEFAULT_FIELDS =
        "[child],schema,version,id,doi,externalIds,license,licenseUrl,dataSource,dataSourceUrl,dataSourceVersion,type,title,authors,name,first,middle,last,prefix,suffix,affiliation,date,dateElectronic,dateAccepted,dateReceived,journal,volume,number,pages,keywords,tags"

      # Exceptions that can be raised by the Solr connection
      EXCEPTIONS =
        Net::HTTP::EXCEPTIONS +
          [
            RSolr::Error::Http,
            RSolr::Error::InvalidRubyResponse,
            Sciveyor::Solr::Connection::Error
          ]

      # Get a response from Solr
      #
      # This method breaks out the retrieval of a Solr response in order to
      # provide for easier testing.
      #
      # @param [Hash] params
      # @option params [Integer] :rows maximum number of results to return
      # @option params [String] :sort sorting string ('<method> <direction>')
      #
      # @return [Solr::SearchResult] Solr search result
      def self.search(params)
        # We actually have to do this manually to make POST-parameters work
        # right, instead of calling rsolr-ext's #find method.
        raw_response = search_raw(params)

        # Fix with a reasonable default if broken
        raw_response = { "response" => { "docs" => [] } } if raw_response.empty?

        SearchResult.new(
          RSolr::Ext::Response::Base.new(raw_response, "search", params)
        )
      rescue Solr::Connection::Error => e
        # Add information about the parameters to the exception
        raise $!, "#{$!}\n\nParams: #{params}", $!.backtrace
      end

      # Get a raw hash response from Solr
      #
      # Sometimes we don't want a cleaned up result, so just get the raw hash.
      #
      # @param [Hash] params Solr query parameters
      # @return [Hash] Solr search result, unprocessed
      def self.search_raw(params)
        ensure_connected!
        camelize_params!(params)

        # We have a different destination if term vectors are enabled
        dest = params[:tv] ? "termvectors" : "select"

        Thread.current[:solr_handle].post dest, data: params
      rescue *EXCEPTIONS => e
        Rails.logger.warn "Connection to Solr failed!\n\nError: #{e.inspect}\n\nQuery: #{params}"
        {}
      end

      # Get the info/statistics hash from Solr
      #
      # This method retrieves information about the Solr server, including the
      # Solr and Java versions.
      #
      # @return [Hash] Unprocessed Solr response
      def self.info
        ensure_connected!
        Thread.current[:solr_handle].get "admin/system"
      rescue *EXCEPTIONS => e
        Rails.logger.warn "Connection to Solr failed!\n\nError: #{e.inspect}"
        {}
      end

      # Ping the Solr server
      #
      # Returns the latency of the connection, or +nil+ if there is a connection
      # failure.
      #
      # @return [Integer] latency of Solr connection
      def self.ping
        ensure_connected!

        result = search_raw(q: "*:*", rows: 0)

        return nil if result.blank?
        result["responseHeader"]["QTime"]
      end

      # Retrieve the Solr connection object
      #
      # @return [void]
      private_class_method def self.ensure_connected!
        Thread.current[:solr_url] ||= ENV["SOLR_URL"]
        Thread.current[:solr_handle] ||= connect
      end

      # Make the actual Solr connection
      #
      # Read the appropriate settings and connect to the Solr server
      #
      # @return [RSolr::Client] the Solr connection object
      private_class_method def self.connect
        RSolr::Ext.connect(
          url: ENV["SOLR_URL"],
          timeout: Integer(ENV["SOLR_TIMEOUT"] || "120"),
          open_timeout: Integer(ENV["SOLR_TIMEOUT"] || "120")
        )
      end

      # Convert some parameters
      #
      # We want to allow users to pass 'Ruby-esque' symbols to this class, so
      # coerce all of the parameter keys to Java-style 'lowerCamelCase' here.
      #
      # @param [Hash] params the parameters to convert
      # @return [Hash] those parameters, converted from snake_case to camelCase
      private_class_method def self.camelize_params!(params)
        params.keys.each do |k|
          if k.to_s.include? "_"
            params[k.to_s.camelize(:lower)] = params.delete(k)
          end
        end
      end
    end
  end
end
