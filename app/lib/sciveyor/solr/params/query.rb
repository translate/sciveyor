# frozen_string_literal: true

module Sciveyor
  module Solr
    class Params
      # A class to encapsulate manipulations of field-value queries
      class Query
        # Convert a query from separate field and value to a query string
        #
        # This method is the inverse of `.field_value`.
        #
        # @param [String] field the field to search
        # @param [String] value the value to search for
        # @return [String] the query string
        def self.query_string(field, value)
          return nil if field.blank? || value.blank?
          field = field.to_s

          # Special handling for a few of these field types
          if field == "authors"
            authors_query(value)
          elsif field == "date"
            date_query(value)
          elsif field == "dismax"
            " {!edismax v=\"#{value.gsub("\"", '\"')}\"}"
          else
            "#{field}:(#{value})"
          end
        end

        # Convert a query from a query string to field and value
        #
        # This method is the inverse of `.query_string`.
        #
        # @param [String] query the query string
        # @return [Array<(String, String)>] the field and value
        def self.field_value(query)
          # There are three kinds of unusual search above; cleanly undo them all

          # dismax
          m = / *{!edismax v="([^"]+)"}/.match(query)
          return "dismax", m[1] if m && m[1]

          # authors
          if query =~ /}name_clean:/
            authors =
              query
                .split(" AND ")
                .map do |part|
                  m =
                    / *{!parent which="type:article"}name_clean:\(?([^)]+)\)?/.match(
                      part
                    )
                  if m.nil? || m.length < 2
                    # No idea how to parse this, bail
                    return nil, nil
                  end

                  # This is now all the lucene queries, OR'ed together, take the
                  # first one and clean it up
                  author = m[1].split(" OR ")[0]
                  author.delete_prefix!("\"")
                  author.delete_suffix!("\"")
                  author.gsub!("*", ".")

                  author
                end

            return "authors", authors.join(", ")
          end

          if query.start_with?("date:(")
            # Remove the field name and the parentheses, split
            dates =
              query[6..-2]
                .split(" OR ")
                .map do |part|
                  m =
                    /\[(\d\d\d\d)-01-01T00:00:00Z TO (\d\d\d\d)-12-31T23:59:59Z\]/.match(
                      part
                    )
                  if m.nil? || m.length < 3
                    # No idea how to parse this, bail
                    return nil, nil
                  end

                  # If the start and end years are the same, then it's a
                  # one-year search, otherwise it's a year-range search
                  if m[1] == m[2]
                    m[1]
                  else
                    "#{m[1]}-#{m[2]}"
                  end
                end

            return "date", dates.join(", ")
          end

          # Okay, in this case it must just be a normal query of the form
          # field:(value), so return that
          split = query.partition(":")
          return split[0], split[2].delete_prefix("(").delete_suffix(")")
        end

        # Split and clean up the authors parameter
        #
        # Authors can be passed as a list and are expected to be joined as an
        # AND query. This also utilizes the Lucene name functions we've defined
        # elsewhere.
        #
        # @param [String] value the authors search string
        # @return [String] the Solr query for this list of authors
        private_class_method def self.authors_query(value)
          authors =
            value
              .split(",")
              .map do |a|
                " {!parent which=\"type:article\"}name_clean:" +
                  Sciveyor::Documents::Author.new(name: a.strip).to_lucene
              end
          authors.join(" AND ")
        end

        # Get the query string matching the given array of year ranges.
        #
        # @param [String] year_ranges the list of year ranges
        # @return [String] query string for this set of year ranges
        private_class_method def self.date_query(year_ranges)
          # Strip whitespace, split on commas
          ranges = year_ranges.gsub(/\s/, "").split(",")
          year_queries = []

          ranges.each do |r|
            if r.include? "-"
              range_years = r.split("-")
              next unless range_years.size == 2
              next if range_years[0].match(/\A\d+\z/).nil?
              next if range_years[1].match(/\A\d+\z/).nil?

              year_queries << "[#{range_years[0]}-01-01T00:00:00Z TO #{range_years[1]}-12-31T23:59:59Z]"
            else
              next if r.match(/\A\d+\z/).nil?

              year_queries << "[#{r}-01-01T00:00:00Z TO #{r}-12-31T23:59:59Z]"
            end
          end

          return nil if year_queries.empty?
          "date:(#{year_queries.join(" OR ")})"
        end
      end
    end
  end
end
