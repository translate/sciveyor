# frozen_string_literal: true

module Sciveyor
  module Solr
    class Params
      # A class to encapsulate manipulations of filter querie
      class FilterQuery
        # Convert a facet from a user-friendly string to a Solr fq
        #
        # @param [String] facet the facet to convert
        # @return [String] the Solr fq value for this facet
        def self.facet_fq_string(facet)
          parts = facet.partition(":")

          field = parts[0]
          value = parts[2]

          value = value.delete_prefix("\"").delete_suffix("\"")
          return nil if value.empty?

          case field
          when "author"
            return "{!parent which=\"type:article\"}(+name:\"#{value}\")"
          when "journal"
            return "+type:article +journal:\"#{value}\""
          when "date"
            if value == "before"
              return "+type:article +date:[* TO 1800-01-01T00:00:00Z]"
            elsif value == "after"
              return "+type:article +date:[2020-01-01T00:00:00Z TO *]"
            else
              begin
                int = Integer(value, 10)
                return nil if int % 10 != 0
              rescue ArgumentError
                return nil
              end

              return(
                "+type:article +date:[#{value}-01-01T00:00:00Z TO #{value}-01-01T00:00:00Z+10YEARS]"
              )
            end
          else
            return nil
          end
        end

        # Convert a Solr fq string to a user-friendly facet
        #
        # This method is the inverse of `facet_fq_string`.
        #
        # @param [String] fq the Solr fq value to convert
        # @return [String] the user-friendly facet
        def self.fq_facet(fq)
          # Ignore the facet query built from the categories
          return nil if %r{/\* categories:} =~ fq

          # author
          m = /{!parent which="type:article"}\(\+name:"([^"]+)"\)/.match(fq)
          return "author:#{m[1]}" if m && m[1]

          # journal
          m = /\+type:article \+journal:"([^)]+)"/.match(fq)
          return "journal:#{m[1]}" if m && m[1]

          # date
          m = /\+type:article \+date:\[([0-9]+)/.match(fq)
          if m && m[1]
            value = m[1]
            value = "after" if value == "2020"
            return "date:#{value}"
          end

          m = /\+type:article \+date:\[\* /.match(fq)
          return "date:before" if m

          # Don't know how to parse this one; skip it
          nil
        end

        # Convert the array of categories to a Solr fq string
        #
        # This method is the inverse of `fq_categories`.
        #
        # @param [Array<Integer>] categories the categories to convert
        # @return [String] the Solr fq value for these categories
        def self.categories_fq_string(categories)
          journals =
            categories.collect do |id|
              category = Category.find(id)
              next if category.journals.empty?

              category.journals.map { |j| "\"#{j}\"" }
            end
          journals.compact!&.uniq!

          unless journals.empty?
            return(
              "+type:article +journal:(#{journals.join(" OR ")}) /* categories:#{categories.join(",")} */"
            )
          end
          nil
        end

        # Convert a Solr fq string to a list of category IDs
        #
        # This method is the inverse of `categories_fq_string`.
        #
        # @param [String] fq the Solr fq value to convert
        # @return [Array<Integer>] the list of category IDs
        def self.fq_categories(fq)
          m = %r{/\* categories:([0-9,]+) \*/}.match(fq)
          return nil unless m && m[1]

          m[1].split(",").map { |id| Integer(id, 10) }
        end
      end
    end
  end
end
