# frozen_string_literal: true

module Sciveyor
  module Solr
    # A representation of a Solr facet
    #
    # Solr facets arrive in a variety of formats, and thus have to be parsed in
    # a variety of ways.  This class attempts to handle all of that in the most
    # generic and extensible way possible.
    #
    # @!attribute query
    #   @return [String] the Solr filter query for this facet
    # @!attribute field
    #   @return [Symbol] the field that we are faceting on
    # @!attribute value
    #   @return [String] the value for this facet
    # @!attribute hits
    #   @return [Integer] the number of hits for this facet
    class Facet
      include Virtus.model(strict: true, required: false)
      include VirtusExt::Validator
      include Comparable

      attribute :query, String
      attribute :field, Symbol, required: true
      attribute :value, String, required: true
      attribute :hits, Integer, required: true

      # Compare facet objects appropriately given their field
      #
      # In general, this sorts first by count and then by value.
      #
      # @param [Facet] other object for comparison
      # @return [Integer] -1, 0, or 1, appropriately
      def <=>(other)
        return -(hits <=> other.hits) if hits != other.hits

        # Date values are special and should be sorted descending
        if field == :date
          dt = value_to_datetime
          other_dt = other.value_to_datetime

          return -(dt <=> other_dt)
        end

        # Sort everything else normally
        (value <=> other.value)
      end

      # Get a date-time for the given value
      #
      # This takes into account the 'before' and 'after' special strings.
      #
      # @return [DateTime] the converted date
      def value_to_datetime
        return nil unless field == :date

        if value == "before"
          return DateTime.new(1799, 1, 1)
        elsif value == "after"
          return DateTime.new(2021, 1, 1)
        else
          return DateTime.iso8601(value)
        end
      end

      private

      # Make sure that the options are consistent
      #
      # @return [void]
      def validate!
        # Make sure we have a valid field
        if field != :author && field != :journal && field != :date
          raise ArgumentError, "do not know how to handle facets on #{field}"
        end

        # Strip quotes from the value if present
        self.value = value[1..-2] if value[0] == "\"" && value[-1] == "\""

        # Construct the faceting query
        if field == :date
          if value == "before" || value == "after"
            self.query = "#{field}:#{value}"
          else
            self.query = "#{field}:#{value_to_datetime.year}"
          end
        else
          self.query = "#{field}:#{value}"
        end
      end
    end
  end
end
