# frozen_string_literal: true

module Sciveyor
  module Solr
    class Params
      # @return [Array<String>] the fields to search by, which correspond to
      #   the values stored in `values`
      attr_accessor :fields
      def fields=(v)
        @fields = v
        fields_values_to_q
      end

      # @return [Array<String>] the values to search for, which correspond to
      #   the fields stored in `fields`
      attr_accessor :values
      def values=(v)
        @values = v
        fields_values_to_q
      end

      # @return [String] the boolean operator used to combine the field/value
      #   searches
      attr_accessor :boolean
      def boolean=(v)
        @boolean = v
        fields_values_to_q
      end

      # @return [Array<String>] the faceted browsing queries by which to filter
      #   this search
      attr_accessor :facets
      def facets=(v)
        @facets = v
        facets_categories_to_fq
      end

      # @return [Array<Integer>] the IDs of categories by which to filter this
      #   search
      attr_accessor :categories
      def categories=(v)
        @categories = v
        facets_categories_to_fq
      end

      # @return [String] the Solr search query for this search, created from
      #   `fields` and `values`
      attr_reader :q

      # @return [Array<String>] the filter queries for this search, created
      #   from `facets` and `categories`
      attr_reader :fq

      # @return [Integer] the record at which to start returning results
      attr_accessor :offset

      # @return [Integer] the number of results to return
      attr_accessor :limit

      # @return [String] the sort key for this search
      attr_accessor :sort

      # @return [String] the Solr cursor mark for this search
      attr_accessor :cursor_mark

      # Build search parameters
      #
      # This class can be constructed either from a client request, or from the
      # representation of parameters saved in a dataset.
      #
      # If the former, we sanitize the parameters and construct the required
      # Solr queries.
      #
      # If the latter, we rebuild parameters from the saved representation in a
      # dataset, which consists only of an array of queries, a boolean
      # operator, and an array of filter queries.
      #
      # @param [ActionController::Parameters] params the request parameters
      # @param [Boolean] api_search if true, this is an API search, not a
      #   user-generated searchA
      # @param [Dataset] dataset the dataset
      # @return [SearchParams] the processed parameters
      def initialize(params: nil, api: false, dataset: nil)
        if params && dataset
          raise ArgumentError, "cannot pass both params and dataset"
        end

        if params
          # Start by sanitizing the parameters
          params = permit_params(params)

          # Remove any blank values (e.g., from form submissions)
          params.delete_if { |_, v| v.blank? }

          # Parse out into variables
          params_to_position(params, api)
          params_to_fq(params)
          params_to_search(params)
          return
        end

        if dataset
          # Save fq, boolean
          @fq = dataset.fq.dup
          @boolean = dataset.boolean

          # Reconstruct fields and values from the q array
          dataset_to_fields(dataset.q)
          fields_values_to_q

          # Reconstruct facets and categories from the fq
          dataset_to_facets_categories
          return
        end

        raise ArgumentError, "must pass either params or dataset"
      end

      # Convert these parameters to a Solr query
      #
      # @return [Hash] parameters to send to a Solr search
      def to_solr
        ret = {}

        ret[:sort] = @sort || default_sort
        unless @api
          # Make sure that any sort sent to Solr includes 'id asc' if we're
          # using the cursor-mark support, otherwise we get 400 errors from
          # Solr.
          ret[:sort] += ",id asc" if !ret[:sort].include?("id asc")
        end

        ret[:rows] = @limit || default_limit
        ret[:start] = @offset || 0
        ret[:cursor_mark] = @cursor_mark || "*" unless @api
        ret[:fq] = @fq if @fq
        ret[:q] = @q if @q

        ret
      end

      # Convert these parameters to dataset attributes
      #
      # This function returns a hash that can be passed to `Dataset.find_by` or
      # `Dataset.find_or_create_by`.
      #
      # @return [Hash] collection of dataset attributes
      def to_dataset
        ret = {}

        ret[:boolean] = @boolean || "and"
        ret[:fq] = @fq || []

        # Build the query array
        if @fields && @values
          ret[:q] = @fields
            .zip(@values)
            .map { |(field, value)| Query.query_string(field, value) }
        else
          ret[:q] = ["*:*"]
        end

        ret
      end

      # Return a permitted set of params to generate a new request
      #
      # @return [ActionController::Parameters] permitted request params
      def to_request
        hash = {}
        hash[:offset] = @offset if @offset
        hash[:limit] = @limit if @limit
        hash[:cursor_mark] = @cursor_mark if @cursor_mark && @cursor_mark != "*"
        hash[:sort] = @sort if @sort
        hash[:boolean] = @boolean if @boolean && @boolean != "and"
        hash[:fields] = @fields if @fields.present?
        hash[:values] = @values if @values.present?
        hash[:facets] = @facets if @facets.present?
        hash[:categories] = @categories if @categories.present?

        permit_params(ActionController::Parameters.new(hash))
      end

      private

      # Sanitize the Rails parameters we received from a request
      #
      # @param [ActionController::Params] params the incoming parameters
      # @return [ActionController::Params] the whitelisted parameters
      def permit_params(params)
        params.permit(
          :offset,
          :limit,
          :cursor_mark,
          :sort,
          :solr,
          :boolean,
          fields: [],
          values: [],
          facets: [],
          categories: []
        )
      end

      # Parse the sort, start, and rows values
      #
      # @param [Hash] params the params from the controller
      # @param [Boolean] api if true, this is an API query, not a standard
      #   HTML search query
      def params_to_position(params, api)
        @api = api
        @sort = params[:sort] if params[:sort]

        # If this is an API search, they are allowed to set offset and limit
        # to control the start and rows values; otherwise we are using cursor
        # support
        if @api
          offset = (Integer(params[:offset], exception: false) || 0).lbound(0)
          @offset = offset if offset != 0

          limit = (Integer(params[:limit], exception: false) || 10).lbound(1)
          @limit = limit if limit != 10
        else
          cursor_mark = params[:cursor_mark] || "*"
          @cursor_mark = cursor_mark if cursor_mark != "*"
        end
      end

      # Return the default sorting parameter
      #
      # Default sorting to relevance if there's a search, otherwise year.
      #
      # @return [String] the default sorting parameter
      def default_sort
        ret = @values.present? ? +"score desc" : +"date desc"

        # We must always include id in the sort string to use cursor
        # support
        ret += ",id asc" unless @api

        ret
      end

      # Return the default limit parameter
      #
      # We have to make the limit a bit larger if we're using infinite scroll,
      # or else there won't be enough items on screen.
      #
      # @return [Integer] the default limit parameter
      def default_limit
        @api ? 10 : 20
      end

      # Parse the faceted browsing and category parameters
      #
      # @param [Hash] params the params from the controller
      def params_to_fq(params)
        @facets = params[:facets].dup if params[:facets]

        if params[:categories]
          @categories = params[:categories].map { |c| Integer(c, 10) }
        end

        facets_categories_to_fq
      end

      # Convert the facets and categories to fq
      def facets_categories_to_fq
        fq = []

        # Convert facets from user-friendly mode to their underlying Solr
        # query syntax
        if @facets
          @facets.each do |facet|
            query = FilterQuery.facet_fq_string(facet)
            raise ActionController::BadRequest if query.nil?

            fq << query
          end
        end

        # And converting categories to facets
        if @categories
          cat_fq = FilterQuery.categories_fq_string(@categories)
          fq << cat_fq if cat_fq
        end

        if fq.empty?
          @fq = nil
        else
          @fq = fq
        end
      end

      # Parse the actual search parameters
      #
      # @param [Hash] params the params from the controller
      # @return [Hash] a hash with `:q` set
      def params_to_search(params)
        # Custom Solr query support overrides everything else
        if params[:solr]
          @q = params[:solr]
          return
        end

        @boolean = params[:boolean] == "or" ? "or" : "and"

        # Parse the fields and values arrays
        if params[:fields] && params[:values]
          if params[:fields].length != params[:values].length
            raise ArgumentError, "different number of fields and values"
          end

          @fields = params[:fields]
          @values = params[:values]
        end

        # Build the actual query
        fields_values_to_q
      end

      # Set the actual search query from the arrays of fields and values
      def fields_values_to_q
        @q = []

        if @fields && @values
          @fields
            .zip(@values)
            .each { |(field, value)| @q << Query.query_string(field, value) }
        end

        # Prune any empty/nil (invalid) queries
        @q.delete_if(&:blank?)

        # Build the final search query
        if @q.empty?
          @q = "*:*"
        else
          @q = @q.join(@boolean == "or" ? " OR " : " AND ")
        end
      end

      # Given an array of queries from a dataset, recreate fields and values
      def dataset_to_fields(queries)
        queries.each do |q|
          field, value = Query.field_value(q)

          if field && value
            @fields ||= []
            @fields << field

            @values ||= []
            @values << value
          end
        end
      end

      # Given fq and categories, parse the facets values
      #
      # A dataset stores its `fq` array (including both facets and categories).
      # Recreate the `facets` and `categories` arrays on this basis here.
      #
      # Note that this is really only useful for the purposes of displaying
      # things to users, so we don't really worry if something goes wrong here.
      def dataset_to_facets_categories
        @fq.each do |fq|
          facet = FilterQuery.fq_facet(fq)
          next if facet.nil?

          @facets ||= []
          @facets << facet
        end

        @fq.each do |fq|
          ids = FilterQuery.fq_categories(fq)
          next if ids.nil?

          @categories = ids
          break
        end
      end
    end
  end
end
