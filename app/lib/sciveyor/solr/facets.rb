# frozen_string_literal: true

module Sciveyor
  module Solr
    # The list of all facets returned by Solr
    #
    # @!attribute [r] all
    #   @return [Array<Sciveyor::Solr::Facet>] all the facet objects
    class Facets
      attr_reader :all

      # Create parameters for a link to search with the given set of facets
      #
      # All parameters other than `:facets` are simply duplicated.
      #
      # @param [Sciveyor::Search::Params] search_params the active parameters
      # @param [Array<Facet>] facets the facets to link to
      # @return [Sciveyor::Search::Params] params for a search for these
      #   facets
      def self.search_params(search_params, facets)
        ret = search_params.dup

        if facets.empty?
          ret.facets = nil
          return ret
        end

        ret.facets = facets.map(&:query)
        ret
      end

      # Return a list of facets that are active given these parameters
      #
      # @param [Sciveyor::Search::Params] search_params the active parameters
      # @return [Array<Facet>] the active facets
      def active(search_params)
        return [] if empty? || search_params.facets.blank?

        [].tap do |ret|
          search_params.facets.each { |query| ret << for_query(query) }
          ret.compact!
        end
      end

      # Get all facets for a given field
      #
      # @param [Symbol] field the field to retrieve facets for
      # @return [Array<Sciveyor::Solr::Facet>] all facets for this field
      def for_field(field)
        @all.select { |f| f.field == field.to_sym }
      end

      # Get all facets for a given field, sorted
      #
      # @param [Symbol] field the field to retrieve sorted facets for
      # @return [Array<Sciveyor::Solr::Facet>] sorted facets for this field
      def sorted_for_field(field)
        for_field(field).sort
      end

      # Find a facet by its query parameter
      #
      # @param [String] query the query to search for
      # @return [Sciveyor::Solr::Facet] the facet for this query
      def for_query(query)
        all.find { |f| f.query == query }
      end

      # Return true if there are no facets
      #
      # @return [Boolean] true if +all.empty?+
      def empty?
        return true unless @all
        @all.empty?
      end

      # Initialize from the facet parameter from RSolr::Ext
      #
      # @param [Array<RSolr::Ext::Facet>] facets the facet parameters
      def initialize(facets)
        @all = []

        return if facets.nil?

        # Extract the facet info for each of authors, journals, and date
        if facets["authors"] && facets["authors"]["buckets"]
          facets["authors"]["buckets"].each do |f|
            next unless f["val"]
            next unless f["count"]
            next if f["count"] == 0

            @all << Facet.new(field: :author, value: f["val"], hits: f["count"])
          end
        end

        if facets["journals"] && facets["journals"]["buckets"]
          facets["journals"]["buckets"].each do |f|
            next unless f["val"]
            next unless f["count"]
            next if f["count"] == 0

            @all << Facet.new(
              field: :journal,
              value: f["val"],
              hits: f["count"]
            )
          end
        end

        if facets["date"]
          if facets["date"]["buckets"]
            facets["date"]["buckets"].each do |f|
              next unless f["val"]
              next unless f["count"]
              next if f["count"] == 0

              @all << Facet.new(field: :date, value: f["val"], hits: f["count"])
            end
          end
          if facets["date"]["before"]
            count = facets["date"]["before"]["count"]
            if count && count > 0
              @all << Facet.new(field: :date, value: "before", hits: count)
            end
          end
          if facets["date"]["after"]
            count = facets["date"]["after"]["count"]
            if count && count > 0
              @all << Facet.new(field: :date, value: "after", hits: count)
            end
          end
        end
      end
    end
  end
end
