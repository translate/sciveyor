# frozen_string_literal: true

module Sciveyor
  module Analysis
    # Code for counting the occurrences of a term in a dataset, grouped
    #
    # @!attribute term
    #   @return [String] the term to search for
    # @!attribute field
    #   @return [Symbol] the field to group by
    # @!attribute dataset
    #   @return [Dataset] if set, the dataset to analyze (else the entire
    #     corpus)
    class CountTermsByField
      include Service
      include Virtus.model(strict: true, required: false, nullify_blank: true)

      attribute :term, String, required: true
      attribute :field, Symbol
      attribute :dataset, Dataset

      # Count term occurrences, grouping by a field
      #
      # This function takes a term, searches through a dataset (or the corpus)
      # for all the occurrences of that term, takes the number of term
      # occurrences in the dataset, and groups those occurrences by another
      # field of interest (e.g., by year, by journal, etc.).  The result is
      # returned in a hash.
      #
      # @todo This function should support the same kind of work with names
      #   that we have in Sciveyor::Documents::Author.
      #
      # @return [Hash<String, Integer>] number of documents in each grouping
      def call
        ids = dataset ? grouped_ids_dataset : grouped_ids_corpus
        ids_to_term_counts(ids)
      end

      private

      # Group the IDs in a dataset manually by field
      #
      # @return [Hash<String, Array<String>>] list of IDs for each group
      def grouped_ids_dataset
        ret = {}
        total = dataset.document_count

        enum = Sciveyor::Datasets::DocumentEnumerator.new(dataset: dataset)
        enum.each_with_index do |doc, i|
          key = map_document_field(field, doc.send(field))
          ret[key] ||= []
          ret[key] << doc.id
        end

        ret
      end

      # Group the IDs in the entire corpus by field
      #
      # @return [Hash<String, Array<String>>] list of IDs for each group
      def grouped_ids_corpus
        ret = {}
        start = 0

        num_docs = 0
        total_docs = Sciveyor::Solr::CorpusStats.new.size

        loop do
          group_result =
            Sciveyor::Solr::Connection.search_raw(
              :q => "fullText_clean:\"#{term}\"",
              :group => "true",
              "group.field" => field.to_s,
              :fl => "id",
              :facet => "false",
              :start => start.to_s,
              :rows => 1
            )

          # These conditions would indicate a malformed Solr response
          break unless group_result.dig("grouped", field.to_s, "matches")

          grouped = group_result["grouped"][field.to_s]
          break if grouped["matches"].zero?

          groups = grouped["groups"]
          break unless groups

          # This indicates that we're out of records
          break if groups.empty?

          # Get the group
          group = groups[0]

          # Run a new query to get all of the IDs
          key = group["groupValue"]
          group_size = group["doclist"]["numFound"]

          ids_result =
            Sciveyor::Solr::Connection.search_raw(
              :q => "fullText_clean:\"#{term}\"",
              :group => "true",
              "group.field" => field.to_s,
              :fl => "id",
              :facet => "false",
              :start => start.to_s,
              :rows => 1,
              "group.limit" => group_size
            )

          # Malformed Solr response
          break unless ids_result.dig("grouped", field.to_s, "groups")

          # Turn the documents list into an IDs list
          id_group = ids_result["grouped"][field.to_s]["groups"][0]
          mapped = map_document_field(field, key)
          ret[mapped] ||= []
          ret[mapped] += id_group["doclist"]["docs"].map { |doc| doc["id"] }

          # Get the next group
          start += 1
        end

        ret
      end

      # Map the value of the field before grouping
      #
      # This transparently implements support for grouping by year when :date is passed.
      #
      # @param [Symbol] field the field we're mapping
      # @param [String] value the field value
      # @return [String] the mapped field value
      def map_document_field(field, value)
        return value unless field == :date

        value = DateTime.iso8601(value) if value.is_a?(String)

        return value.year
      end

      # Fill in zeros for any missing values in the counts
      #
      # If counts has numeric keys, we'll actually fill in the intervening
      # values.
      #
      # @param [Hash<String, Integer>] counts the counts queried
      # @return [Hash<String, Integer>] the counts with intervening values set
      #   to zero
      def zero_intervening(counts)
        return {} if counts.empty?

        # See if we have numeric keys
        begin
          Integer(counts.keys.first)
        rescue ArgumentError
          # We aren't using this to do any non-numeric grouping yet; this code
          # isn't tested.
          # :nocov:
          return counts
          # :nocov:
        end

        # Actually fill in all of the numerically intervening years
        range = counts.keys.minmax
        Range.new(*range).each { |k| counts[k] ||= 0 }

        counts
      end

      # Convert a list of grouped IDs to term counts
      #
      # Query all the documents listed, get their counts for the term of
      # interest, and return them as a new hash.
      #
      # @param [Hash<String, Array<String>>] ids the grouped IDs to fetch
      # @return [Hash<String, Integer>] grouped term counts by field
      def ids_to_term_counts(ids)
        ret = {}
        total = ids.size

        ids.each_with_index do |(key, arr), i|
          ret[key] = 0

          arr.each do |id|
            doc = Document.find(id, term_vectors: true)
            vec = doc.term_vectors[@term]

            ret[key] += vec[:tf] if vec
          end
        end

        zero_intervening(ret)
      end
    end
  end
end
