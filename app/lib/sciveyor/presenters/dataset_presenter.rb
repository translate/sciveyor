# frozen_string_literal: true

module Sciveyor
  module Presenters
    # Code for formatting attributes of a Dataset object
    class DatasetPresenter
      include Virtus.model(strict: true, required: true)
      attribute :dataset, Dataset

      # Get a human-readable version of a query string
      #
      # @param [String] q the query to format
      # @return [String] a decorated version of the query
      def q_string(q)
        # Build a pseudo-dataset containing only this value, so that we can
        # detect when the parsing fails
        fake_dataset = Struct.new(:q, :boolean, :fq).new([q], "and", [])
        search_params ||= Sciveyor::Solr::Params.new(dataset: fake_dataset)

        field = search_params.fields&.at(0)
        value = search_params.values&.at(0)
        return q if field.blank? || value.blank?

        field_pres = nil
        Class
          .new
          .extend(SearchHelper)
          .advanced_search_fields
          .each do |name, f|
            if f.to_s == field
              field_pres = name
              break
            end
          end
        return q unless field_pres

        field_pres + ": " + value
      end

      # Get a human-readable version of the faceted browsing parameters
      #
      # @return [Array<String>] an array of decorated facet strings
      def fq_string
        return nil if dataset.fq.blank?

        search_params ||= Sciveyor::Solr::Params.new(dataset: dataset)

        ret = []

        # Decorated representation of facets
        search_params.facets.each do |f|
          field, _, value = f.partition(":")
          next if field.blank? || value.blank?

          # We need to convert dates back to datetimes here for the facet
          # parser
          value = "#{value}-01-01T00:00:00Z" if field == "date"

          f = Sciveyor::Solr::Facet.new(field: field, value: value, hits: 0)
          presenter = FacetPresenter.new(facet: f)
          ret << "#{presenter.field_label}: #{presenter.label}"
        end

        # Decorated representation of categories
        if search_params.categories
          names =
            search_params.categories.map do |id|
              category = Category.find_by(id: id)

              I18n.t("datasets.show.cat_deleted", id: id) if category.nil?
              category.name
            end

          ret << Category.model_name.human + ": " + names.join(", ")
        end

        ret
      end
    end
  end
end
