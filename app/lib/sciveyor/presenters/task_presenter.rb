# frozen_string_literal: true

module Sciveyor
  module Presenters
    # Code for formatting attributes of a Task object
    class TaskPresenter
      include Virtus.model(strict: true, required: true)
      attribute :task, ::Task

      # A displayable list of the names of the datasets for this task
      #
      # @return [String] list of datasets
      def dataset_list
        task.datasets.map { |d| d.name_for(task.user) }.to_sentence
      end
    end
  end
end
