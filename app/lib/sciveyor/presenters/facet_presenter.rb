# frozen_string_literal: true

module Sciveyor
  module Presenters
    # Code for formatting attributes on a Facet object
    class FacetPresenter
      include Virtus.model(strict: true, required: true)
      attribute :facet, Solr::Facet

      # Get a human-readable version of the `value` attribute
      #
      # The facets for years are returned in Solr format, looking something
      # like `[1900 TO 1910]`. This function makes sure we convert from those
      # to a readable format for display
      #
      # @return [String] the `value` attribute, in human-readable form
      def label
        case facet.field
        when :author
          facet.value
        when :journal
          facet.value
        when :date
          year_label
        else
          # :nocov:
          raise ArgumentError,
                "do not know how to handle facets on #{facet.field}"
          # :nocov:
        end
      end

      # Get a translated version of the `field` attribute
      #
      # @return [String] the `field` attribute, in human-readable form
      def field_label
        case facet.field
        when :author
          I18n.t("search.index.author_short")
        when :journal
          I18n.t("search.index.journal_short")
        when :date
          I18n.t("search.index.date_short")
        else
          # :nocov:
          raise ArgumentError,
                "do not know how to handle facets on #{facet.field}"
          # :nocov:
        end
      end

      private

      # Format a label suitable for displaying a year facet
      #
      # @return [String] the human-readable form of a year facet query
      def year_label
        @year_label ||=
          begin
            if facet.value == "before"
              I18n.t("search.index.date_before_1800")
            elsif facet.value == "after"
              I18n.t("search.index.date_after_2020")
            else
              year = facet.value_to_datetime.year
              "#{year}–#{year + 9}"
            end
          end
      end
    end
  end
end
