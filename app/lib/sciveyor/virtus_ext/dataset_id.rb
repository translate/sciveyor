# frozen_string_literal: true

module Sciveyor
  module VirtusExt
    # A class to encapsulate a dataset ID, which may need to be looked up from
    # a string
    class DatasetId < Virtus::Attribute
      # Coerce the ID to a Dataset
      #
      # @param [Object] value the object to coerce
      # @return [Dataset] dataset
      def coerce(value)
        return nil if value.blank?
        return value if value.is_a?(Dataset)
        return Dataset.find(value) if value.is_a?(Integer)

        unless value.is_a?(String)
          raise ArgumentError, "cannot create dataset from #{value.class}"
        end

        return GlobalID::Locator.locate(value) if value.start_with?("gid://")
        Dataset.find_by!(uuid: value)
      end
    end
  end
end
