# frozen_string_literal: true

module SearchHelper
  # Return the text to put in the regular search box
  #
  # If we only have one field set, and it's dismax, then show it back to the
  # user like Google would. Otherwise, flag an advanced search.
  #
  # @return [String] the text to use as the search box value
  def search_box_value
    if params[:fields].blank? && params[:values].blank?
      ""
    elsif params[:fields]&.length == 1 && params[:fields][0] == "dismax" &&
          params[:values][0].present?
      params[:values][0]
    else
      I18n.t("search.index.adv_search_placeholder")
    end
  end

  # Return the fields you can choose from on the advanced search apge
  #
  # @return [Hash<String, Symbol>] a list of advanced search fields and
  #   their corresponding label strings
  def advanced_search_fields
    {
      I18n.t("search.advanced.dismax") => :dismax,
      I18n.t("search.advanced.fulltext") + " " +
        I18n.t("search.advanced.type_clean") =>
        :fullText_clean,
      I18n.t("search.advanced.fulltext") + " " +
        I18n.t("search.advanced.type_stem") =>
        :fullText_stem,
      I18n.t("search.advanced.fulltext") + " " +
        I18n.t("search.advanced.type_lem") =>
        :fullText_lem,
      Document.human_attribute_name(:title) + " " +
        I18n.t("search.advanced.type_clean") =>
        :title_clean,
      Document.human_attribute_name(:title) + " " +
        I18n.t("search.advanced.type_stem") =>
        :title_stem,
      Document.human_attribute_name(:title) + " " +
        I18n.t("search.advanced.type_lem") =>
        :title_lem,
      I18n.t("search.advanced.abstract") + " " +
        I18n.t("search.advanced.type_clean") =>
        :abstract_clean,
      I18n.t("search.advanced.abstract") + " " +
        I18n.t("search.advanced.type_stem") =>
        :abstract_stem,
      I18n.t("search.advanced.abstract") + " " +
        I18n.t("search.advanced.type_lem") =>
        :abstract_lem,
      Document.human_attribute_name(:authors) => :authors,
      Document.human_attribute_name(:journal) + " " +
        I18n.t("search.advanced.type_clean") =>
        :journal_clean,
      Document.human_attribute_name(:date) => :date,
      Document.human_attribute_name(:volume) => :volume,
      Document.human_attribute_name(:number) => :number,
      Document.human_attribute_name(:pages) => :pages
    }
  end

  # Get the list of facet links for one particular field
  #
  # This function takes the facets from the `Document` class, checks them
  # against the active facets, and creates a set of list items.
  #
  # @param [Sciveyor::Solr::Params] search_params current search parameters
  # @param [Sciveyor::Solr::Facets] facets the facets returned
  # @param [Symbol] field the field to return links for
  # @return [String] the built markup of addition links for this field
  def facet_addition_links(search_params, facets, field)
    # Get the facets for this field
    active_facets = facets.active(search_params)
    field_facets = (facets.sorted_for_field(field) - active_facets).take(5)

    # Build the return value
    tags =
      field_facets.map do |f|
        p = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        new_facets = active_facets + [f]
        new_params =
          Sciveyor::Solr::Facets.search_params(search_params, new_facets)

        facet_add_link(new_params, f.hits.to_s, p.label)
      end

    safe_join(tags)
  end

  def facet_removal_links(search_params, facets)
    active_facets = facets&.active(search_params)
    tags = []

    # Remove all link
    if search_params.categories || search_params.facets
      remove_params = search_params.dup
      remove_params.categories = nil
      remove_params.facets = nil

      tags << facet_remove_link(
        remove_params,
        I18n.t("search.index.remove_all")
      )
    end

    active_facets&.each do |f|
      other_facets = active_facets.reject { |x| x == f }
      other_params =
        Sciveyor::Solr::Facets.search_params(search_params, other_facets)

      p = Sciveyor::Presenters::FacetPresenter.new(facet: f)
      tags << facet_remove_link(other_params, "#{p.field_label}: #{p.label}")
    end

    safe_join(tags)
  end

  def category_addition_tree(search_params, roots = Category.roots)
    tags =
      roots.map do |root|
        content_tag(:li) { category_add_link(search_params, root) }
      end

    safe_join(tags)
  end

  def category_removal_links(search_params)
    tags =
      Category
        .active(search_params)
        .map do |category|
          facet_remove_link(
            category.toggle_search_params(search_params),
            "#{Category.model_name.human}: #{category.name}"
          )
        end

    safe_join(tags)
  end

  private

  def facet_add_link(search_params, hits, label)
    link_to search_path(search_params.to_request), class: "nav-link" do
      hits =
        content_tag(
          :div,
          hits.to_s,
          class: "float-end badge bg-light text-dark mt-1"
        )

      safe_join([hits, label])
    end
  end

  def category_add_link(search_params, category)
    link =
      link_to search_path(
                category.toggle_search_params(search_params).to_request
              ),
              class: "nav-link" do
        contents = [
          check_box_tag(
            "category_#{category.to_param}",
            "1",
            category.enabled?(search_params),
            disabled: true
          ),
          category.name
        ]

        safe_join(contents)
      end

    tags = [link]
    if category.children.exists?
      tags << content_tag(
        :ul,
        category_addition_tree(search_params, category.children)
      )
    end

    safe_join(tags)
  end

  def facet_remove_link(search_params, label)
    link_to search_path(search_params.to_request), class: "nav-link" do
      close = content_tag(:div, close_icon, class: "float-end")

      safe_join([close, label])
    end
  end
end
