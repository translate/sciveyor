# frozen_string_literal: true

module Datasets
  module TasksHelper
    # Create a link to download a given file, by content type
    #
    # @param [Task] task the task to download from
    # @param [String] content_type the content type to download
    # @return [String] link to the file download (or `nil` if none)
    def task_download_url(task:, content_type:)
      file = task.upload_for(content_type)
      return nil unless file

      static_file_url(file.filename)
    end
  end
end
