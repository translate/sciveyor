# frozen_string_literal: true

xml.instruct!
opts = {}
opts[:id] = params[:id] if params[:id].present?
xml.formats(opts) do
  Sciveyor::Documents::Serializers::Base.available.each do |k|
    klass = Sciveyor::Documents::Serializers::Base.for(k)
    xml.format(name: k.to_s,
               type: Mime::Type.lookup_by_extension(k).to_s,
               docs: klass.url)
  end
end
