# frozen_string_literal: true

# Send notification emails to users
#
# This mailer is responsible for sending emails to users when their analysis
# tasks complete.
class UserMailer < ApplicationMailer
  # Email users that their jobs have finished
  #
  # @param [String] email the address to send the mail
  # @param [Task] task the task that just finished
  # @return [void]
  def job_finished_email(email, task)
    # Only way to get locals into the mail templates is to set instance vars
    @task = task
    @presenter = Sciveyor::Presenters::TaskPresenter.new(task: @task)

    bootstrap_mail(
      from: "noreply@sciveyor.com",
      to: email,
      subject: I18n.t("user_mailer.job_finished.subject")
    )
  end
end
