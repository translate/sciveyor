# frozen_string_literal: true

# Base class for all application mailers
class ApplicationMailer < ActionMailer::Base
  default from: "noreply@sciveyor.com"
  layout "mailer"
end
