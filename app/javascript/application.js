import Rails from "@rails/ujs";
Rails.start();

import "@popperjs/core";
import "bootstrap";

import "fa-solid";
import "fontawesome";

import "util/global";
import "util/auto_fetch";
import "util/cookies";
