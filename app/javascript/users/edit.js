// ---------------------------------------------------------------------------
// AJAX list of library links from the libraries controller
import { onready } from "util/onready";

onready(function () {
  var libraryList = document.querySelector("div#library-list");

  // If there's a library list at all, we want to refresh its contents (e.g.,
  // after the user closes the "add new library" dialog box)
  if (!libraryList) return;

  var ajax_url = libraryList.getAttribute("data-sv-fetch-url");

  var request = new XMLHttpRequest();
  request.open("GET", ajax_url, true);
  request.onload = function () {
    if (this.status >= 200 && this.status < 400) {
      var libraryList = document.querySelector("div#library-list");
      libraryList.innerHTML = this.response;
    }
  };

  request.send();
});
