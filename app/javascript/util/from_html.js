// Return an array of elements created from an HTML string fragment
export function fromHTML(str) {
  var doc = document.implementation.createHTMLDocument("");
  doc.body.innerHTML = str;

  return Array.from(doc.body.children);
}

// Append an array of elements from an HTML string fragment to element
export function appendFromHTML(element, str) {
  var ret = undefined;

  for (var elt of fromHTML(str)) {
    element.appendChild(elt);
    if (!ret) ret = elt;
  }

  return ret;
}
