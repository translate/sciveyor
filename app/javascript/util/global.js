import { Modal } from "bootstrap";
import { onready } from "util/onready";
import { appendFromHTML } from "util/from_html";

// Enable client-side form validation for all forms within element
function checkValidation(element) {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = element.querySelectorAll(".needs-validation");

  // Loop over them and prevent submission
  for (var form of forms) {
    form.addEventListener("submit", function (e) {
      if (e.target.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
      }

      e.target.classList.add("was-validated");

      // Remove all server-side validation, as the user has now changed the
      // form
      for (var elt of form.querySelectorAll(".is-invalid")) {
        elt.classList.remove("is-invalid");
      }
      for (var elt of form.querySelectorAll(".server-errors")) {
        elt.classList.add("d-none");
      }

      // Show the client-side error messages, which will be displayed if the
      // client-side validation failed
      for (var elt of form.querySelectorAll(".client-errors")) {
        elt.classList.remove("d-none");
      }
    });
  }
}

// Load as AJAX any modal dialog boxes that are suitably marked up
document.addEventListener("click", function (e) {
  if (!e.target || !e.target.classList.contains("ajax-modal")) return;
  e.preventDefault();

  var context = e.target;
  var id = context.getAttribute("id");
  var url = context.getAttribute("href");
  var modalElt = document.querySelector(
    "#modal-container #ajax-" + id + "-modal"
  );

  if (!modalElt) {
    var container = document.querySelector("#modal-container");

    // FIXME: should add the 'fade' class after 'modal' here, but it's causing
    // strange bugs in system tests.
    appendFromHTML(
      container,
      "<div id='ajax-" +
        id +
        "-modal' class='modal' tabindex='-1' role='dialog'></div>"
    );

    modalElt = document.querySelector(
      "#modal-container #ajax-" + id + "-modal"
    );
  }

  var request = new XMLHttpRequest();
  request.open("GET", url, true);

  request.onload = function () {
    if (this.status >= 200 && this.status < 400) {
      modalElt.innerHTML = this.response;
      checkValidation(modalElt);
      new Modal(modalElt).show();
    }
  };

  request.send();
});

onready(function () {
  // Set up Bootstrap validation for any forms currently shown
  checkValidation(document);

  // Submit the sign-in form that's on all pages on enter
  var sign_ins = document.querySelectorAll(".dropdown-sign-in-form input");
  for (var input of sign_ins) {
    input.addEventListener("keydown", function (e) {
      if (e.keyCode == 13 || e.keyCode == 10) {
        this.form.submit();
        return false;
      }
    });
  }
});
