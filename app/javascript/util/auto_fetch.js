// ---------------------------------------------------------------------------
// General support for automatically refreshing AJAX frames (loaded globally)
import { onready } from "util/onready";

function doAutoFetch(element) {
  if (!window.performAutoFetch) return;

  var request = new XMLHttpRequest();
  request.open("GET", element.getAttribute("data-sv-fetch-url"), true);

  request.onload = function () {
    if (this.status >= 200 && this.status < 400) {
      element.innerHTML = this.response;
      window.setTimeout(doAutoFetch, 4000, element);
    }
  };

  request.send();
}

onready(function () {
  window.performAutoFetch = true;

  for (var elt of document.querySelectorAll("[data-sv-fetch=auto]")) {
    doAutoFetch(elt);
  }
});
