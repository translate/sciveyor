// ---------------------------------------------------------------------------
// Force-layout graph support for the Network results page
// NOTE: This is broken, but this code is very slow anyway and so disabled in
// production.

import {
  forceSimulation,
  forceManyBody,
  forceLink,
  forceCenter,
} from "d3-force";
import { select, selectAll } from "d3-selection";
import { onready } from "util/onready";

onready(function () {
  // Get the elements we need
  if (!document.querySelector(".network_graph")) return;

  var results = JSON.parse(window.json_data);

  // Make the vis object
  var w = document.querySelector("html").clientWidth;
  if (w > 750) {
    w = 750;
  }

  var h;
  if (w > 480) {
    h = 480;
  } else {
    h = w;
  }

  var nodes = results.d3_nodes;
  for (var i = 0; i < nodes.length; i++) {
    nodes[i].id = i;
  }
  var links = results.d3_links;
  for (var i = 0; i < links.length; i++) {
    links[i].id = i;
    links[i].source = nodes[links[i].source];
    links[i].target = nodes[links[i].target];
  }

  // Thanks to https://gist.github.com/sfrdmn/1437516 for this mouseover code,
  // tweaked for our purposes
  /*var mouseOverFunction = function () {
    var circle = select(this);
    circle.attr("fill", "red");

    select(".network_graph_box").style("display", "block");
    select(".network_graph_box p").html(
      "<b>" +
        results.word_stem +
        ":</b> " +
        circle.data()[0].name +
        "<br>" +
        results.word_forms +
        ": " +
        circle.data()[0].forms.join(" ")
    );
  };

  var mouseOutFunction = function () {
    var circle = d3.select(this);
    circle.attr("fill", "black");

    select(".network_graph_box").style("display", "none");
  };

  var mouseMoveFunction = function () {
    var coord = d3.mouse(this);
    select(".network_graph_box")
      .style("left", coord[0] + 15 + "px")
      .style("top", coord[1] + "px");
  };*/

  /*var zoom = function () {
    svg.attr(
      "transform",
      "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")"
    );
    };*/

  var fNode = forceManyBody();
  var fLink = forceLink(links); /*.strength(function (d) {
    return 1.0 - d.strength;
  });*/

  var simulation = forceSimulation(nodes)
    .force("link", fLink)
    .force("charge", fNode)
    .force("center", forceCenter())
    .on("tick", ticked);

  var svg = select(".network_graph")
    .insert("svg", ":first-child")
    .attr("width", w)
    .attr("height", h)
    .attr("viewBox", [-w / 2, -h / 2, w, h])
    .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

  /*svg
    .append("rect")
    .attr("class", "overlay")
    .attr("width", w)
    .attr("height", h);*/

  const link = svg
    .append("g")
    .attr("stroke", "#999")
    //.attr("stroke-width", (d) => d.strength * 1.75 + 0.25)
    .selectAll("line")
    .data(links)
    .join("line");

  // Get the maximum node weight
  /*var max_weight = 0;
  for (var i = 0; i < nodes.length; i++) {
    if (nodes[i].weight > max_weight) {
      max_weight = nodes[i].weight;
    }
  }*/

  const node = svg
    .append("g")
    .attr("fill", "#006DB0")
    .attr("stroke", "#fff")
    .attr("stroke-width", 1.5)
    .selectAll("circle")
    .data(nodes)
    .join("circle")
    .attr("r", 7);
  /*.attr("r", function (d) {
      return ((d.weight - 1) / (max_weight - 1)) * 4.0 + 3.0;
    })*/
  /*.on("mouseover", mouseOverFunction)
    .on("mouseout", mouseOutFunction);*/

  var ticked = function () {
    link
      .attr("x1", (d) => d.source.x)
      .attr("y1", (d) => d.source.y)
      .attr("x2", (d) => d.target.x)
      .attr("y2", (d) => d.target.y);

    node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
  };

  var promise = new Promise((resolve) => setTimeout(resolve, 10000));
  promise.then(() => simulation.stop());
});
