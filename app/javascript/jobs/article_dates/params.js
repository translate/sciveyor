// ---------------------------------------------------------------------------
// ArticleDates job parameters page
import { toggleVisAndDisabled } from "util/show_hide";

document.addEventListener("change", function (e) {
  if (e.target && e.target.getAttribute("name") == "job_params[normalize]") {
    toggleVisAndDisabled("#normalize-controls");
  }
});
