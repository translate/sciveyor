// ---------------------------------------------------------------------------
// Graph support for the TermDates results page
import { onready } from "util/onready";

function drawTermDatesGraph() {
  // Get the elements we need
  var graphContainer = document.querySelector(".term_dates_graph");
  var tableContainer = document.querySelector(".term_dates_table");
  if (!graphContainer || !tableContainer) return;

  var results = JSON.parse(window.json_data);

  // Make a DataTable object
  var data = new google.visualization.DataTable();
  var rows = results.data;

  data.addColumn("number", results.date_header);
  data.addColumn("number", results.value_header);
  data.addRows(rows);

  // Make the line chart object
  var w = document.querySelector("html").clientWidth;
  if (w > 750) {
    w = 750;
  }

  var h;
  if (w > 480) {
    h = 480;
  } else {
    h = w;
  }

  var options = {
    width: w,
    height: h,
    legend: { position: "none" },
    hAxis: { format: "####", title: results.date_header },
    vAxis: { title: results.value_header },
    pointSize: 3,
  };

  var chart = new google.visualization.LineChart(graphContainer);
  chart.draw(data, options);
  google.visualization.events.trigger(chart, "updatelayout");

  // Make a pretty table object
  var table = new google.visualization.Table(tableContainer);
  table.draw(data, {
    page: "enable",
    pageSize: 20,
    sortColumn: 0,
    width: "20em",
  });
}

onready(function () {
  // Get the elements we need
  if (
    !document.querySelector(".term_dates_graph") ||
    !document.querySelector(".term_dates_table")
  )
    return;

  // Load the external libraries we need
  if (google.charts === undefined) return;
  google.charts.load("current", { packages: ["corechart", "table"] });
  google.charts.setOnLoadCallback(drawTermDatesGraph);
});
