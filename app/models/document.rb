# frozen_string_literal: true

# Representation of a document in the Solr database
#
# This class provides an ActiveRecord-like model object for documents hosted in
# the Sciveyor Solr backend.  It abstracts both single-document retrieval and
# document searching in class-level methods, and access to the data provided by
# Solr in instance-level methods and attributes.
#
# @!attribute [r] schema
#   @return [String] the schema URL (always `https://data.sciveyor.com/schema`)
# @!attribute [r] version
#   @return [Integer] the schema version
# @!attribute [r] id
#   @raise [RecordInvalid] if the id is missing (validates :presence)
#   @return [String] the id of this document
# @!attribute [r] doi
#   @return [String] the DOI (Digital Object Identifier) of this document
# @!attribute [r] externalIds
#   @return [Array<String>] a list of external identifiers for this document
#     (formatted as `type:value`)
#
# @!attribute [r] license
#   @return [String] the human-readable name of the document's license
# @!attribute [r] licenseUrl
#   @return [String] a URL referencing the document's license terms
# @!attribute [r] dataSource
#   @return [String] a description of where this document's data was obtained
# @!attribute [r] dataSourceUrl
#   @return [String] a URL referencing the document's data source description
# @!attribute [r] dataSourceVersion
#   @return [Number] the version of this document data
#
# @!attrubte [r] type
#   @return [String] the type of this document (currently always `author`)
#
# @!attribute [r] authors
#   @return [Sciveyor::Documents::Authors] the document's authors, parsed into
#     `Author` objects
#
# @!attribute [r] title
#   @return [String] the title of this document
# @!attribute [r] journal
#   @return [String] the journal in which this document was published
# @!attribute [r] date
#   @return [DateTime] the date on which this document was published
# @!attribute [r] dateElectronic
#   @return [DateTime] the date on which this document was published
#     electronically
# @!attribute [r] dateAccepted
#   @return [DateTime] the date on which this document was accepted
# @!attribute [r] dateReceived
#   @return [DateTime] the date on which this document was received
# @!attribute [r] volume
#   @return [String] the journal volume number in which this document was
#     published
# @!attribute [r] number
#   @return [String] the journal issue number in which this document was
#     published
# @!attribute [r] pages
#   @return [String] the page numbers in the journal of this document, in the
#     format 'start-end'
#
# @!attribute [r] keywords
#   @return [Array<String>] a list of this document's keywords (provided by
#     the author)
# @!attribute [r] tags
#   @return [Array<String>] a list of this document's tags (provided by the
#     journal or other services)
#
# @!attribute [r] term_vectors
#   Term vectors for this document
#
#   The Solr server returns a list of information for each term in every
#   document.  The following data is provided (based on Solr server
#   configuration):
#
#   - `:tf`, term frequency: the number of times this term appears in
#     the given document
#   - `:offsets`, term offsets: the start and end character offsets for
#     this word within the full text.  Note that these offsets can be
#     complicated by string encoding issues, be careful when using them!
#   - `:positions`, term positions: the position of this word (in
#     *number of words*) within the full text.  Note that these positions
#     rely on the precise way in which Solr splits words, which is specified
#     by [Unicode UAX #29.](http://unicode.org/reports/tr29/)
#   - `:df`, document frequency: the number of documents in the collection
#     that contain this word
#   - `:tfidf`, term frequency-inverse document frequency: equal to `(term
#     frequency / number of words in this document) * log(size of collection
#     / document frequency)`.  A measure of how "significant" or "important"
#     a given word is within a document, which gives high weight to words
#     that occur frequently in a given document but do _not_ occur in other
#     documents.
#
#   @note This attribute may be `nil`, if the query type requested from
#     the Solr server does not return term vectors.
#
#   @return [Hash] term vector information.  The hash contains the following
#     keys:
#       term_vectors['word']
#       term_vectors['word'][:tf] = Integer
#       term_vectors['word'][:offsets] = Array<Range>
#       term_vectors['word'][:offsets][0] = Range
#       # ...
#       term_vectors['word'][:positions] = Array<Integer>
#       term_vectors['word'][:positions][0] = Integer
#       # ...
#       term_vectors['word'][:df] = Float
#       term_vectors['word'][:tfidf] = Float
#       term_vectors['otherword']
#       # ...
class Document
  # Make this class act like an ActiveRecord model, though it's not backed by
  # the database (it's in Solr).
  include ActiveModel::Model
  include ActiveModel::Conversion

  # And give it GlobalID support
  include GlobalID::Identification

  attr_accessor :schema,
                :version,
                :id,
                :doi,
                :externalIds,
                :license,
                :licenseUrl,
                :dataSource,
                :dataSourceUrl,
                :dataSourceVersion,
                :type,
                :title,
                :authors,
                :journal,
                :date,
                :dateElectronic,
                :dateAccepted,
                :dateReceived,
                :volume,
                :number,
                :pages,
                :keywords,
                :tags,
                :term_vectors

  # The id attribute is the only required one
  validates :id, presence: true

  # Inform Rails that these models are all saved by Solr
  def persisted?
    true
  end

  # Return a document (just bibliographic data) by id
  #
  # @param [String] id id of the document to be retrieved
  # @param [Hash] options options which modify the behavior of the search
  # @option options [Boolean] :term_vectors if true, return term vectors
  # @return [Document] the document requested
  # @raise [Sciveyor::Solr::Connection::Error] thrown if there is an error
  #   querying Solr
  # @raise [ActiveRecord::RecordNotFound] thrown if no matching document can
  #   be found
  def self.find(id, options = {})
    find_by!(id: id, term_vectors: options[:term_vectors])
  end

  # Query a document and raise an exception if it's not found
  #
  # @option args [Boolean] :term_vectors if true, return term vectors
  # @option args [String] :field any document field may be queried here as a
  #   search query (see example)
  # @return [Document] the document requested, or nil if not found
  # @raise [Sciveyor::Solr::Connection::Error] thrown if there is an error
  #   querying Solr
  # @raise [ActiveRecord::RecordNotFound] thrown if no matching document can
  #   be found
  def self.find_by!(args)
    find_by(args) || raise(ActiveRecord::RecordNotFound)
  end

  # Query a document and return it (or nil)
  #
  # @option args [Boolean] :term_vectors if true, return term vectors
  # @option args [String] :field any document field may be queried here as a
  #   search query (see example)
  # @return [Document] the document requested, or nil if not found
  # @raise [Sciveyor::Solr::Connection::Error] thrown if there is an error
  #   querying Solr
  def self.find_by(args)
    # Delete the special arguments
    term_vectors = args.delete(:term_vectors)

    # Build the query
    query = {}
    query[:q] = query_for_args(args)
    query[:tv] = "true" if term_vectors == true

    # Run the search
    result = Sciveyor::Solr::Connection.search(query)
    return nil if result.num_hits < 1
    result.documents[0]
  end

  # @return [String] the starting page of this document, if it can be parsed
  def start_page
    return nil unless pages

    @start_page ||= pages.split("-")[0]
    @start_page
  end

  # @return [String] the ending page of this document, if it can be parsed
  def end_page
    return nil unless pages
    parts = pages.split("-")
    return nil if parts.length <= 1

    @end_page ||=
      begin
        spage = parts[0]
        epage = parts[-1]

        # Check for range strings like "1442-7"
        if spage.length > epage.length
          ret = spage
          ret[-epage.length..-1] = epage
        else
          ret = epage
        end
        ret
      end
    @end_page
  end

  # Set all attributes and create author lists
  #
  # @param [Hash] attributes attributes for this document
  def initialize(attributes = {})
    super

    # Don't let any blank strings hang around as values for the Solr
    # fields. This prevents a whole lot of #blank? and #present? calls
    # throughout.
    %i[
      id
      doi
      license
      licenseUrl
      dataSource
      dataSourceUrl
      type
      title
      journal
      volume
      number
      pages
    ].each do |a|
      value = send(a)
      send("#{a}=".to_sym, nil) if value&.strip&.empty?
    end

    # Ensure good data in array-format fields
    %i[externalIds keywords tags].each do |a|
      value = send(a)
      send("#{a}=".to_sym, nil) if value.is_a?(String) && value&.strip&.empty?
      send("#{a}=".to_sym, nil) if value&.empty?
    end

    # Convert dates into Date objects if present. This will throw a DateError
    # if the values are invalid.
    %i[date dateElectronic dateAccepted dateReceived].each do |a|
      value = send(a)
      next if value.nil?

      if value.is_a?(String)
        value = value.strip
        next if value.empty?

        dt = DateTime.iso8601(value)
        send("#{a}=".to_sym, dt)
      end
    end

    # Convert the authors into an authors object (and do this even if the
    # child docs are nil or empty)
    self.authors = Sciveyor::Documents::Authors.from_docs(authors)
  end

  # Keys in author child documents that we need to process
  AUTHOR_KEYS = {
    authorsName: "name",
    authorsFirst: "first",
    authorsMiddle: "middle",
    authorsLast: "last",
    authorsPrefix: "prefix",
    authorsSuffix: "suffix",
    authorsAffiliation: "affiliation"
  }

  # Convert the list of attributes to a Solr query.
  #
  # @param [Hash] args the arguments passed to find_by
  # @return [String] a single query string
  private_class_method def self.query_for_args(args)
    # Start by peeling off any author arguments and building them into a
    # child document query
    author_query = []

    AUTHOR_KEYS.each do |arg, field|
      value = args.delete(arg)
      author_query << "#{field}:\"#{value}\"" if value
    end

    # Start by building the normal arg queries
    arr = args.map { |k, v| "#{k}:\"#{v}\"" }

    # Add the child-document query
    unless author_query.empty?
      arr << "{!parent which=\"type:article\"}(#{author_query.join(" AND ")})"
    end

    arr.join(" AND ")
  end
end
