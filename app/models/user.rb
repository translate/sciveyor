# frozen_string_literal: true

# Representation of a user in the database
#
# Sciveyor keeps track of users so that it can send emails regarding
# background jobs and keep a set of customizable user options.
#
# @!attribute name
#   @raise [RecordInvalid] if the name is missing (`validates :presence`)
#   @return [String] Full name
# @!attribute email
#   @return [String] Email address (from Devise)
# @!attribute encrypted_password
#   @return [String] Password (encrypted, from Devise)
# @!attribute reset_password_token
#   @return [String] Token sent with reset password email (from Devise)
# @!attribute reset_password_sent_at
#   @return [DateTime] Time of the last reset password mail (from Devise)
# @!attribute remember_created_at
#   @return [DateTime] Time at which the user last selected "remember me"
#     (from Devise)
# @!attribute sign_in_count
#   @return [Integer] The number of logins for this user (from Devise)
# @!attribute current_sign_in_at
#   @return [DateTime] The time at which the user currently signed in (from
#     Devise)
# @!attribute last_sign_in_at
#   @return [DateTime] The last time this user signed in (from Devise)
# @!attribute current_sign_in_ip
#   @return [String] The IP from which the user currently is logged in (from
#     Devise)
# @!attribute last_sign_in_ip
#   @return [String] The IP from which the user last logged in (from Devise)
#
# @!attribute language
#   @raise [RecordInvalid] if the language is missing (`validates :presence`)
#   @raise [RecordInvalid] if the language is not a valid language code
#     (`validates :format`)
#   @return [String] Locale code of user's preferred language
# @!attribute timezone
#   @raise [RecordInvalid] if the timezone is missing (`validates :presence`)
#   @return [String] User's timezone, in Rails' format
#
# @!attribute tasks
#   @return [Array<Task>] All tasks created by the user (`has_many`)
# @!attribute libraries
#   @return [Array<Library>] All library links added by the user (`has_many`)
#
# @!attribute workflow_active
#   @return [Boolean] True if the user is currently building a query in the
#     workflow controller
# @!attribute workflow_class
#   @return [String] If set, the class the user has selected to perform in the
#     workflow controller
# @!attribute workflow_datasets
#   @return [Array<Dataset>] An array of the datasets the user has selected
#     to perform in the workflow controller
#
# @!attribute export_filename
#   @return [String] The user's data, collected as a ZIP file archive
# @!attribute export_content_type
#   @return [String] The content type of the user's data archive
# @!attribute export_requested_at
#   @return [Time] The time at which the user last requested a data export
class User < ApplicationRecord
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :validatable

  validates :name, presence: true
  validates :language,
            presence: true,
            format: {
              with: /\A[a-z]{2,3}(-[A-Z]{2})?\Z/
            }
  validates :timezone, presence: true

  has_many :tasks, dependent: :destroy
  has_many :libraries, dependent: :destroy

  before_destroy :delete_export

  # @return [String] string representation of this user
  def to_s
    "#{name} <#{email}>"
  end

  # @return [Hash<String, Dataset>] all datasets which the user has tagged as
  #   of interest, with the name that the user chose for each as key
  def datasets_hash
    sql =
      "SELECT name, dataset_uuid FROM users_datasets WHERE user_id = :user_id"
    query = User.sanitize_sql_array([sql, { user_id: self.id }])
    result = User.connection.exec_query(query)
    return {} if result.empty?

    Hash[result.rows.map { |r| [r[0], Dataset.find_by(uuid: r[1])] }]
  end

  # @return [Array<Dataset>] the raw list of datasets for this user, without
  #   names
  def datasets
    sql = "SELECT dataset_uuid FROM users_datasets WHERE user_id = :user_id"
    query = User.sanitize_sql_array([sql, { user_id: self.id }])
    result = User.connection.exec_query(query)
    return [] if result.empty?

    Dataset.where(uuid: result.rows.flatten)
  end

  # Save the fact that this user is interested in this dataset
  #
  # This creates a record in the join table connecting the user with the
  # dataset. (Because datasets are permanent and not owned by an individual
  # user, this is how users store a list of datasets of interest.)
  #
  # @param [Dataset] dataset the dataset of interest
  # @param [String] name the name to save for this dataset
  # @return [void]
  def add_dataset(dataset, name)
    return if dataset.nil?

    # There must be a name, so default it if we don't have one
    name = Dataset.model_name.human if name.blank?

    # Don't double-insert
    sql =
      "SELECT dataset_uuid FROM users_datasets WHERE user_id = :user_id AND dataset_uuid = :dataset_uuid"
    query =
      User.sanitize_sql_array(
        [sql, { user_id: self.id, dataset_uuid: dataset.uuid }]
      )
    result = User.connection.exec_query(query)
    return unless result.empty?

    # Okay, go ahead
    sql =
      "INSERT INTO users_datasets (user_id, dataset_uuid, name) VALUES (:user_id, :dataset_uuid, :name)"
    query =
      User.sanitize_sql_array(
        [sql, { user_id: self.id, dataset_uuid: dataset.uuid, name: name }]
      )
    User.connection.exec_insert(query)
  end

  # Remove the connection between this user and the given dataset
  #
  # If a user no longer is interested in a given dataset, this destroys the
  # record in the join table connecting the two together.
  #
  # @param [Dataset] dataset the dataset of interest
  # @return [void]
  def remove_dataset(dataset)
    return if dataset.nil?

    sql =
      "DELETE FROM users_datasets WHERE user_id = :user_id AND dataset_uuid = :dataset_uuid"
    query =
      User.sanitize_sql_array(
        [sql, { user_id: self.id, dataset_uuid: dataset.uuid }]
      )
    User.connection.exec_delete(query)
  end

  # Get a particular workflow dataset for this user
  #
  # The `workflow_datasets` attribute is an array of dataset uuid values, so
  # this will convert them into actual dataset objects.
  #
  # @param [Integer] n the number of the dataset to return
  # @raise [RecordNotFound] if the index is outside the range for the number
  #   of datasets in the user's workflow
  # @return [Dataset] the given dataset
  def workflow_dataset(num)
    raise ActiveRecord::RecordNotFound if workflow_datasets.size <= num
    Dataset.find_by!(uuid: workflow_datasets[num])
  end

  # Returns true if the user is allowed to start another export job
  #
  # @return [Boolean] true if the user can export their data now
  def can_export?
    return true unless export_requested_at
    export_requested_at < 1.day.ago
  end

  # Override the Devise email delivery logic to queue mail delivery
  #
  # We tap into this method to make sure that emails from Devise are
  # delivered in the background, on the maintenance queue.
  #
  # @param [Symbol] notification the notification to be sent
  # @param [Array] args the arguments for the message
  # @return [void]
  # :nocov:
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_now
  end

  private

  # Delete the export file associated with this user
  #
  # @return [void]
  def delete_export
    Sciveyor::Storage.delete(self.export_filename)
  end
end
