# frozen_string_literal: true

# A file full of results that belongs to a task
#
# As analyses can generate multiple kinds of files uploaded to storage, this
# class encapsulates the content of a file and its basic description.
#
# @!attribute [String] filename
#   @raise [RecordInvalid] if the filename is missing (validates :presence)
#   @return [String] The name of this file in Sciveyor storage
# @!attribute [String] content_type
#   @raise [RecordInvalid] if the content type is missing (validates
#     :presence)
#   @return [String] The content type of this file
# @!attribute description
#   @raise [RecordInvalid] if the description is missing (validates :presence)
#   @return [String] A description of this file's contents
# @!attribute short_description
#   @return [String] A short description (for a download button) of this file
# @!attribute task
#   @raise [RecordInvalid] if the task is missing (validates :presence)
#   @return [Task] The task to which this file belongs (`belongs_to`)
# @!attribute downloadable
#   @return [Boolean] If true, this file can be downloaded by the end user
#     (default is `false`)
class Upload < ApplicationRecord
  validates :filename, presence: true
  validates :content_type, presence: true
  validates :description, presence: true
  validates :task_id, presence: true

  before_destroy :delete_file

  belongs_to :task

  scope :downloadable, -> { where(downloadable: true) }

  # Set the contents of the file from the given string
  #
  # @param [String] content the content for the file
  def from_string(content)
    Sciveyor::Storage.upload(StringIO.new(content), self.filename)
  end

  # Get the contents of this file as a string
  #
  # @raise [Sciveyor::Storage::Error] if the file cannot be fetched
  # @return [String] the contents of this file
  def get_string
    Sciveyor::Storage.get(self.filename) do |io, type|
      io.set_encoding "UTF-8"
      return io.read
    end
  end

  private

  # Delete the associated file
  #
  # @return [void]
  def delete_file
    Sciveyor::Storage.delete(self.filename)
  end
end
