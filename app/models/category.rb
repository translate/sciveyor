# frozen_string_literal: true

# A category of journals
#
# Sciveyor supports categorization of journals, so that users can filter
# results by journal type.  This is the class for each category in the tree.
#
# @!attribute name
#   @raise [RecordInvalid] if the name is missing (`validates :presence`)
#   @return [String] name of the category
# @!attribute journals
#   @return [Array<String>] list of journals in this category
# @!attribute path
#   @return [Array<Integer>] array of integers representing tree path
class Category < ApplicationRecord
  validates :name, presence: true

  # @return [String] string representation of this category
  def to_s
    name
  end

  # @return [Array<Category>] all categories with no parent
  scope :roots, -> { where(path: []) }

  # @return [Category] the root category that contains this category
  def root
    if root_id = path.first
      find(root_id)
    else
      self
    end
  end

  # @return [Category] the parent category of this category, or nil
  def parent
    if parent_id = path.last
      find(parent_id)
    else
      nil
    end
  end

  # @return [Array<Category>] all categories ancestral to this one
  def ancestors
    path.present? ? Category.where(id: path) : Category.none
  end

  # @return [Array<Category>] all categories with the same parent as this one
  def siblings
    Category.where(path: path)
  end

  # @return [Array<Category>] all direct children of this node
  def children
    Category.where(path: path + [id])
  end

  # @return [Array<Category>] all categories that are descendants of this one
  def descendants
    Category.where(":id = ANY(path)", id: id)
  end

  # Set the parent of this category to the given category
  #
  # @param [Category] parent the new parent, or nil to make this a root
  # @return [void]
  def parent=(parent)
    if parent.nil?
      self.path = []
    else
      self.path = parent.path + [parent.id]
    end
  end

  # Returns the list of currently active categories
  #
  # @param [Sciveyor::Solr::Params] search_params the parameters to check
  # @return [Array<Category>] the list of categories active
  def self.active(search_params)
    (search_params.categories || []).map { |id| find(id) }
  end

  # Is this category enabled for these params, or no?
  #
  # @param [Sciveyor::Solr::Params] search_params the parameters to check
  # @return [Boolean] if true, category is enabled on these params
  def enabled?(search_params)
    (search_params.categories || []).include?(to_param.to_i)
  end

  # Generate parameters that would toggle this category on or off
  #
  # This function returns a copy of `params`, but with the parameters
  # changed such that this category and all of its descendant categories
  # are switched either on or off.
  #
  # @param [Sciveyor::Solr::Params] search_params the current parameters
  # @return [Hash] new parameters with this category and its descendants
  #   toggled
  def toggle_search_params(search_params)
    ret = search_params.dup

    if enabled?(ret)
      remove = [to_param]
      remove += descendants.collect(&:to_param)

      ret.categories -= remove.map { |id| id.to_i }
    else
      add = [to_param]
      add += descendants.collect(&:to_param)

      ret.categories ||= []
      ret.categories += add.map { |id| id.to_i }
    end
    ret.categories.uniq!

    ret.categories = nil if ret.categories.empty?
    ret
  end
end
