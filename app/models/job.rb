# frozen_string_literal: true

# A record of a job to be performed in the background
#
# We keep track of a very thin record corresponding to every job in the
# database, which will be used to launch workers.
#
# @!attribute task
#   @return [Task] the task that this job is running against
# @!attribute created_at
#   @return [DateTime] the time at which this job was created (i.e., when
#     the user requested it to be run)
class Job < ApplicationRecord
  validates :task_id, presence: true
  belongs_to :task
end
