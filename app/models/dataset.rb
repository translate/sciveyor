# frozen_string_literal: true

# The saved results of a given search, for analysis
#
# A dataset is a given set of searches, persisted in the database so that we
# can run digital humanities analyses on the returned collections of documents.
#
# @!attribute uuid
#   @raise [RecordInvalid] if absent (`validates_presence`)
#   @return [String] the unique identifier of this dataset
# @!attribute document_count
#   @return [Integer] the approximate number of documents in this dataset
#     (cached)
#
# @!attribute q
#   @return [Array<String>] the searches for this dataset
# @!attribute boolean
#   @return [String] the boolean operator to combine `q`, either `and` or `or`.
#     Defaults to `and`.
# @!attribute fq
#   @return [Array<String>] the filter queries for this dataset
#
# @!attribute tasks
#   @return [Array<Task>] The tasks run on this dataset
#     (`has_and_belongs_to_many`)
class Dataset < ApplicationRecord
  validates :uuid, presence: true
  before_validation :ensure_uuid, on: :create
  before_save :update_size_cache

  has_and_belongs_to_many :tasks

  # @return [String] uuid for URL formation
  def to_param
    uuid
  end

  # @return [String] string representation of this dataset
  def to_s
    uuid
  end

  # @return [String] the name that the given user has saved for this dataset
  def name_for(user)
    return uuid if user.nil?

    user.datasets_hash.each do |name, dataset|
      return name if uuid == dataset.uuid
    end

    # User hasn't actually saved this dataset after all
    return uuid
  end

  # Return the result of doing this query, with additional parameters added
  #
  # @param [Hash] extra_params Solr-format search parameters to add
  # @return [Sciveyor::Solr::SearchResult] the search results
  def search(extra_params = {})
    search_params = Sciveyor::Solr::Params.new(dataset: self)
    Sciveyor::Solr::Connection.search(search_params.to_solr.merge(extra_params))
  end

  # Return the number of documents this dataset contains
  #
  # Note that this is an expensive operation, as it requires executing the
  # search corresponding to the dataset. If you are happy with an approximate,
  # cached result, use instead `#document_count`.
  #
  # @return [Integer] the number of documents in the dataset
  def size
    search(rows: 0).num_hits
  end

  private

  # Make sure that every record has a UUID created.
  #
  # @return [void]
  def ensure_uuid
    self.uuid = Nanoid.generate if uuid.nil?
  end

  # Update the cached number of documents in the Dataset model
  #
  # @return [void]
  def update_size_cache
    self.document_count = self.size
  end
end
