# frozen_string_literal: true

# An analysis task
#
# While the processing is actually occurring in a background job, we
# need a way for those jobs to communicate with users via the web
# front-end.  This model is how they do so.
#
# Tasks have two special partials that can be implemented to engage novel
# behavior:
#
# - `_params.html.haml` (optional): If this view is present, then after the
#   task has collected the appropriate number of datasets, the user will be
#   presented with this form in order to set special parameters for the
#   task. This partial should consist of a form that submits to
#   `workflow_run_path`.
# - `results.html.haml` (optional): If this view is present, then after the
#   task is completed, the user will be offered a link to view this template
#   in addition to whatever downloadable file results the task produces.
#
# @!attribute name
#   @raise [RecordInvalid] if the name is missing (validates :presence)
#   @return [String] The name of this task
# @!attribute type
#   @raise [RecordInvalid] if the type is missing (validates :presence)
#   @return [String] The identifier for the kind of job to run
# @!attribute created_at
#   @return [DateTime] The time at which this task was created
# @!attribute updated_at
#   @return [DateTime] The time at which this task was last updated
# @!attribute finished
#   @return [Boolean] True if a job for this task has been run and finished
# @!attribute finished_at
#   @return [Time] The time at which this task was finished
# @!attribute failed
#   @return [Boolean] True if a job for this task has been run and failed
# @!attribute job_message
#   @return [String] A message left by the last job run, when it finished or
#     failed
# @!attribute job_params
#   @return [String] A JSON hash of parameters to pass to jobs
# @!attribute attempts
#   @return [Integer] The number of times this job has been attempted
# @!attribute user
#   @raise [RecordInvalid] if the user is missing (validates :presence)
#   @return [User] The user that created this dataset (`belongs_to`)
# @!attribute datasets
#   @return [Dataset] The datasets on which this task is running
#     (`has_and_belongs_to_many`)
# @!attribute uploads
#   @return [Array<Upload>] Any stored files created by this task
# @!attribute jobs
#   @return [Array<Job>] Any jobs that are currently performing this task
class Task < ApplicationRecord
  # Let me use a column name called 'type', please
  self.inheritance_column = :_type_disabled

  validates :name, presence: true
  validates :user_id, presence: true
  validates :type, presence: true

  belongs_to :user
  has_and_belongs_to_many :datasets
  has_many :uploads, dependent: :destroy
  has_many :jobs, dependent: :destroy

  scope :active, -> { where(finished: false) }
  scope :finished, -> { where(finished: true) }
  scope :failed, -> { where(finished: true, failed: true) }

  # @return [String] string representation of this task
  def to_s
    status = finished ? (failed ? "failed" : "finished") : "active"

    "#{name} (#{type}): #{status}"
  end

  # Get the path for a view template for this job
  #
  # @param [String] template the view path to generate
  # @return [String] the template path to pass to `render`
  def template_path(template)
    class_folder = job_class.name.demodulize.underscore.sub(%r{\A.*/}, "")
    "jobs/#{class_folder}/#{template}"
  end

  # Get the upload object with the given content type
  #
  # @param [String] content_type the content type to find
  # @return [Upload] the uploaded file object
  def upload_for(content_type)
    uploads.detect { |f| f.content_type == content_type }
  end

  # Get the JSON content from an upload if available
  #
  # If there is no JSON upload attached to this task, this method will return
  # `nil`.
  #
  # @return [String] JSON data as string (or `nil`)
  def json
    begin
      upload_for("application/json")&.get_string
    rescue Sciveyor::Storage::Error
      nil
    end
  end

  # Classes that should be hidden from the list of jobs available
  HIDDEN_CLASSES = [Sciveyor::Jobs::Base, Sciveyor::Jobs::UserExport].freeze

  # Get a list of all classes that are jobs
  #
  # This method looks up all the defined job classes in
  # `app/lib/sciveyor/jobs` and returns them in a list so that we may loop
  # over them (e.g., when including all job-start markup).
  #
  # @return [Array<Class>] array of class objects
  def self.job_list
    # Get all the job files
    analysis_files =
      Dir[Rails.root.join("app", "lib", "sciveyor", "jobs", "*.rb")]
    classes =
      analysis_files.map do |f|
        # This will raise a NameError if the class doesn't exist, but we want
        # that, because that means there's a file in app/lib/sciveyor/jobs
        # that doesn't respect Rails' naming conventions.
        class_name = "Sciveyor::Jobs::" + File.basename(f, ".rb").camelize
        class_name.constantize
      end
    classes.compact!
    classes.reject! { |klass| HIDDEN_CLASSES.include?(klass) }

    # Make sure that worked
    classes.each { |c| return [] unless c.is_a?(Class) }

    classes
  end

  # Convert class_name to a class object
  #
  # @param [String] class_name the class name to convert
  # @return [Class] the job class
  def self.job_class(class_name)
    ("Sciveyor::Jobs::" + class_name).safe_constantize.tap do |klass|
      # We are never allowed to start the Base class as a job
      if klass.nil? || klass == Sciveyor::Jobs::Base
        raise ArgumentError, "#{class_name} is not a valid class"
      end
    end
  end

  # Convert #type into a class object
  #
  # @return [Class] the job class
  def job_class
    self.class.job_class(type)
  end

  # Start a job for this task
  #
  # @param [Hash] options the options to pass to the job
  # @return [Job] the job that was just created
  def job_start(options = {})
    # Get the name of the job from the translation table
    self.name = I18n.t("jobs.#{type.underscore}.short_desc")

    # Encode the options to JSON
    self.job_params = JSON.generate(options) unless options.empty?

    # Clear out all our other job parameters
    self.attempts = 0
    self.finished_at = nil
    self.finished = false
    self.failed = false

    self.job_message = I18n.t("common.progress.initializing")
    save

    # See if we should run all jobs immediately
    if (ENV["BLOCKING_JOBS"] || "false").to_boolean
      self.job_class.new.perform(self, options)
    else
      Job.create(task: self)
    end
  end

  # Mark this task as completed
  #
  # @param [String] message if set, will use a customized success message
  # @return [void]
  def mark_completed(message = nil)
    self.finished = true
    self.failed = false
    self.finished_at = Time.current

    self.job_message = message || I18n.t("common.progress.success")

    finish!
  end

  # Mark the given task as failed
  #
  # @param [String] message if set, will use a customized failure message
  # @return [void]
  def mark_failed(message = nil)
    self.finished = true
    self.failed = true
    self.finished_at = Time.current

    self.job_message = message || I18n.t("common.progress.failure")

    finish!
  end

  # Perform a query in a separate database connection
  #
  # In general, task execution is performed in a single massive SQL transaction
  # that will be rolled back if the job fails. We do still want to be able to
  # save certain kinds of logging and debugging information. We can do that if
  # we pull off another connection from the pool and use it to make the DB
  # write.
  #
  # @note Do not use this method for saving any data that has to do with the
  #   completion of the actual task.
  # @yield the connection object will be passed to the block
  # @yieldparam [ActiveRecord::ConnectionAdapters::AbstractAdapter] the new
  #   connection
  # @return [void]
  def self.in_separate_connection
    return unless block_given?

    conn = ActiveRecord::Base.connection_pool.checkout
    yield conn
    ActiveRecord::Base.connection_pool.checkin(conn)
  end

  private

  # Hook to be called whenever a job finishes
  #
  # This hook will set the finished attribute on the job and send a notification
  # email to the user.
  #
  # @return [void]
  def finish!
    # Make sure the task is saved
    save

    # Send the user an email
    UserMailer.job_finished_email(user.email, self).deliver_now
  end
end
