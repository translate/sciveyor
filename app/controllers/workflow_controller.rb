# frozen_string_literal: true

# Display the user's workflow through Sciveyor
#
# We walk the user through the process of logging in, selecting an analysis
# type to run, gathering datasets, and collecting results.  This controller
# is responsible for all of that.
class WorkflowController < ApplicationController
  layout "full_page"
  before_action :authenticate_user!, except: :index

  # Show the introduction page or the user dashboard
  #
  # @return [void]
  def index
    @database_size = Sciveyor::Solr::CorpusStats.new.size || 0

    if user_signed_in?
      render "dashboard"
    else
      render "index"
    end
  end

  # Start running a new analysis
  #
  # @return [void]
  def start
  end

  # Destroy the currently building analysis, leaving workflow mode
  #
  # @return [void]
  def stop
    current_user.workflow_active = false
    current_user.workflow_class = nil
    current_user.workflow_datasets.clear
    current_user.save

    redirect_to workflow_path, alert: I18n.t("workflow.stop.success")
  end

  # Show information about a job
  #
  # @return [void]
  def info
    set_workflow_parameters
  end

  # Get the user to collect datasets for a job
  #
  # @return [void]
  def activate
    set_workflow_parameters

    # Write out the class that the user has chosen
    current_user.workflow_active = true
    current_user.workflow_class = params[:class]

    # See if we've been asked to link or unlink a dataset to this job
    if params[:link_dataset_id]
      # Make sure the dataset exists
      dataset = Dataset.find_by!(uuid: params[:link_dataset_id])
      current_user.workflow_datasets << dataset.uuid
    elsif params[:unlink_dataset_id]
      current_user.workflow_datasets.delete(params[:unlink_dataset_id])
    end

    # Save our changes, if any, and update the workflow parameters
    current_user.save!
    set_workflow_parameters
  end

  # Get the configuration of the job from the user
  #
  # This renders the `_params.html.haml` view from inside the selected job.
  #
  # @return [void]
  def options
    set_workflow_parameters

    # Make sure we've linked enough datasets
    return if current_user.workflow_datasets.size == @klass.num_datasets

    raise ArgumentError,
          "incorrect number of datasets specified for #{params[:class]}"
  end

  # Perform the task that the user has selected
  #
  # @return [void]
  def run
    set_workflow_parameters

    # Create a task
    task =
      current_user.tasks.create(
        name: @klass.t(".short_desc"),
        type: params[:class],
        datasets:
          current_user.workflow_datasets.map { |id| Dataset.find_by!(uuid: id) }
      )

    # Start it up
    task.job_start(@job_params || {})

    current_user.workflow_active = false
    current_user.workflow_class = nil
    current_user.workflow_datasets.clear
    current_user.save

    redirect_to root_path, flash: { success: I18n.t("workflow.run.finished") }
  end

  # Allow the user to pick up data from all of their tasks
  #
  # @return [void]
  def fetch
    set_fetch_parameters

    if params[:terminate]
      # Delete all unfinished tasks in the DB.
      current_user.tasks.active.readonly(false).destroy_all
      redirect_to root_path, alert: I18n.t("workflow.fetch.terminate")
      return
    end

    render
  end

  # Show the results display only for quick updates
  #
  # @return [void]
  def fetch_load
    disable_browser_cache
    set_fetch_parameters

    render layout: false
  end

  # Delete a task
  #
  # This action deletes a given task and its associated files.
  #
  # @return [void]
  def destroy
    current_user.tasks.find(params[:id]).destroy
    redirect_to workflow_fetch_path,
                flash: {
                  success: I18n.t("workflow.destroy.success")
                }
  end

  # Show a view from a task
  #
  # This action will render a view that comes packaged with a background job.
  #
  # @return [void]
  def view
    task = current_user.tasks.find(params[:id])
    presenter = Sciveyor::Presenters::TaskPresenter.new(task: task)

    render(
      task.template_path(params[:template]),
      formats: [(params[:format] || :html).to_sym],
      locals: {
        task: task,
        presenter: presenter,
        klass: task.job_class
      }
    )
  end

  private

  # Get the current workflow details from the params
  #
  # @return [void]
  def set_workflow_parameters
    raise ActiveRecord::RecordNotFound unless params[:class]
    @klass = Task.job_class(params[:class])

    @num_datasets = @klass.num_datasets
    @num_workflow_datasets = current_user.workflow_datasets.size

    @job_params = params[:job_params]&.to_unsafe_h&.with_indifferent_access
  end

  # Get the parameters for fetching workflow tasks
  #
  # @return [void]
  def set_fetch_parameters
    tasks = current_user.tasks

    @pending_tasks = tasks.active.order(:created_at)
    @finished_tasks = tasks.finished.order(finished_at: :desc)
  end
end
