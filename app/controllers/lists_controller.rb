# frozen_string_literal: true

# Access lists of authors and journals
#
# This controller returns JSON-formatted lists of authors and journals, for
# use in automated queries (such as autocomplete on the advanced search page).
class ListsController < ApplicationController
  # Build a list of authors
  #
  # @return [void]
  def authors
    common_params

    @solr_json_facet = {
      "authors" => {
        "type" => "terms",
        "field" => "name",
        "limit" => @limit,
        "offset" => @offset,
        "domain" => {
          "blockChildren" => "type:article"
        }
      }
    }.to_json

    @solr_filter_query = "{!parent which=\"type:article\"}name_clean"
    @returned_facet = :author

    Rails.logger.info(
      short_message: "API query for list of authors",
      _method: "lists#authors",
      _filter: params[:filter] || "<all>",
      _limit: @limit,
      _offset: @offset
    )

    render json: get_list
  end

  # Build a list of journals
  #
  # @return [void]
  def journals
    common_params

    @solr_json_facet = {
      "journals" => {
        "type" => "terms",
        "field" => "journal",
        "limit" => @limit,
        "offset" => @offset
      }
    }.to_json

    @solr_filter_query = "journal_clean"
    @returned_facet = :journal

    Rails.logger.info(
      short_message: "API query for list of journals",
      _method: "lists#journals",
      _filter: params[:filter] || "<all>",
      _limit: @limit,
      _offset: @offset
    )

    render json: get_list
  end

  private

  # Grab some common parameters for all lists
  #
  # @return [void]
  def common_params
    if params[:limit]
      begin
        @limit = Integer(params[:limit], 10)
        raise ActionController::BadRequest if @limit <= 0
      rescue ArgumentError
        raise ActionController::BadRequest
      end
    end

    if params[:offset]
      begin
        @offset = Integer(params[:offset], 10)
        raise ActionController::BadRequest if @offset < 0
      rescue ArgumentError
        raise ActionController::BadRequest
      end
    end

    @limit ||= 10
    @offset ||= 0
  end

  # Return the list of values for this field
  #
  # @return [Array<Hash>] the list of results
  def get_list
    result = solr_query
    raise Sciveyor::Solr::Connection::Error unless result.facets

    facets = result.facets.for_field(@returned_facet)
    raise ActiveRecord::RecordNotFound unless facets

    facets
      .map do |f|
        f.hits.positive? ? { "value" => f.value, "hits" => f.hits } : nil
      end
      .compact
  end

  # Get the Solr query for a partial search for the given filter
  #
  # @return [Sciveyor::Solr::SearchResult] the search result
  def solr_query
    if params[:filter].blank?
      query = "*:*"
    else
      query = "#{@solr_filter_query}:(*#{params[:filter]}*)"
    end

    Sciveyor::Solr::Connection.search(
      :q => query,
      :rows => 1,
      "json.facet" => @solr_json_facet
    )
  end
end
