# frozen_string_literal: true

# Display static pages and images
#
# There are a number of pages in Sciveyor that do not change with time, as well
# as images that need to be shown throughout the site. This controller is the
# thin support for those tasks.
class StaticController < ApplicationController
  layout "full_page"

  # Show a variety of static information pages
  #
  # @return [void]
  %i[cookies user_data].each do |page|
    define_method page do
    end
  end

  # Return the form posted to this URL as a download
  #
  # This is used to facilitate downloads of SVG files from D3.js, among other
  # uses.
  #
  # @return [void]
  def echo
    params.require(%i[data content_type filename])

    send_data(
      params[:data],
      filename: params[:filename],
      type: params[:content_type]
    )
  end

  # Download a file from storage and serve it to the user
  #
  # This is used exclusively to return the results of jobs to users, so there
  # is no reason to cache the results that we receive from these calls.
  #
  # @return [void]
  def file
    type = "application/octet-stream"
    disposition = params[:download] ? "attachment" : "inline"
    data = nil

    begin
      Sciveyor::Storage.get(params[:name]) do |io, content_type|
        type = content_type
        data = io.read
      end
    rescue Sciveyor::Storage::Error
      raise ActiveRecord::RecordNotFound
    end

    send_data(
      data,
      filename: params[:name],
      type: type,
      disposition: disposition
    )
  end

  # Render the web app manifest
  #
  # This needs to be dynamic, as the icon paths change.
  #
  # @return [void]
  def manifest
  end

  # Render the IE browser configuration
  #
  # This needs to be dynamic, as the icon paths change.
  #
  # @return [void]
  def browserconfig
  end
end
