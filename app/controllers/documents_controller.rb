# frozen_string_literal: true

# Interact with individual documents
#
# This controller allows users to interact directly with single documents,
# including adding them to datasets, exporting them in different formats, and
# visiting them on external services.
class DocumentsController < ApplicationController
  # Export an individual document
  #
  # This action is content-negotiated: you must request the page for a document
  # with one of the MIME types we can export, and you will get a citation
  # export back, as a download.
  #
  # @return [void]
  def export
    @document = Document.find(params[:id])

    respond_to do |format|
      format.any(*Sciveyor::Documents::Serializers::Base.available) do
        klass = Sciveyor::Documents::Serializers::Base.for(request.format)

        headers[
          "Cache-Control"
        ] = "no-cache, must-revalidate, post-check=0, pre-check=0"
        headers["Expires"] = "0"

        send_data(
          klass.new(@document).serialize,
          filename: "export.#{request.format.to_sym}",
          type: request.format.to_s,
          disposition: "attachment"
        )

        Rails.logger.info(
          short_message: "Exported individual document",
          _method: "documents#export",
          _document_id: @document.id,
          _format: format.to_s
        )

        return
      end
      format.any do
        render file: error_page_path, formats: [:html], status: :not_acceptable
        return
      end
    end
  end
end
