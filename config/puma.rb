# frozen_string_literal: true
workers Integer(ENV["RAILS_WEB_WORKERS"] || 2)
threads_count = Integer(ENV["RAILS_MAX_THREADS"] || 5)
threads threads_count, threads_count

preload_app!

port 3000
environment ENV["RAILS_ENV"] || "development"

pidfile "tmp/pids/puma.pid"
state_path "tmp/pids/puma.state"
