# frozen_string_literal: true

require_relative "boot"

require "rails"

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "rails/test_unit/railtie"
require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Load our core extensions here, before we even configure the application
core_ext_files = File.expand_path("../lib/core_ext/**/*.rb", __dir__)
Dir[core_ext_files].each { |l| require l }

module Sciveyor
  # Central application class, started by Rails
  class Application < Rails::Application
    # Initialize configuration defaults for current config standard here
    config.load_defaults 7.0

    # Log at info, without colors, and use a formatter that accepts hashes
    config.log_level = :info
    config.log_formatter = Sciveyor::LogFormatter.new
    config.colorize_logging = false

    # Don't log all SQL transactions, even in development
    config.after_initialize do
      ActiveRecord::Base.logger = Rails.logger.clone
      ActiveRecord::Base.logger.level = Logger::INFO
    end

    # Always use Lograge to clean up request logs
    config.lograge.enabled = true
    config.lograge.formatter = Lograge::Formatters::KeyValue.new

    # Show error pages in all environments
    config.consider_all_requests_local = false
    config.action_dispatch.show_exceptions = true

    # We always want to enable the public file server, our standard deployments
    # pass everything through Puma
    config.public_file_server.enabled = true

    # Set mailer defaults
    config.action_mailer.default_url_options = { host: "sciveyor.com" }

    # Configure cookies
    config.session_store :cookie_store,
                         key: "_sciveyor_session",
                         httponly: true,
                         secure: Rails.env.production?
  end
end
