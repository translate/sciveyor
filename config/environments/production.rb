# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Enable caching.
  config.action_controller.perform_caching = true

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to
  # raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false
  config.action_mailer.perform_caching = false

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false
  config.assets.compress = true

  # Don't log any deprecations.
  config.active_support.report_deprecations = false

  # Either log to stdout, or send all logging info to Graylog, if available
  if ENV["GRAYLOG_HOSTNAME"].present?
    config.lograge.formatter = Lograge::Formatters::Graylog2.new
    config.logger =
      GELF::Logger.new(
        ENV["GRAYLOG_HOSTNAME"],
        ENV["GRAYLOG_PORT"].to_i,
        "WAN",
        { host: ENV["LOG_HOST"] || "sciveyor", facility: "rails" }
      )
  else
    config.logger = Logger.new(STDOUT, formatter: config.log_formatter)
  end

  # Get mail configuration parameters from ENV
  config.action_mailer.delivery_method = :mailgun
  config.action_mailer.mailgun_settings = {
    api_key: ENV["MAILGUN_API_KEY"],
    domain: ENV["MAILGUN_DOMAIN"],
    api_host: ENV["MAILGUN_API_HOST"] || "api.mailgun.net"
  }
end
