# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.headers = {
    "Cache-Control" => "public, max-age=#{1.hour.to_i}"
  }

  # Disable caching.
  config.action_controller.perform_caching = false
  config.cache_store = :null_store
  config.action_mailer.perform_caching = false

  # Send logging information to file
  config.log_level = :debug
  config.logger =
    Logger.new(
      Rails.root.join("tmp", "sciveyor_test.log"),
      formatter: config.log_formatter
    )

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raise exceptions for disallowed deprecations.
  config.active_support.disallowed_deprecation = :raise

  # Tell Active Support which deprecation messages to disallow.
  config.active_support.disallowed_deprecation_warnings = []

  # Disable all caching of assets and active compilation. Normally you might not
  # want compilation on, but we need it for our system tests.
  config.assets.compile = true
  config.assets.quiet = false
  config.assets.debug = true
  config.assets.compress = false
  config.assets.cache_store = :null_store

  # Raises error for missing translations
  config.i18n.raise_on_missing_translations = true

  # Disable request forgery protection in test environment
  config.action_controller.allow_forgery_protection = false

  # Tell Action Mailer not to deliver emails to the real world
  config.action_mailer.delivery_method = :test
end
