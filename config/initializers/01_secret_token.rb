# frozen_string_literal: true

if Rails.env.development? || Rails.env.test? || ENV["DOCKER_BUILD"].present?
  # Load some (obviously insecure) development/test tokens
  Rails.application.config.secret_key_base = "x" * 30
else
  # Load from .env, raising an exception if not found
  raise <<-ERROR.strip_heredoc unless ENV["SECRET_KEY_BASE"]
      No secret keys available in ENV. Please copy .env.example to .env, and
      run rake sciveyor:secrets:regen.
    ERROR

  Rails.application.config.secret_key_base = ENV.fetch("SECRET_KEY_BASE")
end
