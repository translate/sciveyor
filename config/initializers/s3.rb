# frozen_string_literal: true

if (Rails.env.production? || ENV["TEST_S3"]) && ENV["DOCKER_BUILD"].blank?
  unless ENV["S3_ACCESS_KEY_ID"].present? &&
           ENV["S3_SECRET_ACCESS_KEY"].present? && ENV["S3_BUCKET"].present?
    raise <<-ERROR.strip_heredoc
      The S3 storage credentials and bucket have not been configured. Please
      set the variables S3_ACCESS_KEY_ID, S3_SECRET_ACCESS_KEY, and S3_BUCKET
      (and, optionally, S3_ENDPOINT and S3_REGION) in .env.
    ERROR
  end

  Aws.config.update(
    credentials:
      Aws::Credentials.new(
        ENV["S3_ACCESS_KEY_ID"],
        ENV["S3_SECRET_ACCESS_KEY"]
      ),
    region: ENV["S3_REGION"] || "us-east-1",
    endpoint: ENV["S3_ENDPOINT"] || "https://s3.amazonaws.com"
  )
end
