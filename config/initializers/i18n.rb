# frozen_string_literal: true

# Enable pluralization and language fallbacks (from 'de-DE' to 'de')
require "i18n/backend/fallbacks"
I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
require "i18n/backend/pluralization"
I18n::Backend::Simple.send(:include, I18n::Backend::Pluralization)

# Look in subdirectories as well
Rails.application.config.i18n.load_path +=
  Dir[Rails.root.join("config", "locales", "**", "*.{rb,yml}")]

# Raise errors if we try to use a locale that isn't available
I18n.config.enforce_available_locales = true

# Default to English
Rails.application.config.i18n.default_locale = :en
Rails.application.config.i18n.available_locales = []

# Always use the fallbacks
Rails.application.config.i18n.fallbacks = [I18n.default_locale]

# This exact line is taken from the README file of the rails-i18n gem, which
# supplies localizations for all our Rails defaults. Just paste it in when you
# update the version of rails-i18n and the rest will be automatic here.
"af, ar, az, be, bg, bn, bs, ca, cs, csb, da, de, de-AT, de-CH, de-DE, dsb, dz, el, el-CY, en, en-AU, en-CA, en-CY, en-GB, en-IE, en-IN, en-NZ, en-TT, en-US, en-ZA, eo, es, es-419, es-AR, es-CL, es-CO, es-CR, es-EC, es-ES, es-MX, es-NI, es-PA, es-PE, es-US, es-VE, et, eu, fa, fi, fr, fr-CA, fr-CH, fr-FR, fur, fy, gl, gsw-CH, he, hi, hi-IN, hr, hsb, hu, id, is, it, it-CH, ja, ka, kk, km, kn, ko, lb, lo, lt, lv, mg, mk, ml, mn, mr-IN, ms, nb, ne, nl, nn, oc, or, pa, pap-AW, pap-CW, pl, pt, pt-BR, rm, ro, ru, sc, scr, sk, sl, sq, sr, st, sv, sv-FI, sv-SE, sw, ta, te, th, tl, tr, tt, ug, uk, ur, uz, vi, wo, zh-CN, zh-HK, zh-TW, zh-YUE"
  .split(",")
  .each do |loc|
    loc.strip!
    next if loc == "en-US" # No special translation for US English
    next unless TwitterCldr.supported_locale?(loc.downcase)

    Rails.application.config.i18n.available_locales << loc
  end
