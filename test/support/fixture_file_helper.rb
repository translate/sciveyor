# frozen_string_literal: true

# Helper methods for loading fixtures into storage
module FixtureFileHelper
  def fixture_to_storage(storage_file, fixture_file)
    File.open(fixture_file, "r") do |f|
      Sciveyor::Storage.upload(f, storage_file)
    end
  end
end
