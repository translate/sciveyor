# frozen_string_literal: true

module JobHelper
  def run_job_now(task, options = {})
    job = task.job_start(options)

    klass = task.job_class
    klass.new.perform(task, options)
  end

  def test_should_call_finish_and_send_email
    mailer_ret = mock
    mailer_ret.expects(:deliver_now)
    UserMailer.expects(:job_finished_email).returns(mailer_ret)

    perform

    assert @task.reload.finished
    refute_nil @task.finished_at
  end

  def test_available_should_respect_feature_flag
    env_variable =
      self.class.name.underscore.sub("_test", "").gsub(%r{\A.*/}, "").upcase
    env_variable << "_JOB_DISABLED"

    class_name = self.class.name.sub("Test", "")
    klass = class_name.constantize

    ENV[env_variable] = "1"
    refute klass.available?
    ENV.delete(env_variable)
  end
end
