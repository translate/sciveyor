# frozen_string_literal: true

require "test_helper"

class SearchHelperTest < ActionView::TestCase
  # There's no reason that we should have to do this, but for some reason Rails
  # isn't including the other helpers into the context when we test this helper.
  # Try removing this in future Rails versions to see if it gets fixed.
  include ApplicationHelper

  test "advanced_search_fields works" do
    ret = advanced_search_fields

    assert_kind_of Hash, ret
    assert_kind_of String, ret.keys[0]
    assert_kind_of Symbol, ret.values[0]
    assert_includes ret.values, :pages
  end

  test "facet_addition_links should have link for adding author facet" do
    h = ActionController::Parameters.new(controller: "search", action: "index")
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(q: "*:*")
    ret = facet_addition_links(search_params, res.facets, :author)

    url =
      "/search?" +
        CGI.escape("facets[]=author:Peter J. Hotez").gsub(
          "facets%5B%5D%3D",
          "facets%5B%5D="
        )
    assert_includes ret, url
  end

  test "facet_addition_links should have link for adding journal facet" do
    h = ActionController::Parameters.new(controller: "search", action: "index")
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(q: "*:*")
    ret = facet_addition_links(search_params, res.facets, :journal)

    url =
      "/search?" +
        CGI.escape("facets[]=journal:PLoS Neglected Tropical Diseases").gsub(
          "facets%5B%5D%3D",
          "facets%5B%5D="
        )
    assert_includes ret, url
  end

  test "facet_addition_links should have link for adding year facet" do
    h = ActionController::Parameters.new(controller: "search", action: "index")
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(q: "*:*")
    ret = facet_addition_links(search_params, res.facets, :date)

    url =
      "/search?" +
        CGI.escape("facets[]=date:2000").gsub(
          "facets%5B%5D%3D",
          "facets%5B%5D="
        )
    assert_includes ret, url
  end

  # Regression test for a crashing bug
  test "facet_removal_links works with nil facets" do
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index"
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    assert_empty facet_removal_links(search_params, nil)
  end

  test "facet_removal_links works with nothing active" do
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index"
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(q: "*:*")

    assert_empty facet_removal_links(search_params, res.facets)
  end

  test "facet_removal_links works with an active facet" do
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index",
        facets: ["journal:PLoS Neglected Tropical Diseases"]
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(search_params.to_solr)
    ret = facet_removal_links(search_params, res.facets)

    assert_includes ret, "href=\"/search\""
    assert_includes ret, "Journal: PLoS Neglected Tropical Diseases"
  end

  test "facet_removal_links works with overlapping facet and category" do
    cat = create(:category)
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index",
        categories: [cat.to_param],
        facets: ["journal:PLoS Neglected Tropical Diseases"]
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    res = Sciveyor::Solr::Connection.search(search_params.to_solr)
    ret = facet_removal_links(search_params, res.facets)

    assert_includes ret, "href=\"/search?categories%5B%5D=#{cat.to_param}\""
    assert_includes ret, "Journal: PLoS Neglected Tropical Diseases"
  end

  test "category_addition_tree works" do
    parent = create(:category, name: "Parent")
    create(:category, name: "Child", parent: parent)
    Category.stubs(:roots).returns([parent])

    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index"
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    tree = category_addition_tree(search_params)

    assert_includes tree, "Parent</a><ul>"
    assert_includes tree, "Child</a></li></ul>"
  end

  test "category_removal_links works with nothing active" do
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index"
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    assert_empty category_removal_links(search_params)
  end

  test "category_removal_links works with a category active" do
    cat = create(:category)
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index",
        categories: [cat.to_param]
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    ret = category_removal_links(search_params)

    assert_includes ret, "href=\"/search\""
    assert_includes ret, "Category: Test Category"
  end

  test "category_removal_links works with overlapping facet and category" do
    cat = create(:category)
    h =
      ActionController::Parameters.new(
        controller: "search",
        action: "index",
        categories: [cat.to_param],
        facets: ["journal:PLoS Neglected Tropical Diseases"]
      ).permit!
    controller.stubs(:params).returns(h)
    search_params = Sciveyor::Solr::Params.new(params: h)

    ret = category_removal_links(search_params)

    url =
      "/search?" +
        CGI.escape("facets[]=journal:PLoS Neglected Tropical Diseases").gsub(
          "facets%5B%5D%3D",
          "facets%5B%5D="
        )
    assert_includes ret, url
    assert_includes ret, "Category: Test Category"
  end
end
