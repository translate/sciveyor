# frozen_string_literal: true

require "test_helper"

module Datasets
  class TasksHelperTest < ActionView::TestCase
    test "task_download_path works" do
      task = create(:task, type: "ExportCitations")
      create(
        :upload,
        task: task,
        filename: "task-helper-test.json",
        content_type: "application/json",
        description: "test",
        short_description: "test"
      ) { |f| f.from_string("{\"abc\":123}") }

      assert_includes task_download_url(
                        task: task,
                        content_type: "application/json"
                      ),
                      "/file/task-helper-test.json"
      assert_nil task_download_url(task: task, content_type: "text/plain")
    end
  end
end
