# frozen_string_literal: true

require "application_system_test_case"

class InformationTest < ApplicationSystemTestCase
  test "view basic information" do
    sign_in_with
    create_dataset
    visit datasets_path
    assert_selector "td", text: "Integration Dataset"

    click_link "Manage"

    assert_text "338 documents"
    assert_text "Normal search: test\n"
  end
end
