# frozen_string_literal: true

require "application_system_test_case"

class SortTest < ApplicationSystemTestCase
  test "change sort order" do
    visit search_path

    click_link("Sort", match: :first)
    click_link("Sort: Title (descending)")

    within "table.document-list" do
      assert_text "Zoonotic Larval Cestode Infections: Neglected, Neglected Tropical Diseases?"
    end
  end
end
