# frozen_string_literal: true

require "application_system_test_case"

class AdvancedTest < ApplicationSystemTestCase
  test "search for an author" do
    visit "/search/advanced"
    select "Authors", from: "fields[]"
    fill_in "values[]", with: "Hotez", visible: false
    click_button "Perform advanced search"

    assert_selector "table.document-list tr td"
    element = find("table.document-list:first-of-type", match: :first)
    element.assert_text "The Giant Anteater in the Room:"
  end
end
