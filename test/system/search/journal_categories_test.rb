# frozen_string_literal: true

require "application_system_test_case"

class JournalCategoriesTest < ApplicationSystemTestCase
  setup do
    root =
      Category.create(
        name: "Root",
        journals: ["PLoS Neglected Tropical Diseases", "PNTDS"]
      )
    root.children.create(
      name: "Full",
      journals: ["PLoS Neglected Tropical Diseases"]
    )
    root.children.create(name: "Abbrev", journals: ["PNTDS"])
  end

  test "add a category" do
    visit search_path

    within(".filter-list") { click_link("Full") }

    assert_text(/1,499 articles /i)
    assert_selector "a.nav-link", text: /Category: Full/
  end

  test "clear a category" do
    visit search_path

    within(".filter-list") do
      click_link("Full")
      click_link("Abbrev")
    end

    within(".filter-list") { click_link("Abbrev") }

    assert_text(/1,499 articles /i)
    assert_selector "a.nav-link", text: /Category: Full/
  end

  test "clear all categories" do
    visit search_path

    within(".filter-list") { click_link("Abbrev") }

    within("#filters") { click_link("Remove All") }

    assert_text(/1,500 articles /i)
    assert_no_selector ".filter-header", text: "Active filters"
  end
end
