# frozen_string_literal: true

require "application_system_test_case"

class PendingTaskTest < ApplicationSystemTestCase
  test "workflow with pending task" do
    sign_in_with
    create_dataset
    visit datasets_path

    create(
      :task,
      user: User.last,
      datasets: [Dataset.first],
      finished: false,
      failed: false,
      finished_at: nil
    )

    within(".navbar") { click_link "Fetch" }
    within("#pending-table") do
      assert_selector "td", text: "Integration Dataset"
    end
  end

  test "workflow with failed task" do
    sign_in_with
    create_dataset
    visit datasets_path

    create(
      :task,
      user: User.last,
      datasets: [Dataset.first],
      finished: true,
      finished_at: 1.day.ago,
      failed: true,
      name: "Blahdeblah"
    )

    within(".navbar") { click_link "Fetch" }
    assert_selector "td", text: "Blahdeblah"
    assert_selector "td", text: "Task failed"
  end
end
