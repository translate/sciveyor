# frozen_string_literal: true

# Preview the output from the DeviseMailer
class DeviseMailerPreview < ActionMailer::Preview
  # Preview the reset password instructions email
  #
  # @return [void]
  def reset_password_instructions
    DeviseMailer.reset_password_instructions(User.first, "asdftoken123")
  end

  # Preview the password changed email
  #
  # @return [void]
  def password_change
    DeviseMailer.password_change(User.first)
  end
end
