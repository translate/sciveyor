# frozen_string_literal: true

ENV["RAILS_ENV"] ||= "test"
require_relative "support/simplecov"
require_relative "../config/environment"
require "rails/test_help"

# Load some gems
require "fileutils"
require "capybara/rails"
require "mocha/minitest"
require "byebug"

# Require all of my test helpers and assertions
(
  Dir[Rails.root.join("test", "support", "**", "*_helper.rb")] +
    Dir[Rails.root.join("test", "support", "**", "assert_*.rb")]
).each { |helper| require helper }

# Global configuration
unless ENV["TEST_S3"].present?
  require "webmock/minitest"

  WebMock.disable_net_connect!(
    allow_localhost: true,
    # Allow the geckodriver downloader to go get drivers for us
    allow: %w[github.com objects.githubusercontent.com]
  )
end

Mocha.configure do |c|
  # Distinguish keyword arguments from hash parameters
  c.strict_keyword_argument_matching = true
end

# Reset the temporary storage
FileUtils.rm_rf Dir.glob(Rails.root.join("tmp", "test_storage"))
Dir.mkdir(Rails.root.join("tmp", "test_storage"))

# Helpers for all tests
module ActiveSupport
  class TestCase
    # Activate helpers from gems
    include FactoryBot::Syntax::Methods

    # General test helpers
    include FixtureFileHelper
    include StubConnectionHelper
  end
end

# Helpers for controller and integration tests
module ActionDispatch
  class IntegrationTest
    include Devise::Test::IntegrationHelpers
  end
end
