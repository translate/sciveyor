# frozen_string_literal: true

require "test_helper"

class UploadTest < ActiveSupport::TestCase
  test "should be invalid without description" do
    upload = build_stubbed(:upload, description: nil)

    refute upload.valid?
  end

  test "should be valid with description" do
    upload = create(:upload)

    assert upload.valid?
  end
end
