# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class TaskTest
      def perform(task, options)
        puts "wat"
      end
    end
  end
end

class TaskTest < ActiveSupport::TestCase
  include ActionMailer::TestHelper

  test "should get list of task types" do
    assert_includes Task.job_list, Sciveyor::Jobs::ExportCitations
  end

  test "should be invalid without name" do
    task = build_stubbed(:task, name: nil)

    refute task.valid?
  end

  test "should be valid without dataset" do
    task = build_stubbed(:task, datasets: [])

    assert task.valid?
  end

  test "should be invalid without type" do
    task = build_stubbed(:task, type: nil)

    refute task.valid?
  end

  test "should be valid with all parameters" do
    task = create(:task)

    assert task.valid?
  end

  test "to_s should work for active" do
    task = create(:task)

    assert_includes task.to_s, task.name
    assert_includes task.to_s, "active"
  end

  test "to_s should work for failed" do
    task = create(:task, finished: true, failed: true, finished_at: 2.days.ago)

    assert_includes task.to_s, "failed"
  end

  test "to_s should work for finished" do
    task = create(:task, finished: true, finished_at: 2.days.ago)

    assert_includes task.to_s, "finished"
  end

  test "should return template path" do
    task = create(:task, type: "ExportCitations")
    path = task.template_path("test")

    assert_equal "jobs/export_citations/test", path
  end

  test "should default to nil finished_at" do
    task = create(:task)

    assert_nil task.finished_at
  end

  test "should default to false failed" do
    task = create(:task)

    refute task.failed
  end

  test "should return JSON when available" do
    task = create(:task, type: "ExportCitations")
    create(
      :upload,
      task: task,
      filename: "task-test.json",
      content_type: "application/json"
    ) { |f| f.from_string("{\"abc\":123}") }

    assert_equal "{\"abc\":123}", task.reload.json
  end

  test "should return nil when no JSON available" do
    task = create(:task, type: "ExportCitations")

    assert_nil task.json
  end

  test "should associate with uploads" do
    task = create(:task, type: "ExportCitations")
    create(:upload, task: task) { |f| f.from_string("test") }

    Sciveyor::Storage.get(task.uploads.first.filename) do |io, content_type|
      io.set_encoding "UTF-8"
      assert_equal "text/plain", content_type
      assert_equal "test", io.read
    end
  end

  test "should return good job_class (class method)" do
    klass = Task.job_class("ExportCitations")

    assert_equal Sciveyor::Jobs::ExportCitations, klass
  end

  test "should raise error for bad job_class (class method)" do
    assert_raises(ArgumentError) { Task.job_class("NotClass") }
  end

  test "should return good job_class" do
    task = create(:task, type: "ExportCitations")

    assert_equal Sciveyor::Jobs::ExportCitations, task.job_class
  end

  test "should raise error for bad job_class" do
    task = create(:task)

    assert_raises(ArgumentError) { task.job_class }
  end

  test "job_create works when blocking" do
    ENV["BLOCKING_JOBS"] = "true"

    task = create(:task, type: "TaskTest")
    Sciveyor::Jobs::TaskTest.any_instance.expects(:perform).with(task, {})

    task.job_start

    ENV["BLOCKING_JOBS"] = "false"
  end

  test "job_create works when non-blocking" do
    task = create(:task, type: "ExportCitations")
    task.job_start(test: "thing")

    # Check that we created everything we should
    assert_not_equal "Task", task.name
    assert_equal "{\"test\":\"thing\"}", task.job_params
    refute task.finished
    refute task.failed
    refute task.finished_at
    assert task.job_message

    assert_equal 1, Job.count
    assert_equal task, Job.first.task
  end

  test "mark_completed works" do
    task = create(:task)

    task.mark_completed

    assert task.finished
    refute task.failed
    assert task.finished_at
  end

  test "mark_failed works" do
    task = create(:task)

    task.mark_failed("lastmessage")

    assert task.finished
    assert task.failed
    assert task.finished_at
    assert_equal "lastmessage", task.job_message
  end
end
