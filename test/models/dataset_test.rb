# frozen_string_literal: true

require "test_helper"

class DatasetTest < ActiveSupport::TestCase
  test "should be valid on create (uuid generation)" do
    dataset = create(:dataset)

    assert dataset.valid?
  end

  test "should associate with tasks" do
    dataset = create(:dataset)
    create(:task, datasets: [dataset], name: "test")

    assert_equal 1, dataset.tasks.reload.size
    assert_equal "test", dataset.tasks[0].name
  end

  test "should build queries" do
    dataset = create(:full_dataset, num_docs: 2)

    assert_equal 1, dataset.q.size
  end

  test "should save number of documents" do
    dataset = create(:full_dataset, num_docs: 2)

    assert_equal 2, dataset.document_count
  end

  test "to_s should work" do
    dataset = create(:dataset)

    assert_includes dataset.to_s, dataset.uuid
  end

  test "find_or_create_by finds existing" do
    dataset = create(:full_dataset)

    split = dataset.q[0].partition(":")
    params =
      ActionController::Parameters.new(
        {
          fields: [split[0]],
          values: [split[2].delete_prefix("(").delete_suffix(")")],
          fq: dataset.fq
        }
      )
    search_params = Sciveyor::Solr::Params.new(params: params)
    test = Dataset.find_or_create_by(search_params.to_dataset)

    assert_equal 1, Dataset.count
    assert_equal dataset.uuid, test.uuid
  end

  test "find_or_create_by creates non-existing" do
    dataset = create(:full_dataset)

    search_params =
      Sciveyor::Solr::Params.new(params: ActionController::Parameters.new)
    test = Dataset.find_or_create_by(search_params.to_dataset)

    assert_equal 2, Dataset.count
    refute_equal dataset.uuid, test.uuid
  end
end
