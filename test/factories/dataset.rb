# frozen_string_literal: true

WORKING_IDS ||= %w[
  doi:10.1371/journal.pntd.0000534
  doi:10.1371/journal.pntd.0000535
  doi:10.1371/journal.pntd.0000536
  doi:10.1371/journal.pntd.0000537
  doi:10.1371/journal.pntd.0000538
  doi:10.1371/journal.pntd.0000539
  doi:10.1371/journal.pntd.0000540
  doi:10.1371/journal.pntd.0000541
  doi:10.1371/journal.pntd.0000542
  doi:10.1371/journal.pntd.0000543
].freeze

FactoryBot.define do
  sequence :working_id do |n|
    WORKING_IDS[n % WORKING_IDS.size]
  end

  factory :dataset do
    q { [] }
    fq { [] }
    boolean { "and" }

    factory :full_dataset do
      transient { num_docs { 5 } }

      after(:create) do |dataset, evaluator|
        if evaluator.num_docs.positive?
          ids =
            (1..evaluator.num_docs).to_a.map do
              "\"#{FactoryBot.generate(:working_id)}\""
            end
          dataset.q << "id:(#{ids.join(" OR ")})"
          dataset.save
        end
      end
    end
  end
end
