# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    name { "Test Category" }
    journals { ["PLoS Neglected Tropical Diseases"] }
  end

  factory :library do
    name { "Harvard" }
    sequence(:url) { |n| "http://sfx.hul.harvard#{n}.edu/sfx_local?" }
    user
  end

  factory :named_entities, class: Hash do
    transient { entity_hash { { "PERSON" => %w[Tom Dick Harry] } } }

    initialize_with { entity_hash }
  end

  factory :task do
    name { "Task" }
    type { "Fake" }
    user { create(:user) }
    datasets { [create(:dataset)] }
    attempts { 0 }
  end

  factory :upload do
    filename { "test.txt" }
    content_type { "text/plain" }
    description { "A task file" }
    short_description { "File" }
    task
  end

  factory :user do
    name { "John Doe" }
    sequence(:email) { |n| "person#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
    remember_me { false }
    language { "en" }
    timezone { "America/New_York" }
  end
end
