# frozen_string_literal: true

require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "cannot create export if not signed in" do
    get user_export_path

    assert_redirected_to(root_path)
  end

  test "cannot create export if user did so too recently" do
    user = create(:user, export_requested_at: 2.hours.ago)
    sign_in user

    get user_export_path

    assert_response 500
  end

  test "creating export enqueues job" do
    user = create(:user)
    sign_in user

    assert_equal 0, Job.count

    get user_export_path

    assert_equal 1, Job.count
  end

  test "creating export updates request time" do
    old_at = 5.days.ago
    user = create(:user, export_requested_at: old_at)
    sign_in user

    get user_export_path

    assert_not_equal user.export_requested_at, old_at
  end

  test "cannot delete export if not signed in" do
    delete user_export_path

    assert_redirected_to(root_path)
  end

  test "cannot delete export if no export attached" do
    user = create(:user)
    sign_in user

    delete user_export_path

    assert_response 404
  end

  test "deleting export does delete file" do
    user = create(:user)
    user.export_filename = "export-test.zip"
    user.export_content_type = "application/zip"
    Sciveyor::Storage.upload(
      StringIO.new("this is not really a zip file"),
      user.export_filename
    )
    sign_in user

    delete user_export_path

    assert_raises(Sciveyor::Storage::Error) do
      Sciveyor::Storage.get("export-test.zip") { |io, type| }
    end
  end
end
