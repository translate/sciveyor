# frozen_string_literal: true

require "test_helper"

class ListsControllerTest < ActionDispatch::IntegrationTest
  test "should get authors" do
    get lists_authors_url

    assert_response :success
    assert_includes @response.body, "Peter J. Hotez"

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_instance_of Hash, obj[0]
    refute_nil obj[0]["value"]
  end

  test "should get authors with a filter" do
    get lists_authors_url(filter: "boel")

    assert_response :success
    assert_includes @response.body, "Marleen Boelaert"
    refute_includes @response.body, "Peter J. Hotez"

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_instance_of Hash, obj[0]
    refute_nil obj[0]["value"]
  end

  test "should get journals" do
    get lists_journals_url

    assert_response :success
    assert_includes @response.body, "PLoS Neglected Tropical Diseases"

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_instance_of Hash, obj[0]
    refute_nil obj[0]["value"]
  end

  test "should get journals with a filter" do
    get lists_journals_url(filter: "negle")

    assert_response :success
    assert_includes @response.body, "PLoS Neglected Tropical Diseases"

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_instance_of Hash, obj[0]
    refute_nil obj[0]["value"]
  end

  test "limit should work" do
    get lists_journals_url(limit: 1)

    assert_response :success

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_equal 1, obj.count
    assert_equal "PLoS Neglected Tropical Diseases", obj[0]["value"]
  end

  test "limit <= 0 should return bad request" do
    get lists_authors_url(limit: 0)

    assert_response :bad_request
  end

  test "offset should work" do
    get lists_journals_url(offset: 1)

    assert_response :success

    obj = JSON.parse(@response.body)
    assert_instance_of Array, obj
    assert_equal 1, obj.count
    assert_equal "PNTDS", obj[0]["value"]
  end

  test "offset < 0 should return bad request" do
    get lists_journals_url(offset: -10)

    assert_response :bad_request
  end
end
