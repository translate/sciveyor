# frozen_string_literal: true

require "test_helper"

# To test the S3 connection code, you need to activate a local environment
# file with all of the S3 variables (pointing to a test bucket, obviously!),
# and the variable TEST_S3 set to true.
#
# You should then *only* run this file:
# bin/rails test test/lib/sciveyor/storage.rb
#
# It's recommended to put those variables into a file named .env.s3test (or
# something similar). Then, when you want to run the S3 tests, move that file
# to .env.test.local and it will be transparently applied over the top of the
# other settings in .env.test.
#
# Note that there's no real reason to run the remote S3 tests unless you change
# the S3 SDK code, or are worried that there's something there that isn't being
# done correctly. It's been tested locally, and what's there works.

module Sciveyor
  class StorageTest < ActiveSupport::TestCase
    def self.make_test_name(str)
      ENV["TEST_S3"].present? ? str + " (S3)" : str + " (local files)"
    end

    test make_test_name("upload/get works") do
      io = StringIO.new("temporary string")
      Sciveyor::Storage.upload(io, "test-upload.txt")

      Sciveyor::Storage.get("test-upload.txt") do |io, type|
        io.set_encoding "UTF-8"

        assert_equal "text/plain", type
        assert_equal "temporary string", io.read
      end
    end

    test make_test_name("upload fails if file exists") do
      io = StringIO.new("temporary string")
      Sciveyor::Storage.upload(io, "test-dupe-upload.txt")

      io.rewind
      assert_raises(Sciveyor::Storage::Error) do
        Sciveyor::Storage.upload(io, "test-dupe-upload.txt")
      end
    end

    test make_test_name("get fails for missing file") do
      assert_raises(Sciveyor::Storage::Error) do
        Sciveyor::Storage.get("test-missing.txt") { |io, type| flunk }
      end
    end

    test make_test_name("delete works") do
      io = StringIO.new("temporary string")
      Sciveyor::Storage.upload(io, "test-delete.txt")

      Sciveyor::Storage.delete("test-delete.txt")

      assert_raises(Sciveyor::Storage::Error) do
        Sciveyor::Storage.get("test-delete.txt") { |io, type| flunk }
      end
    end

    test make_test_name("delete works even for missing file") do
      Sciveyor::Storage.delete("missingno")
    end
  end
end
