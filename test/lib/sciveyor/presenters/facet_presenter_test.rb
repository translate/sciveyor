# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Presenters
    class FacetPresenterTest < ActiveSupport::TestCase
      test "label reproduces authors" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "W. Shatner", pres.label
      end

      test "label reproduces journals" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "journal",
            value: "\"The Journal\"",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "The Journal", pres.label
      end

      test "label creates proper decade labels" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "date",
            value: "1960-01-01T00:00:00Z",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "1960–1969", pres.label
      end

      test "label creates the before-year label correctly" do
        f = Sciveyor::Solr::Facet.new(field: "date", value: "before", hits: 10)
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "before 1800", pres.label
      end

      test "label creates the after-year label correctly" do
        f = Sciveyor::Solr::Facet.new(field: "date", value: "after", hits: 10)
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "2020 and later", pres.label
      end

      test "label throws for invalid fields" do
        f =
          stub(
            field: :space_facet,
            value: "Spaceman Spiff",
            hits: 10,
            to_hash: {
              field: :space_facet,
              value: "Spaceman Spiff",
              hits: 10
            }
          )

        assert_raises(ArgumentError) do
          Sciveyor::Presenters::FacetPresenter.new(facet: f).label
        end
      end

      test "field_label has label for author" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "author",
            value: "\"W. Shatner\"",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "Authors", pres.field_label
      end

      test "field_label has label for journal" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "journal",
            value: "\"The Journal\"",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "Journal", pres.field_label
      end

      test "field_label has label for year" do
        f =
          Sciveyor::Solr::Facet.new(
            field: "date",
            value: "1960-01-01T00:00:00Z",
            hits: 10
          )
        pres = Sciveyor::Presenters::FacetPresenter.new(facet: f)

        assert_equal "Date", pres.field_label
      end

      test "field_label throws for invalid fields" do
        f =
          stub(
            field: :space_facet,
            value: "Spaceman Spiff",
            hits: 10,
            to_hash: {
              field: :space_facet,
              value: "Spaceman Spiff",
              hits: 10
            }
          )

        assert_raises(ArgumentError) do
          Sciveyor::Presenters::FacetPresenter.new(facet: f).field_label
        end
      end
    end
  end
end
