# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Presenters
    class TaskPresenterTest < ActiveSupport::TestCase
      test "dataset_list works with one dataset" do
        task = create(:task)
        task.user.add_dataset(task.datasets[0], "Dataset")
        pres = Sciveyor::Presenters::TaskPresenter.new(task: task)

        assert_equal "Dataset", pres.dataset_list
      end

      test "dataset_list works with two datasets" do
        task = create(:task)
        dataset = create(:dataset)
        task.datasets << dataset
        task.user.add_dataset(task.datasets[0], "Dataset")
        task.user.add_dataset(task.datasets[1], "Other Dataset")
        pres = Sciveyor::Presenters::TaskPresenter.new(task: task)

        assert_equal "Dataset and Other Dataset", pres.dataset_list
      end
    end
  end
end
