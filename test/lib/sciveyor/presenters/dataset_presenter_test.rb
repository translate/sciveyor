# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Presenters
    class DatasetPresenterTest < ActiveSupport::TestCase
      test "q_string works for dismax" do
        d = Dataset.new(q: [" {!edismax v=\"test\"}"])
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: d)

        assert_equal "Normal search: test", pres.q_string(d.q[0])
      end

      test "q_string works for authors" do
        d =
          Dataset.new(
            q: [" {!parent which=\"type:article\"}name_clean:(\"John Doe\")"]
          )
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: d)

        assert_equal "Authors: John Doe", pres.q_string(d.q[0])
      end

      test "q_string works for a normal field" do
        d = Dataset.new(q: %w[volume:(30) number:(5) pages:(100-101)])
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: d)

        assert_equal "Volume: 30", pres.q_string(d.q[0])
        assert_equal "Number: 5", pres.q_string(d.q[1])
        assert_equal "Pages: 100-101", pres.q_string(d.q[2])
      end

      test "fq_string uses facet decorators (date)" do
        q =
          Dataset.new(
            q: ["testing"],
            fq: [
              "+type:article +date:[1960-01-01T00:00:00Z TO 1960-01-01T00:00:00Z+10YEARS]"
            ]
          )
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: q)

        assert_equal "Date: 1960–1969", pres.fq_string[0]
      end

      test "fq_string uses facet decorators (author)" do
        q =
          Dataset.new(
            q: ["testing"],
            fq: ["{!parent which=\"type:article\"}(+name:\"John Doe\")"]
          )
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: q)

        assert_equal "Authors: John Doe", pres.fq_string[0]
      end

      test "fq_string uses facet decorators (journal)" do
        q =
          Dataset.new(
            q: ["testing"],
            fq: ["+type:article +journal:\"Journal of Test\""]
          )
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: q)

        assert_equal "Journal: Journal of Test", pres.fq_string[0]
      end

      test "fq_string returns nil if no facets" do
        q = Dataset.new(q: ["testing"])
        pres = Sciveyor::Presenters::DatasetPresenter.new(dataset: q)

        assert_nil pres.fq_string
      end
    end
  end
end
