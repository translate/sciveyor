# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class CooccurrenceTest < ActiveSupport::TestCase
      def perform
        dataset = create(:dataset)
        dataset.q << "id:(doi:10.1371/journal.pntd.0000540)"
        dataset.save

        @task = create(:task, type: "Cooccurrence", datasets: [dataset])
        run_job_now(
          @task,
          "scoring" => "t_test",
          "words" => "disease",
          "window" => "6",
          "min_count" => "1",
          "num_pairs" => "10"
        )
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, Cooccurrence.num_datasets
      end

      types = %w[mutual_information t_test log_likelihood]
      words_list = ["disease", "global disease"]
      nums = [%w[num_pairs 10], %w[all 1]]
      types
        .product(words_list)
        .product(nums)
        .each do |((type, words), (mode, num))|
          test "should run with type '#{type}', mode '#{mode}', and words '#{words}'" do
            dataset = create(:dataset)
            dataset.q << "id:(doi:10.1371/journal.pntd.0000540)"
            dataset.save

            task = create(:task, type: "Cooccurrence", datasets: [dataset])

            run_job_now(
              task,
              "scoring" => type,
              mode => num,
              "window" => "25",
              "min_count" => "1",
              "words" => words
            )

            assert_equal "Determine significant associations between distant pairs of words",
                         task.reload.name

            # There should be at least one cooccurrence in there ("word word,X.YYYY...")
            assert_match(
              /\n\w+ \w+,-?\d+(\.\d+)?/,
              task.uploads.reload.first.get_string
            )
          end
        end

      test "should return significance test names" do
        assert_includes Cooccurrence.significance_tests,
                        ["Log-likelihood", :log_likelihood]
      end
    end
  end
end
