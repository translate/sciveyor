# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class WordFrequencyTest < ActiveSupport::TestCase
      def perform
        @task =
          create(
            :task,
            type: "WordFrequency",
            datasets: [create(:full_dataset, num_docs: 2)]
          )
        run_job_now(@task)
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, WordFrequency.num_datasets
      end

      test "should accept all combinations of parameters" do
        params_to_test = [
          { "block_size" => "100", "split_across" => "1", "num_words" => "0" },
          {
            "block_size" => "100",
            "split_across" => "1",
            "word_method" => "all"
          },
          { "block_size" => "100", "split_across" => "0", "num_words" => "0" },
          { "num_blocks" => "10", "split_across" => "1", "num_words" => "0" },
          {
            "num_blocks" => "10",
            "split_across" => "1",
            "num_words" => "0",
            "inclusion_list" => "asdf,sdfhj,wert"
          },
          {
            "num_blocks" => "10",
            "split_across" => "1",
            "num_words" => "0",
            "exclusion_list" => "asdf,sdfgh,qwert"
          },
          {
            "num_blocks" => "1",
            "split_across" => "1",
            "num_words" => "0",
            "stop_list" => "en"
          },
          {
            "num_blocks" => "1",
            "split_across" => "1",
            "ngrams" => "2",
            "all" => "1"
          }
        ]

        task =
          create(
            :task,
            type: "WordFrequency",
            datasets: [create(:full_dataset, num_docs: 2)]
          )

        params_to_test.each { |params| run_job_now(task, params) }
      end

      test "should work" do
        task =
          create(
            :task,
            type: "WordFrequency",
            datasets: [create(:full_dataset, num_docs: 2)]
          )

        run_job_now(
          task,
          "block_size" => "100",
          "split_across" => "1",
          "num_words" => "0"
        )

        assert_equal "Analyze word frequency in dataset", task.reload.name

        data = CSV.parse(task.upload_for("text/csv").get_string)
        assert_kind_of Array, data
      end

      test "should still work when no corpus dfs are returned" do
        analyzer =
          stub(
            blocks: [
              { "word" => 2, "other" => 5 },
              { "word" => 1, "other" => 6 }
            ],
            block_stats: [
              { name: "first block", tokens: 7, types: 2 },
              { name: "second block", tokens: 7, types: 2 }
            ],
            word_list: %w[word other],
            tf_in_dataset: {
              "word" => 3,
              "other" => 11
            },
            df_in_dataset: {
              "word" => 2,
              "other" => 3
            },
            num_dataset_tokens: 14,
            num_dataset_types: 2,
            df_in_corpus: nil
          )
        Sciveyor::Analysis::Frequency.expects(:call).returns(analyzer)

        task =
          create(
            :task,
            type: "WordFrequency",
            datasets: [create(:full_dataset, num_docs: 2)]
          )

        run_job_now(
          task,
          "block_size" => "100",
          "split_across" => "1",
          "num_words" => "0"
        )

        data = CSV.parse(task.upload_for("text/csv").get_string)
        assert_kind_of Array, data
      end
    end
  end
end
