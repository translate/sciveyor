# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class NetworkTest < ActiveSupport::TestCase
      def perform
        @task = create(:task, type: "Network")
        @task.datasets[0].q << "id:\"#{WORKING_IDS[2]}\""
        @task.datasets[0].save
        run_job_now(@task, "focal_word" => "diseases")
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, Network.num_datasets
      end

      test "should work" do
        task = create(:task, type: "Network")
        task.datasets[0].q << "id:\"#{WORKING_IDS[2]}\""
        task.datasets[0].save
        task.user.add_dataset(task.datasets[0], "Dataset")

        run_job_now(task, "focal_word" => "diseases")

        assert_equal "Compute network of associated terms", task.reload.name

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        assert_equal "Dataset", data["name"]
        assert_equal "diseases", data["focal_word"]

        global_node = data["d3_nodes"].index { |n| n["name"] == "global" }
        diseas_node = data["d3_nodes"].index { |n| n["name"] == "diseas" }

        edge =
          data["d3_links"].find do |l|
            l["source"] == global_node && l["target"] == diseas_node
          end

        assert_in_epsilon 1.0, edge["strength"]
      end
    end
  end
end
