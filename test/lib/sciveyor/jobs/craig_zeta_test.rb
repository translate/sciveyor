# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class CraigZetaTest < ActiveSupport::TestCase
      def perform
        dataset = create(:full_dataset, num_docs: 1)
        dataset2 = create(:full_dataset, num_docs: 1)
        @task = create(:task, type: "CraigZeta", datasets: [dataset, dataset2])
        run_job_now(@task)
      end

      include JobHelper

      test "should need two datasets" do
        assert_equal 2, CraigZeta.num_datasets
      end

      test "should raise without enough datasets" do
        assert_raises(ArgumentError) do
          # Call perform manually, as this failure would otherwise be caught by
          # the failure handler
          CraigZeta.new.perform(create(:task, type: "CraigZeta"))
        end
      end

      test "should raise with too many datasets" do
        task = create(:task, type: "CraigZeta")
        dataset = create(:dataset)
        task.datasets << dataset
        task.datasets << dataset

        assert_raises(ArgumentError) do
          # Call perform manually, as this failure would otherwise be caught by
          # the failure handler
          CraigZeta.new.perform(task)
        end
      end

      test "should work" do
        task =
          create(
            :task,
            type: "CraigZeta",
            datasets: [
              create(:full_dataset, num_docs: 1),
              create(:full_dataset, num_docs: 1)
            ]
          )
        task.user.add_dataset(task.datasets[0], "Dataset One")
        task.user.add_dataset(task.datasets[1], "Dataset Two")
        run_job_now(task)

        assert_equal "Determine words that differentiate two datasets (Craig Zeta)",
                     task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        # Data is reasonable
        assert_equal "Dataset One", data["name_1"]
        assert_equal "Dataset Two", data["name_2"]
        assert_kind_of Array, data["markers_1"]
        assert_kind_of Array, data["markers_2"]
        assert_kind_of Array, data["graph_points"]
        assert_kind_of Array, data["zeta_scores"]
      end
    end
  end
end
