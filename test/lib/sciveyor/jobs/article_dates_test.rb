# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class ArticleDatesTest < ActiveSupport::TestCase
      def perform
        dataset = create(:full_dataset, num_docs: 1)
        @task = create(:task, type: "ArticleDates", datasets: [dataset])
        run_job_now(@task)
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, ArticleDates.num_datasets
      end

      test "should work when not normalizing" do
        dataset = create(:full_dataset)

        # Add another article from a much later year, so that we get some
        # intervening zeros
        dataset.q << "id:\"doi:10.1371/journal.pntd.0001716\""
        dataset.boolean = "or"
        dataset.save

        task = create(:task, type: "ArticleDates", datasets: [dataset])

        run_job_now(task, "normalize" => "0")

        assert_equal "Plot number of articles by date", task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        # Data is reasonable
        assert_includes 2009..2012, data["data"][0][0]
        assert_includes 1..5, data["data"][0][1]

        # Fills in intervening years between new and old documents with zeros
        refute_nil(data["data"].find { |y| y[1].zero? })

        # Data is sorted by year
        assert_equal data["data"].sort_by { |d| d[0] }, data["data"]
      end

      test "should work when normalizing to corpus" do
        task =
          create(:task, type: "ArticleDates", datasets: [create(:full_dataset)])

        run_job_now(task, "normalize" => "1", "normalization_dataset" => "")

        assert_equal "Plot number of articles by date", task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        # Save the normalization set in the data
        assert_equal "Entire Corpus", data["normalization_set"]
        assert data["percent"]

        # Data is reasonable
        assert_includes 1859..2012, data["data"][0][0]
        assert_includes 0..1, data["data"][0][1]

        # Fills in intervening years between new and old documents with zeros
        refute_nil(data["data"].find { |y| y[1].zero? })

        # Data is sorted by year
        assert_equal data["data"].sort_by { |d| d[0] }, data["data"]
      end

      test "should work when normalizing to a dataset" do
        normalization_set = create(:full_dataset, num_docs: 10)
        task =
          create(:task, type: "ArticleDates", datasets: [create(:full_dataset)])
        task.user.add_dataset(normalization_set, "normalize test")

        run_job_now(
          task,
          "normalize" => "1",
          "normalization_dataset" => normalization_set.to_param
        )

        assert_equal "Plot number of articles by date", task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        # Save the normalization set in the data
        assert_equal normalization_set.name_for(task.user),
                     data["normalization_set"]
        assert data["percent"]

        # Data is reasonable
        assert_includes 1859..2012, data["data"][0][0]
        assert_includes 0..1, data["data"][0][1]

        # Data is sorted by year
        assert_equal data["data"].sort_by { |d| d[0] }, data["data"]
      end

      # We want to make sure it still works when we normalize to a dataset where
      # the dataset of interest isn't a subset
      test "should work when normalizing badly" do
        normalization_set = create(:dataset)
        normalization_set.q << "id:\"doi:10.1371/journal.pntd.0001716\""
        normalization_set.save
        task =
          create(:task, type: "ArticleDates", datasets: [create(:full_dataset)])

        run_job_now(
          task,
          "normalize" => "1",
          "normalization_dataset" => normalization_set.to_param
        )
        data = JSON.parse(task.reload.json)

        data["data"].each { |a| assert_equal 0, a[1] }
      end
    end
  end
end
