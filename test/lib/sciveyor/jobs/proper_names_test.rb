# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class ProperNamesTest < ActiveSupport::TestCase
      def perform
        dataset = create(:full_dataset, num_docs: 1)
        @task = create(:task, type: "ProperNames", datasets: [dataset])
        run_job_now(@task)
      end

      include JobHelper

      test "should need one dataset" do
        assert_equal 1, ProperNames.num_datasets
      end

      test "should be available" do
        assert ProperNames.available?
      end

      test "should work" do
        task =
          create(:task, type: "ProperNames", datasets: [create(:full_dataset)])

        run_job_now(task)

        assert_equal "Extract references to proper names", task.reload.name
        assert_equal 2, task.uploads.count
        refute_nil task.upload_for("application/json")
        refute_nil task.upload_for("text/csv")

        data = JSON.parse(task.json)
        assert_kind_of Hash, data

        refute_empty data["names"]
        assert_kind_of String, data["names"][0].first
        assert_kind_of Numeric, data["names"][0].second
      end
    end
  end
end
