# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Jobs
    class UserExportTest < ActiveSupport::TestCase
      def perform
        @task = create(:task, type: "UserExport")
        run_job_now(@task)
      end

      include JobHelper

      test "should export all user data" do
        user = create(:user)

        # One library entry
        create(
          :library,
          user: user,
          name: "A library",
          url: "https://google.com?"
        )

        # Three datasets
        dataset1 = create(:full_dataset, num_docs: 3)
        dataset2 = create(:full_dataset, num_docs: 3)
        dataset3 = create(:full_dataset, num_docs: 3)

        user.add_dataset(dataset1, "First Dataset")
        user.add_dataset(dataset2, "Second Dataset")
        user.add_dataset(dataset3, "Empty Dataset")

        # One has two tasks, two has one task
        task11 =
          create(
            :task,
            user: user,
            datasets: [dataset1],
            type: "ExportCitations",
            finished_at: Time.current
          )
        task12 =
          create(
            :task,
            user: user,
            datasets: [dataset1],
            type: "MultipleUploads",
            finished_at: Time.current
          )
        task21 =
          create(
            :task,
            user: user,
            datasets: [dataset2],
            type: "ExportCitations",
            finished_at: Time.current
          )

        file11 = create(:upload, task: task11, filename: "export-test-1.txt")
        file21 = create(:upload, task: task21, filename: "export-test-2.txt")
        file11.from_string(
          "these two will have the same content_type and filename"
        )
        file21.from_string(
          "these two will have the same content_type and filename"
        )

        # The second task has multiple uploads
        file121 =
          create(
            :upload,
            task: task12,
            filename: "export-test-3.csv",
            content_type: "text/csv"
          )
        file121.from_string("this is a first one")
        file122 =
          create(
            :upload,
            task: task12,
            filename: "export-test-4.txt",
            content_type: "text/plain"
          )
        file122.from_string("this is another one")

        # And do the export
        task = create(:task, datasets: [], user: user, type: "UserExport")
        run_job_now(task)

        assert user.reload.export_filename.start_with?("export")
        assert_equal "application/zip", user.export_content_type

        zip_contents = {}
        Sciveyor::Storage.get(user.export_filename) do |io, type|
          # Unpack the zip into a hash
          ::Zip::InputStream.open(io) do |zis|
            while (entry = zis.get_next_entry)
              zip_contents[entry.name] = zis.read
            end
          end
        end

        # Parse some bits from everything and check some random spots
        user_json = zip_contents["user.json"]
        assert user_json

        user_hash = JSON.parse(user_json)
        assert_equal user.name, user_hash["name"]
        assert_equal user.email, user_hash["email"]

        libraries_json = zip_contents["libraries.json"]
        assert libraries_json

        libraries_array = JSON.parse(libraries_json)
        assert_kind_of Array, libraries_array
        assert_kind_of Hash, libraries_array[0]
        assert_equal "A library", libraries_array[0]["name"]

        tasks = user_hash["tasks"]
        assert_equal 3, tasks.count

        big_task = tasks.find { |t| t["uploads"].count == 2 }
        small_tasks = tasks.select { |t| t["uploads"].count == 1 }

        assert big_task
        assert small_tasks.count == 2

        # And make sure all the uploads are actually present, too
        big_task_file_count = 0
        big_task["uploads"].each do |f|
          big_task_file_count += 1 if zip_contents[f]
        end
        assert_equal 2, big_task_file_count
        assert zip_contents[small_tasks[0]["uploads"][0]]
        assert zip_contents[small_tasks[1]["uploads"][0]]

        datasets_json = zip_contents["datasets.json"]
        assert datasets_json

        datasets_array = JSON.parse(datasets_json)
        assert_kind_of Array, datasets_array
        assert_equal 3, datasets_array.count

        dataset_1_hash =
          datasets_array.find { |h| h["name"] == "First Dataset" }
        dataset_2_hash =
          datasets_array.find { |h| h["name"] == "Second Dataset" }
        dataset_3_hash =
          datasets_array.find { |h| h["name"] == "Empty Dataset" }

        assert dataset_1_hash
        assert dataset_2_hash
        assert dataset_3_hash

        assert_equal 1, dataset_1_hash["q"].count
        assert_kind_of String, dataset_2_hash["q"][0]

        assert_equal 7, zip_contents.count
      end
    end
  end
end
