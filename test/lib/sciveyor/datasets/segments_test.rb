# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Datasets
    class SegmentsTest < ActiveSupport::TestCase
      test "with one dataset block, splitting across, it works" do
        analyzer =
          Sciveyor::Datasets::Segments.new(
            dataset: create(:full_dataset, num_docs: 10)
          )
        segments = analyzer.segments

        assert_equal 1, segments.size
        assert_equal 3471, segments[0].words.size
        assert_equal "Block #1/1", segments[0].name
        assert_equal 9, analyzer.dfs["disease"]
        assert_equal 1125, analyzer.corpus_dfs["disease"]
      end

      test "with one block per document, not splitting across, it works" do
        analyzer =
          Sciveyor::Datasets::Segments.new(
            dataset: create(:full_dataset, num_docs: 10),
            split_across: false
          )
        segments = analyzer.segments

        assert_equal 10, segments.size
        assert segments[0].name.start_with?("Block #1/1 (within ‘doi:10.")
        assert_equal 9, analyzer.dfs["disease"]
        assert_equal 1125, analyzer.corpus_dfs["disease"]
      end

      test "with five total blocks, splitting across, it works" do
        analyzer =
          Sciveyor::Datasets::Segments.new(
            dataset: create(:full_dataset, num_docs: 10),
            num_blocks: 5
          )
        segments = analyzer.segments

        assert_equal 5, segments.size
        assert_equal [695, 694, 694, 694, 694],
                     segments.map(&:words).map(&:size)
        assert_equal "Block #1/5", segments[0].name
        assert_equal 9, analyzer.dfs["disease"]
        assert_equal 1125, analyzer.corpus_dfs["disease"]
      end

      test "with truncate_all, splitting across, it works" do
        analyzer =
          Sciveyor::Datasets::Segments.new(
            dataset: create(:full_dataset, num_docs: 10),
            block_size: 10,
            last_block: :truncate_all
          )
        segments = analyzer.segments

        assert_equal 1, segments.size
        assert_equal 10, segments[0].words.size
        assert_equal "Block #1 of 10 words", segments[0].name
        assert_equal 9, analyzer.dfs["disease"]
        assert_equal 1125, analyzer.corpus_dfs["disease"]
      end

      test "with truncate_all, not splitting across, it works" do
        analyzer =
          Sciveyor::Datasets::Segments.new(
            dataset: create(:full_dataset, num_docs: 10),
            split_across: false,
            block_size: 10,
            last_block: :truncate_all
          )
        segments = analyzer.segments

        assert_equal 10, segments.size
        assert_equal 10, segments[0].words.size
        assert_equal 10, segments[1].words.size
        assert segments[0].name.start_with?(
                 "Block #1 of 10 words (within ‘doi:10."
               )
        assert_equal 9, analyzer.dfs["disease"]
        assert_equal 1125, analyzer.corpus_dfs["disease"]
      end
    end
  end
end
