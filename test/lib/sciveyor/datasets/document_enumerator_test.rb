# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Datasets
    class DocumentEnumeratorTest < ActiveSupport::TestCase
      test "with no custom fields, enumerates documents" do
        enum =
          Sciveyor::Datasets::DocumentEnumerator.new(
            dataset: create(:full_dataset, num_docs: 2)
          )

        assert_includes WORKING_IDS, enum.first.id
      end

      test "with no custom fields, includes no term vectors" do
        enum =
          Sciveyor::Datasets::DocumentEnumerator.new(
            dataset: create(:full_dataset, num_docs: 2)
          )

        assert_nil enum.first.term_vectors
      end

      test "with no custom fields, throws if Solr fails" do
        enum =
          Sciveyor::Datasets::DocumentEnumerator.new(
            dataset: create(:full_dataset, num_docs: 2)
          )
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout

        assert_raises(Sciveyor::Solr::Connection::Error) { enum.each { |_| } }
      end

      test "with term vectors, it returns term vectors" do
        enum =
          Sciveyor::Datasets::DocumentEnumerator.new(
            dataset: create(:full_dataset, num_docs: 2),
            term_vectors: true
          )

        refute_nil enum.first.term_vectors
      end

      test "with custom fields, it only includes those" do
        enum =
          Sciveyor::Datasets::DocumentEnumerator.new(
            dataset: create(:full_dataset, num_docs: 2),
            fl: "date"
          )

        assert_nil enum.first.title
        refute_nil enum.first.date
      end
    end
  end
end
