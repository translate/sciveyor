# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    class AsOpenUrlTest < ActiveSupport::TestCase
      test "creates good OpenURL params" do
        doc = build(:full_document)
        params = Sciveyor::Documents::AsOpenUrl.new(doc).params

        assert_equal(
          "ctx_ver=Z39.88-2004&rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&rft.genre=article&rft_id=info:doi%2F10.1371%2Fjournal.pntd.0000677&rft.atitle=A+Novel+Family+of+Cyst+Proteins+with+Epidermal+Growth+Factor+Repeats+in+Giardia+lamblia&rft.title=PLoS+Neglected+Tropical+Diseases&rft.date=2010-05-01&rft.volume=4&rft.issue=5&rft.spage=e677&rft.aufirst=Pei-Wei&rft.aulast=Chiu&rft.au=Yu-Chang+Huang&rft.au=Yu-Jiao+Pan&rft.au=Chih-Hung+Wang&rft.au=Chin-Hung+Sun",
          params
        )
      end
    end
  end
end
