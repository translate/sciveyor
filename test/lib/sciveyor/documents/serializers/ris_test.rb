# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

module Sciveyor
  module Documents
    module Serializers
      class RisTest < ActiveSupport::TestCase
        include CommonTests

        test "single document serialization" do
          doc = build(:full_document)
          str = Sciveyor::Documents::Serializers::Ris.new(doc).serialize

          assert str.start_with?("TY  - JOUR\n")
          assert_includes str, "AU  - Chiu,Pei-Wei"
          assert_includes str, "AU  - Wang,Chih-Hung"
          assert_includes str,
                          "TI  - A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia"
          assert_includes str, "JO  - PLoS Neglected Tropical Diseases"
          assert_includes str, "VL  - 4"
          assert_includes str, "IS  - 5"
          assert_includes str, "SP  - e677"
          refute_includes str, "EP  - "
          assert_includes str, "PY  - 2010"
          assert_includes str, "KW  - Cell Biology/Extra-Cellular Matrix"
          assert str.end_with?("ER  - \n")
        end

        test "array serialization" do
          doc = build(:full_document)
          docs = [doc, doc]
          str = Sciveyor::Documents::Serializers::Ris.new(docs).serialize

          assert str.start_with?("TY  - JOUR\n")
          assert_includes str, "ER  - \nTY  - JOUR\n"
        end
      end
    end
  end
end
