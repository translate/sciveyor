# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

module Sciveyor
  module Documents
    module Serializers
      class ModsTest < ActiveSupport::TestCase
        include CommonTests

        test "single document serialization" do
          doc = build(:full_document, pages: "123-456")
          raw = Sciveyor::Documents::Serializers::Mods.new(doc).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          # This test is incomplete, but we'll validate the schema in the next test
          assert_equal "A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia",
                       xml.at_css("mods titleInfo title").content
          assert_equal "Pei-Wei", xml.at_css("mods name namePart").content
          assert_equal "2010-05-01T00:00:00+00:00",
                       xml.at_css("mods originInfo dateIssued").content
          assert_equal "PLoS Neglected Tropical Diseases",
                       xml.at_css("mods relatedItem titleInfo title").content
          assert_equal "2010-05-01T00:00:00+00:00",
                       xml.at_css(
                         "mods relatedItem originInfo dateIssued"
                       ).content
          refute_nil xml.at_css(
                       "mods relatedItem part detail number:contains(\"4\")"
                     )
          refute_nil xml.at_css(
                       "mods relatedItem part detail number:contains(\"5\")"
                     )
          assert_equal "123",
                       xml.at_css("mods relatedItem part extent start").content
          assert_equal "456",
                       xml.at_css("mods relatedItem part extent end").content
          assert_equal "2010-05-01T00:00:00+00:00",
                       xml.at_css("mods relatedItem part date").content
          assert_equal "10.1371/journal.pntd.0000677",
                       xml.at_css("mods identifier").content
        end

        test "single document schema validation" do
          doc = build(:full_document, pages: "123-456")
          raw = Sciveyor::Documents::Serializers::Mods.new(doc).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          schema_path =
            Rails.root.join("test", "support", "xsd", "mods-3-7.xsd")
          schema = Nokogiri::XML::Schema.new(File.open(schema_path))

          errors = schema.validate(xml)
          assert_empty errors, errors.map(&:to_s).join("; ")
        end

        test "array serialization" do
          doc = build(:full_document)
          doc2 = build(:full_document, id: "doi:10.0000/fakedoi")
          docs = [doc, doc2]
          raw = Sciveyor::Documents::Serializers::Mods.new(docs).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          assert_equal "A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia",
                       xml.at_css("modsCollection mods titleInfo title").content
          assert_equal 2, xml.css("modsCollection mods").size
        end

        test "array schema validation" do
          doc = build(:full_document)
          doc2 = build(:full_document, id: "doi:10.0000/fakedoi")
          docs = [doc, doc2]
          raw = Sciveyor::Documents::Serializers::Mods.new(docs).serialize
          xml = Nokogiri::XML::Document.parse(raw)

          schema_path =
            Rails.root.join("test", "support", "xsd", "mods-3-7.xsd")
          schema = Nokogiri::XML::Schema.new(File.open(schema_path))

          errors = schema.validate(xml)
          assert_empty errors, errors.map(&:to_s).join("; ")
        end
      end
    end
  end
end
