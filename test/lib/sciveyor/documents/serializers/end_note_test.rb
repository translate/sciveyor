# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

module Sciveyor
  module Documents
    module Serializers
      class EndNoteTest < ActiveSupport::TestCase
        include CommonTests

        test "single document serialization" do
          doc = build(:full_document)
          str = Sciveyor::Documents::Serializers::EndNote.new(doc).serialize

          assert str.start_with?("%0 Journal Article\n")
          assert_includes str, "%A Chiu, Pei-Wei"
          assert_includes str, "%A Wang, Chih-Hung"
          assert_includes str,
                          "%T A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia"
          assert_includes str, "%J PLoS Neglected Tropical Diseases"
          assert_includes str, "%V 4"
          assert_includes str, "%N 5"
          assert_includes str, "%P e677"
          assert_includes str, "%R 10.1371/journal.pntd.0000677"
          assert_includes str, "%D 2010"
          assert_includes str, "%K Cell Biology/Extra-Cellular Matrix"

          # This extra carriage return is the item separator, and is thus very
          # important
          assert str.end_with?("\n\n")
        end

        test "array serialization" do
          doc = build(:full_document)
          docs = [doc, doc]
          str = Sciveyor::Documents::Serializers::EndNote.new(docs).serialize

          assert str.start_with?("%0 Journal Article\n")
          assert_includes str, "\n\n%0 Journal Article\n"
        end
      end
    end
  end
end
