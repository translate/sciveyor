# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

module Sciveyor
  module Documents
    module Serializers
      class BibTexTest < ActiveSupport::TestCase
        include CommonTests

        test "single document serialization" do
          doc = build(:full_document)
          str = Sciveyor::Documents::Serializers::BibTex.new(doc).serialize

          assert str.start_with?("@article{Chiu2010,")
          assert_includes str,
                          "author = {Pei-Wei Chiu and Yu-Chang Huang and Yu-Jiao Pan and Chih-Hung Wang and Chin-Hung Sun}"
          assert_includes str,
                          "title = {A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia}"
          assert_includes str, "journal = {PLoS Neglected Tropical Diseases}"
          assert_includes str, "volume = {4}"
          assert_includes str, "number = {5}"
          assert_includes str, "pages = {e677}"
          assert_includes str, "doi = {10.1371/journal.pntd.0000677}"
          assert_includes str, "year = {2010}"
        end

        test "array serialization" do
          doc = build(:full_document)
          docs = [doc, doc]
          str = Sciveyor::Documents::Serializers::BibTex.new(docs).serialize

          # FIXME: This is actually a sort of failure case -- we aren't
          # deduplicating the citation keys in any way!
          assert str.start_with?("@article{Chiu2010,")
          assert_includes str, "}\n@article{Chiu2010,"
        end

        test "anonymous documents" do
          doc = build(:full_document, authors: nil)
          str = Sciveyor::Documents::Serializers::BibTex.new(doc).serialize

          assert str.start_with?("@article{Anon2010,")
        end

        test "date-less documents" do
          doc = build(:full_document, date: nil)
          str = Sciveyor::Documents::Serializers::BibTex.new(doc).serialize

          assert str.start_with?("@article{Chiund,")
        end
      end
    end
  end
end
