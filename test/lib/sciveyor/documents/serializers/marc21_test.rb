# frozen_string_literal: true

require "test_helper"
require_relative "./common_tests"

module Sciveyor
  module Documents
    module Serializers
      class Marc21Test < ActiveSupport::TestCase
        include CommonTests

        test "single serialization" do
          doc = build(:full_document)
          marc = Sciveyor::Documents::Serializers::Marc21.new(doc).serialize

          # We just can't test this nonsense
          assert marc.start_with? "00"
        end
      end
    end
  end
end
