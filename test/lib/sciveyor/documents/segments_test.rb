# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    class SegmentsTest < ActiveSupport::TestCase
      setup do
        @doc = build(:full_document)
        Document.stubs(:find_by!).returns(@doc)
      end

      test "with no options" do
        segmenter = Sciveyor::Documents::Segments.new
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 1, blocks.size
        assert_equal "Block #1/1", blocks[0].name

        assert_equal %w[introduction giardia lamblia prevalent intestinal],
                     blocks[0].words.take(5)

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "creating a single block" do
        segmenter = Sciveyor::Documents::Segments.new(num_blocks: 1)
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 1, blocks.size
        assert_equal "Block #1/1", blocks[0].name

        assert_equal %w[introduction giardia lamblia prevalent intestinal],
                     blocks[0].words.take(5)

        assert_includes segmenter.words_for_last, "giardia"
        assert_includes segmenter.words_for_last, "disease"

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "creating multiple blocks" do
        segmenter = Sciveyor::Documents::Segments.new(num_blocks: 5)
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 5, blocks.size
        assert_equal "Block #1/5", blocks[0].name

        assert_equal [74, 74, 73, 73, 73], blocks.map(&:words).map(&:size)

        assert_includes segmenter.words_for_last, "giardia"
        assert_includes segmenter.words_for_last, "disease"

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "invalid last_block acts like big_last" do
        segmenter =
          Sciveyor::Documents::Segments.new(block_size: 3, last_block: :purple)
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 122, blocks.size
      end

      test "creating word-size blocks, big_last" do
        segmenter =
          Sciveyor::Documents::Segments.new(
            block_size: 3,
            last_block: :big_last
          )
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 122, blocks.size
        assert_equal "Block #1 of 3 words", blocks[0].name
        assert_equal %w[introduction giardia lamblia], blocks.first.words

        assert blocks.last.words.count > 3

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "creating word-size blocks, small_last" do
        segmenter =
          Sciveyor::Documents::Segments.new(
            block_size: 3,
            last_block: :small_last
          )
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 123, blocks.size
        assert_equal "Block #1 of 3 words", blocks[0].name
        assert_equal %w[introduction giardia lamblia], blocks.first.words

        assert blocks.last.words.count < 3

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "creating word-size blocks, truncate_last" do
        segmenter =
          Sciveyor::Documents::Segments.new(
            block_size: 3,
            last_block: :truncate_last
          )
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 122, blocks.size
        assert_equal "Block #1 of 3 words", blocks[0].name
        assert_equal %w[introduction giardia lamblia], blocks.first.words

        assert_equal 3, blocks.last.words.count

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "creating word-size blocks, truncate_all" do
        segmenter =
          Sciveyor::Documents::Segments.new(
            block_size: 3,
            last_block: :truncate_all
          )
        segmenter.add(@doc.id)
        blocks = segmenter.blocks

        assert_equal 1, blocks.size
        assert_equal "Block #1 of 3 words", blocks[0].name
        assert_equal %w[introduction giardia lamblia], blocks.first.words

        assert_equal 1125, segmenter.corpus_dfs["disease"]
      end

      test "reset scrubs all the parameters" do
        segmenter = Sciveyor::Documents::Segments.new
        segmenter.add(@doc.id)

        assert_equal 1, segmenter.blocks.size

        segmenter.reset!
        new_blocks = segmenter.blocks

        assert_empty new_blocks
      end
    end
  end
end
