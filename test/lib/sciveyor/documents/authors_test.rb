# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Documents
    class AuthorsTest < ActiveSupport::TestCase
      test "from_docs works with a set of documents" do
        Sciveyor::Documents::Author.expects(:new).with({ name: "A One" })
        Sciveyor::Documents::Author.expects(:new).with({ name: "B Two" })

        Sciveyor::Documents::Authors.from_docs(
          [{ name: "A One" }, { name: "B Two" }]
        )
      end

      test "from_docs works with a single document" do
        Sciveyor::Documents::Author.expects(:new).with({ name: "A One" })

        a = Sciveyor::Documents::Authors.from_docs({ name: "A One" })
        assert_equal 1, a.count
      end

      test "from_docs returns an empty array on nil" do
        assert_equal [], Sciveyor::Documents::Authors.from_docs(nil)
      end

      test "from_docs returns an empty array on empty array" do
        assert_equal [], Sciveyor::Documents::Authors.from_docs([])
      end

      test "to_s works as expected" do
        a =
          Sciveyor::Documents::Authors.from_docs(
            [{ name: "A One" }, { name: "B Two" }]
          )

        assert_equal "A One, B Two", a.to_s
      end
    end
  end
end
