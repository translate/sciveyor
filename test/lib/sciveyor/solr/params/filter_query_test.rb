# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class Params
      class FilterQueryTest < ActiveSupport::TestCase
        test "facet_fq_string returns nil if given a bad facet type" do
          assert_nil Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                       "thing:blue"
                     )
        end

        test "facet_fq_string returns nil if given an empty value" do
          assert_nil Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                       "author:\"\""
                     )
        end

        test "facet_fq_string removes quotes from values" do
          assert_equal "+type:article +journal:\"Test\"",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "journal:\"Test\""
                       )
        end

        test "facet_fq_string works for author facets" do
          assert_equal "{!parent which=\"type:article\"}(+name:\"Jane Doe\")",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "author:Jane Doe"
                       )
        end

        test "fq_facet works for author facets" do
          assert_equal "author:Jane Doe",
                       Sciveyor::Solr::Params::FilterQuery.fq_facet(
                         "{!parent which=\"type:article\"}(+name:\"Jane Doe\")"
                       )
        end

        test "facet_fq_string works for journal facets" do
          assert_equal "+type:article +journal:\"Journal of Test\"",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "journal:Journal of Test"
                       )
        end

        test "fq_facet works for journal facets" do
          assert_equal "journal:Journal of Test",
                       Sciveyor::Solr::Params::FilterQuery.fq_facet(
                         "+type:article +journal:\"Journal of Test\""
                       )
        end

        test "facet_fq_string works for date before" do
          assert_equal "+type:article +date:[* TO 1800-01-01T00:00:00Z]",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "date:before"
                       )
        end

        test "fq_facet works for date before" do
          assert_equal "date:before",
                       Sciveyor::Solr::Params::FilterQuery.fq_facet(
                         "+type:article +date:[* TO 1800-01-01T00:00:00Z]"
                       )
        end

        test "facet_fq_string works for date after" do
          assert_equal "+type:article +date:[2020-01-01T00:00:00Z TO *]",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "date:after"
                       )
        end

        test "fq_facet works for date after" do
          assert_equal "date:after",
                       Sciveyor::Solr::Params::FilterQuery.fq_facet(
                         "+type:article +date:[2020-01-01T00:00:00Z TO *]"
                       )
        end

        test "facet_fq_string works for normal date" do
          assert_equal "+type:article +date:[1960-01-01T00:00:00Z TO 1960-01-01T00:00:00Z+10YEARS]",
                       Sciveyor::Solr::Params::FilterQuery.facet_fq_string(
                         "date:1960"
                       )
        end

        test "fq_facet works for normal date" do
          assert_equal "date:1960",
                       Sciveyor::Solr::Params::FilterQuery.fq_facet(
                         "+type:article +date:[1960-01-01T00:00:00Z TO 1960-01-01T00:00:00Z+10YEARS]"
                       )
        end

        test "categories_fq_string works" do
          category =
            Category.create(
              name: "Test Category",
              journals: ["Gutenberg", "PLoS Neglected Tropical Diseases"]
            )

          assert_equal "+type:article +journal:(\"Gutenberg\" OR \"PLoS Neglected Tropical Diseases\") /* categories:#{category.id} */",
                       Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
                         [category.to_param]
                       )
        end

        test "categories_fq_string returns nil for empty categories" do
          category = Category.create(name: "Empty Category")
          assert_nil Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
                       [category.to_param]
                     )
        end

        test "categories_fq_string works with empty and full categories" do
          category1 =
            Category.create(
              name: "Test Category",
              journals: ["PNTDS", "PLoS Neglected Tropical Diseases"]
            )
          category2 = Category.create(name: "Empty Category")
          assert_equal "+type:article +journal:(\"PNTDS\" OR \"PLoS Neglected Tropical Diseases\") /* categories:#{category1.id},#{category2.id} */",
                       Sciveyor::Solr::Params::FilterQuery.categories_fq_string(
                         [category1.to_param, category2.to_param]
                       )
        end

        test "fq_facet returns nil for category facet" do
          assert_nil Sciveyor::Solr::Params::FilterQuery.fq_facet(
                       "+type:article +journal:(\"Gutenberg\" OR \"PLoS Neglected Tropical Diseases\") /* categories:1 */"
                     )
        end
      end
    end
  end
end
