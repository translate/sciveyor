# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class Params
      class QueryTest < ActiveSupport::TestCase
        test "query_string works for basic fields" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "volume",
                         "30"
                       ),
                       "volume:(30)"
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "number",
                         "5"
                       ),
                       "number:(5)"
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "pages",
                         "300-301"
                       ),
                       "pages:(300-301)"
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "journal_clean",
                         "Astrobiology"
                       ),
                       "journal_clean:(Astrobiology)"
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "title_stem",
                         "Testing with Spaces"
                       ),
                       "title_stem:(Testing with Spaces)"
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "fullText_lem",
                         "alien"
                       ),
                       "fullText_lem:(alien)"
        end

        test "field_value works for basic fields" do
          assert_equal Sciveyor::Solr::Params::Query.field_value("volume:(30)"),
                       %w[volume 30]
          assert_equal Sciveyor::Solr::Params::Query.field_value("number:(5)"),
                       %w[number 5]
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "pages:(300-301)"
                       ),
                       %w[pages 300-301]
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "journal_clean:(Astrobiology)"
                       ),
                       %w[journal_clean Astrobiology]
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "title_stem:(Testing with Spaces)"
                       ),
                       ["title_stem", "Testing with Spaces"]
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "fullText_lem:(alien)"
                       ),
                       %w[fullText_lem alien]
        end

        test "query_string handles multiple authors correctly" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "authors",
                         "W. Shatner, J. Doe"
                       ),
                       " {!parent which=\"type:article\"}name_clean:(\"W* Shatner\") AND  {!parent which=\"type:article\"}name_clean:(\"J* Doe\")"
        end

        test "field_value handles multiple authors correctly" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         " {!parent which=\"type:article\"}name_clean:(\"W* Shatner\") AND  {!parent which=\"type:article\"}name_clean:(\"J* Doe\")"
                       ),
                       ["authors", "W. Shatner, J. Doe"]
        end

        test "query_string handles Lucene name forms correctly" do
          ret =
            Sciveyor::Solr::Params::Query.query_string(
              "authors",
              "Joe John Public"
            )

          # No need to test all of these, just hit a couple
          assert_includes ret, "\"Joe Public\""
          assert_includes ret, "\"J Public\""
          assert_includes ret, "\"JJ Public\""
          assert_includes ret, "\"J John Public\""
        end

        test "field_value handles Lucene name forms correctly" do
          # Use query_string to build the huge thing
          ret =
            Sciveyor::Solr::Params::Query.query_string(
              "authors",
              "Joe John Public"
            )

          assert_equal Sciveyor::Solr::Params::Query.field_value(ret),
                       ["authors", "Joe John Public"]
        end

        test "query_string handles only single year" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "date",
                         "1900"
                       ),
                       "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z])"
        end

        test "query_string handles year range" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "date",
                         "1900 - 1910"
                       ),
                       "date:([1900-01-01T00:00:00Z TO 1910-12-31T23:59:59Z])"
        end

        test "query_string handles multiple single years" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "date",
                         "1900, 1910"
                       ),
                       "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z] OR [1910-01-01T00:00:00Z TO 1910-12-31T23:59:59Z])"
        end

        test "query_string handles single years with ranges" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "date",
                         "1900, 1910-1920, 1930"
                       ),
                       "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z] OR [1910-01-01T00:00:00Z TO 1920-12-31T23:59:59Z] OR [1930-01-01T00:00:00Z TO 1930-12-31T23:59:59Z])"
        end

        test "query_string rejects non-numeric year params" do
          assert_nil Sciveyor::Solr::Params::Query.query_string(
                       "date",
                       "asdf, wut-asf, 1-2-523"
                     )
        end

        test "field_value handles only single year" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z])"
                       ),
                       %w[date 1900]
        end

        test "field_value handles year range" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "date:([1900-01-01T00:00:00Z TO 1910-12-31T23:59:59Z])"
                       ),
                       %w[date 1900-1910]
        end

        test "field_value handles multiple single years" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z] OR [1910-01-01T00:00:00Z TO 1910-12-31T23:59:59Z])"
                       ),
                       ["date", "1900, 1910"]
        end

        test "field_value handles single years with ranges" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         "date:([1900-01-01T00:00:00Z TO 1900-12-31T23:59:59Z] OR [1910-01-01T00:00:00Z TO 1920-12-31T23:59:59Z] OR [1930-01-01T00:00:00Z TO 1930-12-31T23:59:59Z])"
                       ),
                       ["date", "1900, 1910-1920, 1930"]
        end

        test "query_string correctly parses edismax search" do
          assert_equal Sciveyor::Solr::Params::Query.query_string(
                         "dismax",
                         "test"
                       ),
                       " {!edismax v=\"test\"}"
        end

        test "field_value correctly parses edismax search" do
          assert_equal Sciveyor::Solr::Params::Query.field_value(
                         " {!edismax v=\"test\"}"
                       ),
                       %w[dismax test]
        end
      end
    end
  end
end
