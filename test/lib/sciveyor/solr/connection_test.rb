# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class ConnectionTest < ActiveSupport::TestCase
      test "search wraps results in a result object" do
        res = Sciveyor::Solr::Connection.search(q: "*:*")

        assert_kind_of Sciveyor::Solr::SearchResult, res
      end

      test "search_raw returns an empty hash if Solr fails" do
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout

        assert_equal({}, Sciveyor::Solr::Connection.search_raw(q: ""))
      end

      test "info connects to the right Solr path" do
        RSolr::Client.any_instance.expects(:get).with("admin/system")
        Sciveyor::Solr::Connection.info
      end

      test "info returns an empty hash when Solr fails" do
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
        assert_equal({}, Sciveyor::Solr::Connection.info)
      end

      test "ping works" do
        assert_kind_of Integer, Sciveyor::Solr::Connection.ping
      end

      test "ping returns nil when Solr fails" do
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout

        assert_nil Sciveyor::Solr::Connection.ping
      end
    end
  end
end
