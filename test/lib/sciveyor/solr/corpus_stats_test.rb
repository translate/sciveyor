# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Solr
    class CorpusStatsTest < ActiveSupport::TestCase
      test "works" do
        stats = Sciveyor::Solr::CorpusStats.new

        assert_equal 1500, stats.size
      end
    end
  end
end
