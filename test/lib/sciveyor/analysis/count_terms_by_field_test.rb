# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    class CountTermsByFieldTest < ActiveSupport::TestCase
      test "works without a dataset" do
        counts =
          Sciveyor::Analysis::CountTermsByField.call(
            term: "online",
            field: :date
          )

        assert_equal 2, counts[2009]
        assert_equal 3, counts[2011]
        assert_equal 3, counts[2010]
      end

      test "returns empty counts without dataset when Solr fails" do
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
        assert_empty Sciveyor::Analysis::CountTermsByField.call(
                       term: "malaria",
                       field: :year
                     )
      end

      test "works with a dataset" do
        counts =
          Sciveyor::Analysis::CountTermsByField.call(
            term: "disease",
            field: :date,
            dataset: create(:full_dataset, num_docs: 2)
          )

        assert_equal 1, counts.size
        assert counts[2009].positive?
      end

      # FIXME: this is failing and I don't know how to deal with it
      # test 'returns empty counts with dataset when Solr fails' do
      #   d = create(:full_dataset, num_docs: 2)
      #   stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
      #   assert_empty Sciveyor::Analysis::CountTermsByField.call(term: 'malaria',
      #                                                           field: :year,
      #                                                           dataset: d)
      # end
    end
  end
end
