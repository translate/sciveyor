# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    class CountArticlesByFieldTest < ActiveSupport::TestCase
      test "works without a dataset" do
        result = Sciveyor::Analysis::CountArticlesByField.call(field: :date)

        assert_kind_of Sciveyor::Analysis::CountArticlesByField::Result, result
        refute result.normalize

        assert_equal 224, result.counts["2009"]
        assert_equal 42, result.counts["2007"]
      end

      test "returns empty result when Solr fails" do
        stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
        result = Sciveyor::Analysis::CountArticlesByField.call(field: :date)

        assert_empty result.counts
      end

      test "works with a dataset" do
        result =
          Sciveyor::Analysis::CountArticlesByField.call(
            field: :date,
            dataset: create(:full_dataset, num_docs: 10)
          )

        assert_kind_of Sciveyor::Analysis::CountArticlesByField::Result, result
        refute result.normalize

        assert_equal 1, result.counts.size
        assert_equal 10, result.counts["2009"]
      end

      # FIXME: Test is failing, not sure how I want to deal with this.
      # test 'returns empty result when Solr fails with a dataset' do
      #   d = create(:full_dataset, num_docs: 10)
      #   stub_request(:any, /(127\.0\.0\.1|localhost)/).to_timeout
      #   result = Sciveyor::Analysis::CountArticlesByField.call(
      #     field: :date,
      #     dataset: d)

      #   assert_empty result.counts
      # end

      test "works when normalizing to a dataset" do
        result =
          Sciveyor::Analysis::CountArticlesByField.call(
            field: :date,
            dataset: create(:full_dataset, num_docs: 10),
            normalize: true,
            normalization_dataset: create(:full_dataset, num_docs: 10)
          )

        assert_kind_of Sciveyor::Analysis::CountArticlesByField::Result, result
        assert result.normalize
        refute_nil result.normalization_dataset

        assert_equal 1, result.counts.size
        assert_equal 1.0, result.counts["2009"]
      end

      test "works when normalizing to the corpus" do
        result =
          Sciveyor::Analysis::CountArticlesByField.call(
            field: :date,
            dataset: create(:full_dataset, num_docs: 10),
            normalize: true
          )

        assert_kind_of Sciveyor::Analysis::CountArticlesByField::Result, result
        assert result.normalize
        assert_nil result.normalization_dataset

        assert_equal 6, result.counts.size
        assert_in_delta 0.0446, result.counts["2009"]

        assert_equal 0, result.counts["2012"]
      end

      test "works with a non-numeric field" do
        result =
          Sciveyor::Analysis::CountArticlesByField.call(
            field: :journal,
            dataset: create(:full_dataset, num_docs: 10),
            normalize: true
          )

        assert_in_delta 0.0066666,
                        result.counts["PLoS Neglected Tropical Diseases"]
      end
    end
  end
end
