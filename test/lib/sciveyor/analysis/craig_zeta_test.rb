# frozen_string_literal: true

require "test_helper"

module Sciveyor
  module Analysis
    class CraigZetaTest < ActiveSupport::TestCase
      setup do
        # N.B.: If you do 10-doc datasets here, all the words are in common, and
        # there's no zeta scores.
        @dataset1 = create(:full_dataset, num_docs: 2)
        @dataset2 = create(:full_dataset, num_docs: 2)
      end

      test "zeta_scores are properly valued and sorted" do
        analyzer =
          Sciveyor::Analysis::CraigZeta.call(
            dataset_1: @dataset1,
            dataset_2: @dataset2
          )

        assert analyzer.zeta_scores.first[1] >
                 analyzer.zeta_scores.reverse_each.first[1]

        analyzer.zeta_scores.values.each do |score|
          assert_includes (0..2), score
        end
      end

      test "dataset markers work" do
        analyzer =
          Sciveyor::Analysis::CraigZeta.call(
            dataset_1: @dataset1,
            dataset_2: @dataset2
          )

        assert_equal analyzer.zeta_scores.first[0],
                     analyzer.dataset_1_markers.first
        assert_equal analyzer.zeta_scores.reverse_each.first[0],
                     analyzer.dataset_2_markers.first
        assert_equal analyzer.dataset_1_markers.size,
                     analyzer.dataset_2_markers.size
      end

      test "graph_points work" do
        analyzer =
          Sciveyor::Analysis::CraigZeta.call(
            dataset_1: @dataset1,
            dataset_2: @dataset2
          )

        assert_kind_of Float, analyzer.graph_points[0].x
        assert_kind_of Float, analyzer.graph_points[0].y
        assert_kind_of String, analyzer.graph_points[0].name

        analyzer.graph_points.each do |p|
          assert_includes (0..1), p.x
          assert_includes (0..1), p.y
          assert_match /(#{@dataset1.uuid}|#{@dataset2.uuid})/, p.name

          assert_kind_of Array, p.to_a
        end
      end
    end
  end
end
