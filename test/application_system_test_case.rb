# frozen_string_literal: true

# Always instantly execute jobs in system tests
ENV["BLOCKING_JOBS"] = "true"

require "test_helper"
require "webdrivers/geckodriver"
require "selenium/webdriver"

# Set a new command name to be sure that we don't clobber merged tests
SimpleCov.command_name "test:system" if ENV["COVERAGE"]

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium_headless

  setup { Capybara.current_window.resize_to(1400, 1400) }

  # Helpers for making our system tests much cleaner
  include SystemDatasetHelper
  include SystemStubHelper
  include SystemUserHelper
end
