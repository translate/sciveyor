# frozen_string_literal: true

require "socket"
require "action_view/helpers/date_helper"

# Instantiate the date helpers for printing out our durations below
class DurationHelper
  include ActionView::Helpers::DateHelper
end

# Called when we want to terminate a job forcibly
def on_terminate(task, message, backtrace = nil)
  log = {
    short_message: "Terminating job: #{message}",
    _method: "analyze_one",
    _task_id: task.id
  }
  log[:_backtrace] = backtrace.join("; ") if backtrace

  Rails.logger.error(log)

  task.failed = true
  task.finished = true
  task.finished_at = DateTime.now
  task.job_message = message
  task.save
end

def work_one_job(max_time)
  Rails.logger.debug(
    short_message: "Querying job from database (analyze_one)",
    _method: "analyze_one"
  )

  # Pull one job off of the queue, if it exists
  result = ActiveRecord::Base.connection.exec_query(<<-SQL)
          -- This query will return exactly one job, and is suitable to be run
          -- concurrently because of SKIP LOCKED. When we commit the transaction
          -- the row will be deleted.
          DELETE FROM sciveyor.jobs
          WHERE id = (
            SELECT id FROM sciveyor.jobs
            ORDER BY id
            FOR UPDATE SKIP LOCKED
            LIMIT 1
          )
          RETURNING id, task_id;
        SQL

  # If the query didn't return a job, then there simply aren't any
  if result.empty?
    Rails.logger.debug(
      short_message: "No tasks available to work, exiting (analyze_one)",
      _method: "analyze_one"
    )
    return
  end

  # Get the task
  task_id = result.rows[0][1]
  task = Task.find(task_id)

  # Don't try to run the task more than three times
  Task.in_separate_connection do |conn|
    sql = <<-SQL
          -- Increment attempts and return the incremented value
          UPDATE sciveyor.tasks
                 SET attempts = attempts + 1
                 WHERE id = :id
                 RETURNING attempts;
          SQL

    query = Task.sanitize_sql_array([sql, id: task_id])
    result = conn.exec_query(query)

    attempts = result.rows[0][0]
    if attempts > 3
      on_terminate(
        task,
        "Attempted to run task too many times (#{attempts}), giving up"
      )
      return
    end
  end

  # Capture exceptions, timeouts, and Ctrl-C
  begin
    # Get the worker options
    json = task.job_params
    options = json ? JSON.parse(json) : {}
    klass = task.job_class

    Rails.logger.info(
      short_message: "Starting job (analyze_one)",
      _method: "analyze_one",
      _task_id: task_id,
      _type: klass.to_s,
      _options: json
    )

    # Do the actual work
    Timeout.timeout(max_time) { klass.new.perform(task, options) }
  rescue Timeout::Error
    on_terminate(task, "Job worker ran past maximum allowed time")
  rescue SignalException
    on_terminate(task, "Job worker terminated by signal")
  rescue Exception => e
    on_terminate(task, e.to_s, e.backtrace)
  end

  if task.failed
    Rails.logger.error(
      short_message: "Job failed",
      _method: "analyze_one",
      _task_id: task_id
    )
  else
    Rails.logger.info(
      short_message: "Job succeeded",
      _method: "analyze_one",
      _task_id: task_id
    )
  end
end

# Work one job, and then quit with an exit code. Designed to be monitored by
# our monitoring process.
namespace :sciveyor do
  namespace :jobs do
    desc "Run exactly one job from the job queue"
    task analyze_one: :environment do
      # Get the max timeout in seconds
      if Rails.env.test?
        max_time = 2
      else
        max_time = (ENV["JOB_TIMEOUT"] || "12").to_i.hours.to_i
      end

      # Work one job in a transaction
      ActiveRecord::Base.connection.transaction { work_one_job(max_time) }
    rescue ActiveRecord::StatementInvalid
      Rails.logger.fatal(
        short_message: "Database connection failed (schema not yet loaded?)",
        _method: "analyze_one"
      )
    rescue ActiveRecord::ConnectionNotEstablished
      Rails.logger.fatal(
        short_message: "Cannot connect to database",
        _method: "analyze_one"
      )
    end

    desc "Run a daemon working the job queue"
    task analyze: :environment do
      begin
        # Simply work the analysis queue, one-at-a-time, until the end of time,
        # sleeping for 15 seconds between each go to let GC/VM catch up.
        loop do
          system("bundle exec rake sciveyor:jobs:analyze_one")
          sleep 15
        end
      end
    end
  end
end
