# frozen_string_literal: true

module Sciveyor
  class LogFormatter
    def call(severity, time, progname, msg = "")
      return "" if msg.blank?

      if msg.is_a?(Hash)
        msg_str = msg.delete(:short_message)
        msg_hash = msg
      else
        msg_str = msg
        msg_hash = {}
      end

      msg_hash[:timestamp] = time.to_s
      msg_hash[:level] = severity
      msg_hash[:progname] = progname if progname.present?

      ret = []
      msg_hash.each do |k, v|
        k = k.to_s
        k.delete_prefix!("_")

        v = "\"#{v}\"" if v.is_a?(String)

        ret << "#{k}=#{v}"
      end

      ret << msg_str
      ret.join(" ") + "\n"
    end
  end
end
