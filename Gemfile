# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.3"

# Rails and basics
gem "gelf"
gem "lograge"
gem "puma", "~> 5.0"
gem "rails", "~> 7.0.3"

# Database and related tools
gem "nanoid"
gem "pg", "~> 1.1"
gem "virtus"

# User authentication and administration
gem "devise", "~> 4", ">= 4.7.1"
gem "devise-i18n"

# Internationalization
gem "rails-i18n", "= 7.0.6"
gem "twitter_cldr"

# Textual analysis
gem "distribution"
gem "prime"
gem "engtagger"
gem "fast-stemmer"
gem "lemmatizer"
gem "rsolr", ">= 1.0.7"
gem "rsolr-ext"

# Citation processing
gem "bibtex-ruby", require: "bibtex"

# Support for file attachments and exporting
gem "aws-sdk-s3"
gem "marcel"
gem "marc"
gem "rdf", ">= 0.3.5"
gem "sparql", ">= 3.1.0"
gem "rdf-n3"
gem "rdf-vocab"
gem "rubyzip", "~> 2", require: "zip"

# Asset tools and template generators
gem "dartsass-rails"
gem "haml", ">= 5.1.0"
gem "haml-rails", "~> 2.0"
gem "importmap-rails"
gem "kramdown"
gem "nokogiri", ">= 1.10.8"
gem "sprockets-rails"

# Mail
gem "mailgun-ruby"
gem "bootstrap-email", ">= 1.0.0.alpha"

# Testing
group :test, :development do
  gem "prettier"

  gem "capybara", ">= 3.26"

  # gem "capybara-slow_finder_errors"
  gem "factory_bot_rails"
  gem "listen", "~> 3.3"
  gem "mocha", require: false

  gem "byebug"
end

group :test do
  gem "webdrivers", "~> 4.0", require: false
  gem "webmock", ">= 3.5.0", require: false
  gem "rails-controller-testing"
  gem "simplecov", require: false
end
