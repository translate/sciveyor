# Release Instructions

1.  Make sure your local commit is up to date:

    ```shell_session
    $ git status
    On branch main
    Your branch is up to date with 'origin/main'.

    nothing to commit, working tree clean
    $ git push
    Everything up-to-date
    $ git pull
    Already up to date.
    ```

2.  Make sure the tests all pass:

    ```shell_session
    $ doppler run -c dev_test bin/rails test
    $ doppler run -c dev_test bin/rails test:system
    ```

3.  (FIXME: Update translations, but Transifex is dead.)

4.  Update the README and ChangeLog. The version number may appear multiple
    times, be sure to catch them all. Make sure to write the release date in the
    ChangeLog header. If you're changing a minor version or better, update the
    version badge in `docs/badges`.

    ```shell_session
    $ # edit files
    $ git commit -m 'Update README and ChangeLog for release.'
    $ git push
    ```

5.  Create a new release tag and push it upstream:

    ```shell_session
    $ git tag -am 'vX.Y' vX.Y
    $ git push --tags
    ```

6.  Visit releases at Codeberg. Click "New release." Select the new tag you just
    created from the pull-down menu. Under "Title," repeat the tag name, "vX.Y".
    If the changeset for this release is small enough that you can summarize it
    in a bulleted list, do that in the description, otherwise simply write "See
    ChangeLog for details.".

7.  Build Docker images:

    ```console
    $ sudo docker build -f Dockerfile.web \
      --build-arg GIT_COMMIT=$(git rev-parse HEAD) \
      -t pencechp/sciveyor-web:latest .
    $ sudo docker image tag pencechp/sciveyor-web:latest pencechp/sciveyor-web:X.Y
    $ sudo docker image tag pencechp/sciveyor-web:latest pencechp/sciveyor-web:X
    $ sudo docker push pencechp/sciveyor-web:latest
    $ sudo docker push pencechp/sciveyor-web:X.Y
    $ sudo docker push pencechp/sciveyor-web:X
    $ sudo docker build -f Dockerfile.worker --build-arg \
      GIT_COMMIT=$(git rev-parse HEAD) \
      -t pencechp/sciveyor-worker:latest .
    $ sudo docker image tag pencechp/sciveyor-worker:latest pencechp/sciveyor-worker:X.Y
    $ sudo docker image tag pencechp/sciveyor-worker:latest pencechp/sciveyor-worker:X
    $ sudo docker push pencechp/sciveyor-worker:latest
    $ sudo docker push pencechp/sciveyor-worker:X.Y
    $ sudo docker push pencechp/sciveyor-worker:X
    ```
