# Notes on Logging

Starting in mid-2022, we tried to implement a unified logging standard that would both give interesting logs and work well when onboarded to our Greylog server. Here's some notes for the standards we're using.

1. Use all of the logging levels.

   - `fatal`: Something is about to crash, or we're about to terminate the process. There has been an error from which we can't recover.
   - `error`: Something has genuinely gone wrong -- the user's going to notice that something didn't work right. But we're going to recover from it and try to keep going.
   - `info`: This is usually going to show up in Greylog, even in production, so it should be things that genuinely matter -- for instance, access logs, starting jobs, and so forth.
   - `debug`: Probably won't show up in Greylog, but could be useful in debugging. Use this for log messages that are just indicating that things are operating normally -- they're useful as "canaries" if things really start to break, but aren't informative in normal operation (or would just overwhelm the logs).

2. Use structured logging. We've overridden the Rails logging mechanism to send hashes to Greylog, that will be conserved and will preserve the attributes that you send. A logging call should thus look like:

```ruby
Rails.logger.level(
  short_message: "A short message, absolutely required for every event",
  _other_attributes: "start with an underscore",
  _etc: "can include as many as you want"
)
```

3. Log more structured info rather than less. Most log calls should include the `_method` parameter -- what are we doing? Most of them should also include a `_object_id` parameter -- what object are we working on?
