# Asset Updating

There are a number of packages that we depend on that are JS-only. To check the
full list, read the `config/importmap.rb` file. To update one of these, all you
need to do is re-run the command:

```sh
bin/importmap pin [package-name]
```

Both `bootstrap` and `font-awesome` are manually vendored in `vendor/javascript`
because they include both JS and CSS resources.

- To update `bootstrap`:
  - Move `dist/js/bootstrap.esm.js` to `vendor/javascript/bootstrap.js`
  - Move the `scss` folder and all the docs at the ZIP file root to
    `vendor/javascript/bootstrap/`
- To update `fontawesome`:
  - Move `js/solid.js` file to `vendor/javascript/fa-solid.js`
  - Move `js/fontawesome.js` file to `vendor/javascript/fontawesome.js`
  - Move `css/svg-with-js.css` and `LICENSE.txt` to
    `vendor/javascript/fontawesome/`.
