#!/bin/bash
# Run the analysis worker (Docker entrypoint)

# If Doppler is configured, use it
if [[ ! -z "$DOPPLER_TOKEN" ]]; then
  doppler run -- bundle exec rails sciveyor:jobs:analyze
else
  bundle exec rails sciveyor:jobs:analyze
fi
