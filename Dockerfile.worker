# syntax=docker/dockerfile:1
FROM ruby:3.1.3

# Force production mode and UTF-8 locale
ENV RAILS_ENV=production
ENV LANG=C.UTF-8

# Add the prereqs for package installation
RUN apt-get install -y apt-transport-https ca-certificates curl gnupg-agent

# Add the Doppler package source
RUN curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | gpg --dearmor > /usr/share/keyrings/doppler-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/doppler-keyring.gpg] https://packages.doppler.com/public/cli/deb/debian any-version main" > /etc/apt/sources.list.d/doppler-cli.list

# Install requred pacakges
RUN apt-get update -qq && apt-get install -y postgresql-client doppler

# Raise an error if the Gemfile is out of date when we try to build the image
RUN bundle config set --global frozen 1
RUN bundle config set --global without "development test"

WORKDIR /usr/src/app

# Install the bundle
COPY Gemfile Gemfile.lock ./
RUN gem update bundler
RUN bundle install

# Copy over the app itself
COPY . .

# Create a REVISION file inside the container to show the code build to users
ARG GIT_COMMIT=unspecified
RUN echo $GIT_COMMIT > REVISION

# Precompile assets
RUN DOCKER_BUILD="1" bundle exec rails assets:precompile

CMD cd /usr/src/app && bin/entrypoint-worker.sh
